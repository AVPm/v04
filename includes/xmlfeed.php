<?php
ini_set('display_errors', 0);
session_start();
include("config.php");             //basic settings, db-access, etc
include("../accesscontrol.php");      //control user sessions, project selection and display_item <=> project 

$path = '../../content/displayitems/';


// Start writing XML
	$o = '<?xml version="1.0" encoding="utf-8"?>' . "\n<gallery>\n";

//multialbum version
//get all album from project
$sql = "SELECT di.id, di.name, di.smallthumb, di.info, p.identifier FROM 3pc_displayitem AS di 
        LEFT JOIN 3pc_project AS p ON p.id=di.id_project   
        WHERE di.id_project='".$_SESSION['id_project']."' AND di.itemlevel='album' ORDER BY sortingorder";
$result = mysql_query($sql);
while($row=mysql_fetch_assoc($result)){
  $identifier = $row['identifier'];
  $albumid = $row['id'];
  $albumname = $row['name'];
  if($row['smallthumb'] != ''){
    $albumthumbnpath = $path . $row['smallthumb'];  
  }else{
    $albumthumbnpath = 'pix/imageicon.png';  
  }
  $albuminfo = htmlspecialchars($row['info']);

  //get all images from version, need to crawl through image-versions too
  $sql = "SELECT id, name, fullfile, info FROM 3pc_displayitem WHERE id_parent_displayitem = '$albumid' ORDER BY sortingorder";
  $result2 = mysql_query($sql);
  unset($rows);
  while($row2=mysql_fetch_assoc($result2)){
    $rows[] = $row2;
    $sql = "SELECT id, name, fullfile, info FROM 3pc_displayitem WHERE id_parent_displayitem='".$row2['id']."'";
    $result3 = mysql_query($sql);
    while($row3=mysql_fetch_assoc($result3)){
      $rows[] = $row3;
    }  
  }




//echo '<pre>';
//print_r($rows);
//echo '</pre>';

//echo $sql;


  
  $o .= "\t" . '<album id="'.$albumid.'" title="' . $albumname . '" description="'.$albuminfo.'" lgPath="' . $path . '" tnPath="' . $path . '" tn="'.$albumthumbnpath.'">' . "\n";
	
	//
  //each image		
  foreach($rows AS $row){
    $o .= "\t\t" . '<img src="' . $row['fullfile'] . '" id="'.$row['id'].'" title="' . $row['name'] . '" caption="'.htmlspecialchars($row['info']).'" link="../../index.php?id_displayitem=' . $row['id'] . '&amp;identifier_project='.$identifier.'" target="_self" />' . "\n";
  }

  // Close the album tag
	$o .= "\t</album>\n";
	// Close gallery tag, set header and output XML
}//end of while over all album of project
	$o .= "</gallery>";
	header('Content-type: text/xml');
	echo $o;
?>
