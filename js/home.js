$(function() {
  //image swap
  $('.home_thumb_img').click(function(){
    var main_heading, main_imagesrc, thumb_heading, thumb_imagesrc;
    //get currently displayed stuff => img + text
    main_heading = $('#introheading').html();
    main_imagesrc = $('#introimage').attr('src'); 
    var lastIndex = main_imagesrc.lastIndexOf(".");

    //var s1 = s.substring(0, lastIndex); //after this s1="Text1, Text2, Text"
    //var s2 = s.substring(lastIndex + 1); //after this s2="true"
    var image_name = main_imagesrc.substring(0, lastIndex);;
    var img_type = main_imagesrc.substring(lastIndex + 1);
    var main_imagesrcArr=main_imagesrc.split('.');
    //get stuff from the clicked one
    thumb_heading = $(this).find('span').html();
    thumb_imagesrc = $(this).find('img').attr('src');
    //alter src name for thumbs and large pics respectivly
    //main_imagesrc = main_imagesrcArr[0].concat('_thumb');
    main_imagesrc = image_name.concat('_thumb');
    //main_imagesrc = main_imagesrc.concat('.'+main_imagesrcArr[1]);
    main_imagesrc = main_imagesrc.concat('.'+img_type);
    thumb_imagesrc = thumb_imagesrc.replace('_thumb', ""); 
    //alert(imagesrc2)
    //switcharoo    
    $(this).find('span').html(main_heading);
    $(this).find('img').attr('src', main_imagesrc);
    $('#introheading').html(thumb_heading);
    $('#introimage').attr('src', thumb_imagesrc);          
    
    var id=this.id;
    var idArr=id.split('_');
    var idnum=idArr[1];
    for(var i=1; i<=4; i++){
        $('#intro'+i).hide();
    }
    $('#intro'+idnum).show();
  });
});