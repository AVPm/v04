  
(function($){
  //remember the original size for later 
  
  
	//Resize image on ready or resize
	$.fn.supersized = function() {
	
		var options = $.extend($.fn.supersized.defaults, $.fn.supersized.options);
		
	  this.each(function() {
      this['originalSize'] = {
        width: $(this).width(),
        height: $(this).height()
      };
      $(this).resizenow();
    });
		
		$().ready(function() {
			$('#imagescaler').resizenow(); 
		});
		
		$(window).bind("resize", function(){
    		$('#imagescaler').resizenow(); 
		});
		
		return this;
		
	};
	
	//Adjust image size
	$.fn.resizenow = function() {
		
		var options = $.extend($.fn.supersized.defaults, $.fn.supersized.options);
		
	  	return this.each(function() {
	  	  
      
        var arrayPageSize = getPageSize();
        var widthContent = arrayPageSize[2]-290;
        var heightTable = arrayPageSize[3]-1;         	
			  var heightContent = heightTable-190-33;
        var heightContentProject = heightTable-33;
        var heightScrollbar = heightContent-19-19;
			//Gather browser and current image size
			var imagewidth = $(this).width();
			var imageheight = $(this).height();
			var imageratio = imageheight/imagewidth;
			
			
			//alteration => get parent width & height here
			var browserwidth = widthContent;
			var browserheight = heightScrollbar;
      var browserratio = browserheight/browserwidth;
      //alert(browserwidth);
			//var browserwidth = $(window).width();
			//var browserheight = $(window).height();
			//console.log(this.originalSize);
  		/*
			if(this.originalSize) {
  	  	if(imagewidth > this.originalSize.width)
  	  	  imagewidth = this.originalSize.width;
  	  	if(imageheight > this.originalSize.height)
  	  	  imageheight = this.originalSize.height;
  	  }
			/**/
			//alert(window.flags.overscale);
			
			if(imageratio > browserratio){
			   if((this.originalSize.height < browserheight) && !window.flags.upscale){
			     $(this).height(this.originalSize.height);
			     $(this).width(this.originalSize.width);
         }else{                   
  			   $(this).height(browserheight);				   
				   $(this).width(browserheight/imageratio);
				 }
      }else{
        if(this.originalSize.width < browserwidth && !window.flags.upscale){			   
          $(this).width(this.originalSize.width);
				  $(this).height(this.originalSize.height);
				}else{
				    $(this).width(browserwidth);
				    $(this).height(browserwidth*imageratio);				
        }
      }
      
		  /*
				//When browser is taller	
				if (browserheight > browserwidth){
				    imageheight = browserheight;
				    $(this).height(browserheight);
				    imagewidth = browserheight/imageratio;
				    $(this).width(imagewidth);
				    
				    if (browserwidth > imagewidth){
				    	imagewidth = browserwidth;
				    	$(this).width(browserwidth);
				    	imageheight = browserwidth * imageratio;
				    	$(this).height(imageheight);
				    }
				
				}
				
				//When browser is wider
				if (browserwidth >= browserheight){
				    imagewidth = browserwidth;
				    $(this).width(browserwidth);
				    imageheight = browserwidth * imageratio;
				    $(this).height(imageheight);
				    
				    if (browserheight > imageheight){
				    	imageheight = browserheight;
				    	$(this).height(browserheight);
				    	imagewidth = browserheight/imageratio;
				    	$(this).width(imagewidth);
				    }
				}
			
			return false;
			*/
		});
		
	};
	
	$.fn.supersized.defaults = { 
			startwidth: 640,  
			startheight: 480,
			minsize: .5,
			slideshow: 1,
			slideinterval: 5000  
	};
	
})(jQuery);

