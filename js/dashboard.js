$(function() {
    $('#upload_file').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
            url             :base_url+'uploads/', 
            secureuri       :false,
            fileElementId   :'userfile',
            dataType: 'JSON',
            success : function (data)
            {
               var obj = jQuery.parseJSON(data);                
                if(obj['status'] == 'success')
                {
                    $('#files').html(obj['msg']);
                 }
                else
                 {
                    $('#files').html('Some failure message');
                  }
            }
        });
        return false;
    });
});

function checkAddUser(){
    var userEmail=$("#addUser_email").val();
    $('#adduserHtml').html('Please Wait...');
    $.ajax({
        url:base_url+"user/adduserlist/",
        type: 'POST',
        data: 'useremail='+userEmail,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#adduserHtml').html(msg);
        }    			
    }); 
}

function showregistration(){
    $('#register_choice').hide();
    $('#user_registration').show();
    positionPopup('addUserDiv');
}

function send_userRegistration_email(){
    var userEmail=$("#email_hidden").val();
    $('#adduserHtml').html('Please Wait...');
    $.ajax({
        url:base_url+"user/adduserlist/email",
        type: 'POST',
        data: 'useremail='+userEmail,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#adduserHtml').html(msg);
            location.reload();
       }    			
    }); 
}

function chkNewUserSignup(){
    var userEmail=$("#new_email_address").val();
    var first_name=$("#new_first_name").val();
    var last_name=$("#new_last_name").val();
    var password=$("#new_password").val();
    var stage=$("#new_stage").val();
    $('#adduserHtml').html('Please Wait...');
    $.ajax({
        url:base_url+"user/adduserlist/newuser",
        type: 'POST',
        data: 'email_address='+userEmail+'&first_name='+first_name+'&last_name='+last_name+'&password='+password+'&stage='+stage,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#adduserHtml').html(msg);
            location.reload();
        }    			
    });
}

function changeRole(val, id){
    $.ajax({
        url:base_url+"user/changerole",
        type: 'POST',
        data: 'role_value='+val+'&userid='+id,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#roleChange_msg').show();
            $('#roleChange_msg').html(msg);
        }    			
    });
}

function changeProjectUserRole(val, id){
    var projectid=$('#projectid_hidden').val();
    $.ajax({
        url:base_url+"user/changerole",
        type: 'POST',
        data: 'role_value='+val+'&userid='+id+'&projectid='+projectid,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#roleChange_msg').show();
            $('#roleChange_msg').html(msg);
        }    			
    });
}

function chkaddProject(){
    var identifier=$("#identifier").val();
    var title=$("#title").val();
    var identifier_msg=$("#identifier_msg").html();
    if(identifier==''){
        $("#identifier").css("border", "1px solid red");
        $("#identifier").focus();
        return false;
    }
    else{
        $("#identifier").css( "border", "none" );
    }
    if(identifier_msg!="Available"){

            $("#identifier").css("border", "1px solid red");
            $("#identifier").focus();
            return false;
    }
    else{
            $("#identifier").css( "border", "none" );

    }
    if(title==''){
        $("#title").css("border", "1px solid red");
        $("#title").focus();
        return false;
    }
    else{
        $("#title").css( "border", "none" );
    }
    
}

function chkupdateProject(){
    var identifier=$("#edit_identifier").val();
    var title=$("#edit_title").val();
    var identifier_msg=$("#edit_identifier_msg").html();
    if(identifier==''){
        $("#edit_identifier").css("border", "1px solid red");
        $("#edit_identifier").focus();
        return false;
    }
    else{
        $("#edit_identifier").css( "border", "none" );
    }
    
    if(title==''){
        $("#edit_title").css("border", "1px solid red");
        $("#edit_title").focus();
        return false;
    }
    else{
        $("#edit_title").css( "border", "none" );
    }
}

function check_identifier(){
    $("#identifier_msg").show();
    var identifier= $('#identifier').val();
    $.ajax({
        url:base_url+"user/projectIdentifier/",
        type: 'POST',
        data: 'identifier='+identifier,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#identifier_msg').html(msg);
        }    			
    });
}


function check_edit_identifier(){
    $("#edit_identifier_msg").show();
    var identifier= $('#edit_identifier').val();
    $.ajax({
        url:base_url+"user/projectIdentifier/",
        type: 'POST',
        data: 'identifier='+identifier,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#identifier_msg').html(msg);
        }    			
    });
}

function setBlogHeight(){
    var ch=$('#content').height();
    var wh=$(window).height();
    var bh=wh-ch;
    if(bh>=150){
        $('#blog_list').css('height', bh+'px');
    }
}


(function($){
    $(window).load(function(){
        $(".projects_container").mCustomScrollbar({
                horizontalScroll:true,
                scrollButtons:{
                        enable:true
                },
        });
        
        $(".project_participated").mCustomScrollbar({
                horizontalScroll:true,
                scrollButtons:{
                        enable:true
                },
        });
        
        $("#recent_content").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });
        
        $("#user_list").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });
        
        $("#blog_list").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });
        
        
        //arun comment
        showSearchResult();
        setBlogHeight();
    })
})(jQuery);

$(window).resize(function() {
    if($('#addUserDiv').is(':visible')){
        positionPopup('addUserDiv');
    }
    
    if($('#addProject_div').is(':visible')){
        positionPopup('addProject_div');
    }
    
    if($('#edit_profile').is(':visible')){
        positionPopup('edit_profile');
    }
    
    if($('#addUserProject').is(':visible')){
        positionPopup('addUserProject');
    }
    
    if($('#editprojectDiv').is(':visible')){
        positionPopup('editprojectDiv');
    }
    
    if($('#addBlogComment').is(':visible')){
        positionPopup('addBlogComment');
    }
    
    if($('#editBlogCommentDiv').is(':visible')){
        positionPopup('editBlogCommentDiv');
    }
    
    if($('#blog_list').is(':visible')){
        setBlogHeight();
    }
    //resizeSearchDiv();
    
});

function addUserProject(userid, action){
    var projectId=$('#projectid_hidden').val();
    if(action=='add'){
        var url=base_url+"project/adduserproject/"+userid+"/"+projectId
    }
    else{
        var url=base_url+"project/removeuserproject/"+userid+"/"+projectId
    }
    $.ajax({
        url:url,
        type: 'POST',
        cache: false,
        global: false,
        success:function(msg){ 
            $('#userProjectmsg').html(msg);
        }    			
    });
}

function adduserpoject(id){
    var name=$('#myuser_nameDiv_'+id).html();
    $('#myuser_nameDiv_'+id).css('background', '#666666');
    $('#myuser_linkDiv_'+id).css('background', '#666666');
    $('#myuser_linkDiv_'+id).html('&nbsp');
    $('#rightholder').append('<div class="nameholder" id="projectuser_nameDiv_'+id+'">'+name+'</div>'+
                            '<div class="linkholder" id="projectuser_linkDiv_'+id+'"><a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="removeuserpoject('+id+')">remove user &gt;</a></div>');
    addUserProject(id, 'add');
}

function removeuserpoject(id){
    $('#myuser_nameDiv_'+id).css('background', '');
    $('#myuser_linkDiv_'+id).css('background', '');
    $('#myuser_linkDiv_'+id).html('<a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="adduserpoject('+id+')">add user &gt;</a>');
    $('#projectuser_nameDiv_'+id).remove();
    $('#projectuser_linkDiv_'+id).remove();
    addUserProject(id, 'remove');
}

function showhideDivData(id, linkid){
     if($('#'+id).is(':visible')){
         $('#'+id).slideUp();
         $('#'+linkid).removeClass('but_down');
         $('#'+linkid).addClass('but_right');
     }
     else{
         $('#'+id).slideDown();
         $('#'+linkid).removeClass('but_right');
         $('#'+linkid).addClass('but_down');
     }
}

function viewProjectbyRole(){
    var roleid=$('#project_role').val();
    var roleid2=$('#project_role2').val();
    window.location.href=base_url+'project/role/'+roleid+'/'+roleid2;
}

function removeUser(userid){
    var r = confirm("Are you sure to remove this user?");
    if (r == true) {
        $('#userrow'+userid).remove();
        $.ajax({
            url:base_url+"user/remove/",
            type: 'POST',
            data: 'userid='+userid,
            cache: false,
            global: false,
            success:function(msg){ 
               // $('#identifier_msg').html(msg);
            }    			
        });
    } else {
        return false;
    }
}

function showSearch(){
    if($('#searchDiv').is(':visible')){
        $('#searchDiv').slideUp();
        $('#recent_content').css('top', '228px');
        $('#searchtxt').removeClass('but_down');
        $('#searchtxt').addClass('but_right');
    }
    else{
        $('#searchDiv').slideDown();
        $('#recent_content').css('top', '258px'); 
        $('#searchtxt').removeClass('but_right');
        $('#searchtxt').addClass('but_down');
    }
}

function showSearchResult(){
    var input_search=$('#input_search').val();
    var identifier_project=$('#identifier_project').val();
    var itemtype_filter=$('#itemtype_filter').val();
    var search_limit=$('#search_limit').val();
    var search_order=$('#search_order').val();
    //arun comment
    $('#recent_content').html('<img style="position:absolute; z-index:15; top:20%; left:25%" src="'+base_url+'images/ajax-loader.gif">');
    $.ajax({
        url:base_url+"project/search/",
        type: 'POST',
        data: 'search_txt='+input_search+'&project='+identifier_project+'&type='+itemtype_filter+'&show='+search_limit+'&sort='+search_order,
        cache: false,
        global: false,
        success:function(msg){ 
           var myArray = jQuery.parseJSON(msg);
            if(myArray.length>0){

                //var dropdown='';
                var html='';
                for(var i=0; i<myArray.length;i++){

                    //dropdown+='<option value="'+myArray[i]['institute_id']+'" '+selected+'>'+myArray[i]['name']+'</option>';
                    if(myArray[i]['thumbnail']==''){
                        var thumb=base_url+'images/popup_pic1.jpg';
                    }
                    else{
                        var n = myArray[i]['thumbnail'].indexOf("http");
                        if(n>-1){
                            var thumb=myArray[i]['thumbnail'];
                        }
                        else{
                            var thumb=base_url+'uploads/'+myArray[i]['thumbnail'];
                        }                        
                    }
                    
                    html+='<div class="item">';
                    html+='<a onfocus="this.blur();" href="'+base_url+'project/'+myArray[i]['type']+'/'+myArray[i]['project_id']+'/'+myArray[i]['albumid']+'/'+myArray[i]['id']+'"><img width="98" height="98" class="floatLeft" alt="" src="'+thumb+'"></a>';
                    html+='<div class="floatLeft">';
                    html+='<b>'+myArray[i]['type']+'</b>';
                    html+='<div class="nooverflow">'+myArray[i]['title']+'</div>';
                    html+='<div class="nooverflow">'+myArray[i]['added_date']+'</div>';
                    html+='by User: '+myArray[i]['stage_name']+'<br> <div class="seen_'+myArray[i]['seen']+'">Seen:</div>Project Id: '+myArray[i]['identifier']+'<br></div>';
                    html+='<div class="clear"></div>';
                    html+='</div>';
                }
                //arun comment
                
                $("#recent_content").html(html);    
                $("#recent_content").mCustomScrollbar({
                        scrollButtons:{
                                enable:true
                        }
                });
                

            }
            else{
                //arun comment
                $("#recent_content").html('no record found');
            }
        }    			
    });
    
}

function handleEvent(e){
    var key=e.keyCode || e.which;
    if (key==13){
       showSearchResult();
    }

    return false;
}

function resizeSearchDiv(){
    var h=$('#userdetails').height();
    var wh=$('#content').height();
    var height=wh-h;
    $('#recent_content').css('height', height+'px');
}

function removeblogcomment(commentid, projectid){
    var r = confirm("Are you sure to remove this comment?");
    if (r == true) {
        $.ajax({
            url:base_url+"project/removeBlogComment/",
            type: 'POST',
            data: 'commentid='+commentid,
            cache: false,
            global: false,
            success:function(msg){ 
                window.location.href=base_url+'project/view/'+projectid;
            }    			
        });
    } else {
        return false;
    }
}

function getNewBlogComments(commentid, projectid){
    $.ajax({
        url:base_url+"project/getNewBlogComment/",
        type: 'POST',
        data: 'commentid='+commentid+'&projectid='+projectid,
        cache: false,
        global: false,
        success:function(msg){ 
            var myArray = jQuery.parseJSON(msg);

            if(myArray.length>0){

                //var dropdown='';
                var html='';
                for(var i=0; i<myArray.length;i++){
                    html+='<tr id="userrow'+myArray[i]['id']+'">';
                    html+='<td style="vertical-align: top;" class="whiteText">'+myArray[i]['stage_name']+'</td>';
                    html+='<td style="vertical-align: top;" class="emailcolor">'+myArray[i]['comment']+'</td>';
                    html+='<td style="vertical-align: top;">'+myArray[i]['added_date']+'</td>';
                    html+='</tr>';
                    var lastid=myArray[i]['id'];
                }
            }
            else{
                var lastid=commentid;
            }
            setTimeout("getNewBlogComments("+lastid+", "+projectid+")", 2000);
            $('#trheading').after(html);
            //window.location.href='/project/view/'+projectid;
        }    			
    });
}

function showEditComment(commentid, projectid){
    $.ajax({
        url:base_url+"project/editComment/",
        type: 'POST',
        data: 'commentid='+commentid+'&projectid='+projectid,
        cache: false,
        global: false,
        success:function(msg){ 
            var myArray = jQuery.parseJSON(msg);

            if(myArray.length>0){
                $('#commentid_hidden').val(commentid);
                $('#comment_edit').val(myArray[0]['comment']);
                CKEDITOR.instances['comment_edit'].setData(myArray[0]['comment']);
                openDiv('editBlogCommentDiv');
                /*
                //var dropdown='';
                var html='';
                for(var i=0; i<myArray.length;i++){
                    html+='<tr id="userrow'+myArray[i]['id']+'">';
                    html+='<td style="vertical-align: top;" class="whiteText">'+myArray[i]['stage_name']+'</td>';
                    html+='<td style="vertical-align: top;" class="emailcolor">'+myArray[i]['comment']+'</td>';
                    html+='<td style="vertical-align: top;">'+myArray[i]['added_date']+'</td>';
                    html+='</tr>';
                    var lastid=myArray[i]['id'];
                }
                */
            }
            
            //window.location.href='/project/view/'+projectid;
        }    			
    });
}