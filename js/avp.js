function albumheight(){
    var album_height=$('#album_bottom').offset().top-$('#album_top').offset().top;
    album_height=album_height-19;
    $('#centerlisting_albums').css('height', album_height+'px')
}

function imagecontainerwidth(){
    var num=$('.imagecontainer').length;
    var width=num*161;
    $('#album_images').css('width', width+'px');
}

function videoContainerWidth(){
    var w=$('#videos').width();
    var width=w-200;
    $('#project_videos').css('width', width+'px');
}

function fileIframeResize(){
    var h=$('#outerCenterImg').height();
    var h=h-38;
    $('#file_iframe').css('height', h+'px')
}

function imagecontainerheight(){
    var height=$('#outerCenterImg').height();
    
    if($('#flashvideoplayer').length){
        height=height-38;
        $('#flashvideoplayer').css('height', height+'px');
    }
    else if($('#centerLage_image').length){
        height=height-38;
        $('#centerLage_image').css('height', height+'px');
    }
    else if($('#filesview_default').length){
        height=height-38;
        $('#filesview_default').css('height', height+'px');
    }
    
}

function chktxt(){
    var divWidth=$('#outdiv').width();
    var inWidth=$('#inspan').width();
    var maxw=divWidth-20;
    var html=$('#inspan').html();
    var alt=$('#inspan').attr('alt');
    var length=alt.length;
    var calc=Math.floor(divWidth/10);
    //Math.floor(1.6);
    var show= alt.substr(0,calc);
    if(calc<length){

        show=show+"...";
       // $('#inspan').html(show);
    }
    else{
         show=show;
        //$('#inspan').html(html);
    }
    $('#inspan').html(show);
}
    
$(document).ready(function(){
   albumheight();
   imagecontainerwidth();
   imagecontainerheight();
   show_video();
   fileIframeResize();
   //chktxt();
   //videoContainerWidth();
});

$(window).resize(function(){
    albumheight();
    imagecontainerheight();
    fileIframeResize();
    //chktxt();
    //videoContainerWidth();
    if($('#add_image_popup').is(':visible')){
        positionPopup('add_image_popup');
    } 
    
    if($('#edit_album_image').is(':visible')){
        positionPopup('edit_album_image');
    } 
    
    if($('#add_album').is(':visible')){
        positionPopup('add_album');
    } 
    
    if($('#edit_album_form').is(':visible')){
        positionPopup('edit_album_form');
    } 
    
    if($('#addalbumCommentDiv').is(':visible')){
        positionPopup('addalbumCommentDiv');
    }
    
    if($('#editCommentDiv').is(':visible')){
        positionPopup('editCommentDiv');
    }
    
    if($('#sorting_album').is(':visible')){
        positionPopup('sorting_album');
    }
    show_video;
}); 

  (function($){
    $(window).load(function(){
        $("#images").mCustomScrollbar({
                horizontalScroll:true,
                scrollButtons:{
                        enable:true
                },
        });

        $("#centerlisting_albums").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });

        $("#right_comments").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });
        
        $("#project_videos").mCustomScrollbar({
                scrollButtons:{
                        enable:true
                }
        });
    })
})(jQuery);
function getPageSize(){
    var xScroll, yScroll;	
    if (window.innerHeight && window.scrollMaxY) {	
            xScroll = document.body.scrollWidth;
            yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
            xScroll = document.body.scrollWidth;
            yScroll = document.body.scrollHeight;
    } else { 
            xScroll = document.body.offsetWidth;
            yScroll = document.body.offsetHeight;
    }	
    var windowWidth, windowHeight;
    if (self.innerHeight) {	
            windowWidth = self.innerWidth;
            windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { 

        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { 

        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }		

    if(yScroll < windowHeight){
            pageHeight = windowHeight;
    } else { 
            pageHeight = yScroll;
    }

    if(xScroll < windowWidth){	
            pageWidth = windowWidth;
    } else {
            pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
    return arrayPageSize;
}
   
function placePlayer(preFilePath){
  
  $('#centerLage_image').html('<img id="imagescaler" src="'+preFilePath+'" />')
  $('#imagescaler').supersized();             
}

function removealbum(albumid, table){
    var r = confirm("Are you sure to remove this?");
    var url=base_url+'project/'+table+'/'+curr_project;
    /*
    if(table=='images'){
        var url=base_url+'project/'+table+'/'+curr_project;
    }
    else if(table=='videos'){
        var url=base_url+'project/videos/'+curr_project;
    }
    else if(table=='files'){
        var url=base_url+'project/files/'+curr_project;
    }
    else if(table=='webpages'){
        var url=base_url+'project/files/'+curr_project;
    }
    */
    if (r == true) {
        $.ajax({
            url:base_url+"project/removealbum/",
            type: 'POST',
            data: 'albumid='+albumid+"&table="+table,
            cache: false,
            global: false,
            success:function(msg){ 
                window.location.href=url;
            }    			
        });
    } else {
        return false;
    }
}

function removecomment(type, commentid){
    var r = confirm("Are you sure to remove this comment?");
    if (r == true) {
        $.ajax({
            url:base_url+"project/removecomment/",
            type: 'POST',
            data: 'commentid='+commentid,
            cache: false,
            global: false,
            success:function(msg){ 
                window.location.href=base_url+'project/'+type+'/'+curr_project+'/'+curr_album;
            }    			
        });
    } else {
        return false;
    }
}

function removealbumimage(type, imageid){
    var r = confirm("Are you sure to remove this?");
    if (r == true) {
        $.ajax({
            url:base_url+"project/removealbumimage/",
            type: 'POST',
            data: 'imageid='+imageid+'&type='+type,
            cache: false,
            global: false,
            success:function(msg){ 
                window.location.href=base_url+'project/'+type+'/'+curr_project+'/'+curr_album;
            }    			
        });
    } else {
        return false;
    }
}

function add_imageonly(){
    $('#image_version').val('no');
    $('#imageversion').removeClass('on');
    $('#image_only').removeClass('off');
    $('#imageversion').addClass('off');
    $('#image_only').addClass('on');
}

function add_imageversion(){
    $('#image_version').val('yes');
    $('#imageversion').removeClass('off');
    $('#image_only').removeClass('on');
    $('#imageversion').addClass('on');
    $('#image_only').addClass('off');
}

function showlargeImg(id){
    var alt = $('#'+id).attr("alt");
    placePlayer(base_url+'uploads/'+alt);
}

function copyField(){
    //window.alert('copyfield');
    $('#fakefilefield').val($('#fileselecter').val());
}

function deleteField(){
  //window.alert('deletefield');
  $('#fakefilefield').val('');
  $('#fileselecter').val('');
}


function saveAlbumImagesOrder(){
    var liIds = $('#sortbox_album_images li').map(function(i,n) {
        return $(n).attr('id');
    }).get().join(',');
    $.ajax({
        url:base_url+"project/sortalbumsimages/",
        type: 'POST',
        data: "id="+liIds,
        cache: false,
        global: false,
        success:function(msg){ 
            window.location.href=base_url+'project/images/'+curr_project+'/'+curr_album;
        }    			
    });
}

function show_video(){
    if (document.getElementById("flashvideoplayer")){
         // document.getElementById("flashvideoplayer").style.height = heightonebar+"px";
          //workaround for rescaling and swf-placement after rescaling is done
          var so = new SWFObject(base_url+'includes/mediaplayer.swf','flashvideoplayerembeded','100%','100%','9');
          so.addParam('allowfullscreen','true');
          so.addParam("wmode", "transparent");
          so.addParam('flashvars','skin='+base_url+'includes/skin_avpmatrix.swf&file=playlist.php&autostart=false&controlbar=bottom&repeat=false&shuffle=false&overstretch=false&logo='+base_url+'images/kruegerVP.png');
          so.write('flashvideoplayer');          
        }
        if (document.getElementById("flashvideoplayer2")){
         // document.getElementById("flashvideoplayer2").style.height = heightonebar+"px";
          //workaround for rescaling and swf-placement after rescaling is done
          var so = new SWFObject('/includes/mediaplayer.swf','flashvideoplayerembeded','100%','100%','9');
          so.addParam('allowfullscreen','true');
          so.addParam("wmode", "transparent");
          so.addParam('flashvars','skin='+base_url+'includes/skin_avpmatrix.swf&file=playlist.php&autostart=true&controlbar=bottom&repeat=false&shuffle=false&overstretch=false&logo='+base_url+'images/kruegerVP.png');
          so.write('flashvideoplayer2');          
        }
}

function changeThumbnail(thumbImage, type=''){
    if(type == ''){
        thumbImage = base_url+'uploads/'+thumbImage;
    }

    if(thumbImage!=''){
        var thumbnail='<img width="190" height="190" id="sideimage_3653" style="display:block;" class="buttomsideimage" src="'+thumbImage+'">';
    }
    else{
        var thumbnail='<img width="190" height="190" id="sideimage_3653" style="display:block;" class="buttomsideimage" src="'+base_url+'images/popup_pic1.jpg">';
    }
    $('#videoThumbnail').html(thumbnail);
}

function showComments(id){
    $('.showCommentsVideo').hide();
    $('#commentsVideo'+id).show();
}

function openwebpage(url, height, width){
    var params = [
        //'height='+screen.height,
        //'width='+screen.width,
        'height='+height,
        'width='+width,
        'scrollbars=yes',
        'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');
         // and any other options from
         // https://developer.mozilla.org/en/DOM/window.open

    var popup = window.open(url, 'popup_window', params); 
    popup.moveTo(0,0);
}