//position the popup at the center of the page
function positionPopup(id){
  if(!$("#"+id).is(':visible')){
    return;
  } 
  $("#"+id).css({
      left: ($(window).width() - $('#'+id).width()) / 2,
      top: ($(window).height() - $('#'+id).height()) / 2,
      position:'fixed'
  });
}


function openDiv(divId){
  $("#"+divId).fadeIn(500);
  positionPopup(divId);
}


function closeDiv(divId){
    $("#"+divId).fadeOut(500);
}

function chk_username(){
    $("#username_msg").show();
    var user_name= $('#user_name').val();
    $.ajax({
            url:base_url+"user/checkusername/",
            type: 'POST',
            data: 'username='+user_name,
            cache: false,
            global: false,
            success:function(msg){ 
                $('#username_msg').html(msg);
            }    			
        });
}

function chk_useremail(){
    $("#useremail_msg").show();
    var email_address= $('#email_address').val();
    $.ajax({
            url:base_url+"user/checkuseremail/",
            type: 'POST',
            data: 'useremail='+email_address,
            cache: false,
            global: false,
            success:function(msg){ 
                $('#useremail_msg').html(msg);
            }    			
        });
}


function chkSignup(){
	//alert('ss');
	var nameReg = /^\s*[a-zA-Z0-9\s]+\s*$/;
	var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var fname=$("#first_name").val();
        var lname=$("#last_name").val();
        var email=$("#email_address").val();
	var pass=$("#password").val();
        var cpass=$("#con_password").val();
      //  $("#username_msg").show();
        if(email!='')
            $("#useremail_msg").show();
       // var username_msg=$("#username_msg").html();
        var email_msg=$("#useremail_msg").html();
        var stage=$("#stage").val();
      
	if(fname == ''){
		$("#first_name").css("border", "1px solid red");
		$("#first_name").focus();
		return false;
	}
	else{
		$("#first_name").css("border", "none");
		
	}
        
        if(lname == ''){
		$("#last_name").css("border", "1px solid red");
		$("#last_name").focus();
		return false;
	}
	else{
		$("#last_name").css("border", "none");
		
	}
	
	if(email == '' || emailReg.test(email) == false){
		$("#email_address").css("border", "1px solid red");
		$("#email_address").focus();
		return false;
	}
	else{
		$("#email_address").css("border", "none");
	}
	
	if(pass == ''){
		$("#password").css("border", "1px solid red");
		$("#password").focus();
		return false;
	}
	else{
		$("#password").css("border", "none");
	}
	
	if(cpass == ''){
		$("#con_password").css("border", "1px solid red");
		$("#con_password").focus();
		return false;
	}
	else{
		$("#con_password").css("border", "none");
	}
	
	if(cpass != pass){
		$("#con_password").css("border", "1px solid red");
		$("#con_password").focus();
		return false;
	}
	else{
		$("#con_password").css("border", "none");
	}
	
        if(stage == ''){
		$("#stage").css("border", "1px solid red");
		$("#stage").focus();
		return false;
	}
	else{
            $("#stage").css("border", "none");
		
	}
	/*
        if(username_msg!="Available"){
                $("#user_name").css("border", "1px solid red");
                $("#user_name").focus();
                return false;
        }
        else{
                $("#user_name").css("border", "none");

        }
        */
        
        if(email_msg!="Available"){
            
                $("#email_address").css("border", "1px solid red");
                $("#email_address").focus();
                return false;
        }
        else{
                $("#email_address").css("border", "none");

        }
	
}


function chkProfile(){
    var nameReg = /^\s*[a-zA-Z0-9\s]+\s*$/;
    var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var fname=$('#first_name').val();
    var lname=$('#last_name').val();
    var stage=$('#stage').val();
    var email=$("#email_address").val();
    $("#useremail_msg").show();
    var email_msg=$("#useremail_msg").html();
    if(fname == ''){
        $("#first_name").css("border", "1px solid red");
        $("#first_name").focus();
        return false;
    }
    else{
        $("#first_name").css("border", "none");
    }
    
    if(lname == ''){
        $("#last_name").css("border", "1px solid red");
        $("#last_name").focus();
        return false;
    }
    else{
        $("#last_name").css("border", "none");
    }
    
    if(email == '' || emailReg.test(email) == false){
        $("#email_address").css("border", "1px solid red");
        $("#email_address").focus();
        return false;
    }
    else{
        $("#email_address").css("border", "none");
    }
	
    if(stage == '' || stage == null){
        $("#stage").css("border", "1px solid red");
        $("#stage").focus();
        return false;
    }
    else{
        $("#stage").css("border", "none");
    }
    
    if(email_msg!="Available"){
        $("#email_address").css("border", "1px solid red");
        $("#email_address").focus();
        return false;
    }
    else{
        $("#email_address").css("border", "none");

    }
        
        
}

function resizeUI(id) {
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        //$('#'+id).css('top',  winH/2-$('#dialog').height()/2);
        //$('#'+id).css('left', winW/2-$('#dialog').width()/2);               
        $("#"+id).css({
            //left: ($(window).width() - $('#'+id).width()) / 2,
            //top: ($(window).height() - $('#'+id).height()) / 2,
            //top : ($(window).height() - $('#content').outerHeight()) / 2
        });
        var a=winH/2-$('#wrapper').height()/2;
        a=a-(a*30/100);
       // alert(a);
        $('#'+id).css('margin-top',  a+'px');
}

function buySubscription(id){
    $('#cover').show();
    $('#loader_img').show();
    positionPopup('loader_img');
    
    $.ajax({
        url:base_url+"subscription/getdetails/",
        type: 'POST',
        data: 'id='+id,
        cache: false,
        global: false,
        success:function(msg){ 
            $('#cover').hide();
            $('#loader_img').hide();
            var myArray = jQuery.parseJSON(msg);
            if(myArray.length>0){
                for(var i=0; i<myArray.length;i++){
                    $('#pack_plan').html(myArray[i]['name']);
                    $('#pack_traffic').html(myArray[i]['traffic_storage']+' GB');
                    $('#pack_users').html(myArray[i]['users']);
                    $('#subscriptionId').val(myArray[i]['id'])
                    if(myArray[i]['price']=='0'){
                        $('#pack_amount').html('Free');
                        $('#pack_validity').html('30 Day Trial:');
                    }
                    else{
                        $('#pack_amount').html('$ '+myArray[i]['price']);
                        $('#pack_validity').html('30 Day Charges:');
                    }
                }
                openDiv('formdiv');
            }
        }    			
    });
    
}

 $(document).ready(function(){
        $('#select_all_chk').click(function(){
            if($('#select_all_chk').is(':checked')){
                $(".checkbox").attr ( "checked" ,"checked" );
            }
            else
            {
                $(".checkbox").removeAttr('checked');
            }
        });
        
        $('input:checkbox').click(function(){
            if($('.checkbox:checked').length>0){
                $("#deletechk").removeAttr('disabled');
            }
            else
            {
                $("#deletechk").attr ( "disabled" ,"disabled" );
            }
        });
        
        $('#deletechk').click(function(){
            if($("#deletechk:not(:disabled)")){
                $('#fileManageForm').submit();
            }
        });
    })
    
            
            
$(window).resize(function() {
    if($('#formdiv').is(':visible')){
        positionPopup('formdiv');
    } 
    if($('#registration_success').is(':visible')){
        positionPopup('registration_success');
    } 
});


function saveOrder(type){       
    $('.popup_bottom a').html('saving...').attr('disabled','1');
    if(type=='projects'){
        var liIds = $('#sortprojectbox li').map(function(i,n) {
            return $(n).attr('id');
        }).get().join(',');
    }
    else{
        var liIds = $('#sortbox li').map(function(i,n) {
            return $(n).attr('id');
        }).get().join(',');
    }
    
    $.ajax({
        url:base_url+"project/sortalbums/",
        type: 'POST',
        //data: $('#sortbox').sortable("serialize"),
        data: "id="+liIds+'&type='+type,
        cache: false,
        global: false,
        success:function(msg){
            if(type=='projects'){
               window.location.reload();
            }else{
                if(type == 'user_files')
                    type = 'files';
                if(type == 'user_videos')
                    type = 'videos';
                window.location.href=base_url+'project/'+type+'/'+curr_project;
            }
        }    			
    });
}