<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * profile model class file
 *
 */
class profile_model extends CI_Model {
    
    public function __construct()
    {
        /*
         * call the constructor of CI
         */
        parent::__construct();
    }
    
    /**
     * Function for activate the user's profile
     * @param string $userid user's primary id from "users" table
     *
     * @access public
     * 
     **/  
    public function activeProfile($userid)
    {
            $data = array(
           'is_active' => '1',
            );

            $this->db->where('id', $userid);
            $this->db->update('users', $data); 

    }

    /**
     * Function for check user's email from "users" table
     * @param string $userid user's primary id from "users" table
     *
     * @access public
     * 
     * @return user's email
     **/  
    public function userEmail($userid) {
        $email = $this->db->select('email')->get_where('users', array('id' => $userid))->row()->email;
        

        return $email;
    }
    
    /**
     * Function for complete user's profile who is added by user in his/her user's list through only email
     *
     * @access public
     * 
     **/  
    public function complete_userProfile() {
        $data=array(
                'user_name'=>$this->input->post('email_address'),
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'company_name'=>$this->input->post('company'),
                'email'=>$this->input->post('email_address'),
                'password'=>md5($this->input->post('password')),
                'stage_name'=>$this->input->post('stage'),
                );
        $this->db->where('id', $this->input->post('userid_hidden'));
        $this->db->where('email', $this->input->post('email_address'));
        $this->db->update('users',$data);
    }
}
?>