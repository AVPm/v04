<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * user model class file
 *
 */

class User_model extends CI_Model {
    
    public function __construct()
    {
        /*
         * call the constructor of CI
         */
        parent::__construct();
    }
    
    /**
     * Function for User Signup
     * @param string $email email of user
     * @param string $password password of user
     *
     * @access public
     * 
     * @return TRUE/FALSE if username and password matches then TRUE and if not then false
     **/  
	function login($email,$password)
    {
		$this->db->where("user_name",$email);
                $this->db->where("password",$password);
                $this->db->where("is_active", '1');
            
        $query=$this->db->get("users");
        if($query->num_rows()>0)
        {
         	foreach($query->result() as $rows)
            {
            	//add all data to session
                $newdata = array(
                	   	'user_id' 		=> $rows->id,
                    	'user_name' 	=> $rows->user_name,
		                'user_email'    => $rows->email,
	                    'logged_in' 	=> TRUE,
                   );
			}
            	$this->session->set_userdata($newdata);
                return true;            
		}
		return false;
    }
    
    /**
     * Function for User Signup
     *
     * @access public
     * Save all information of user in "users" tabel and send an activation email to user
     **/  
	public function add_user()
	{
		$data=array(
			'user_name'=>$this->input->post('email_address'),
                        'first_name'=>$this->input->post('first_name'),
                        'last_name'=>$this->input->post('last_name'),
                        'company_name'=>$this->input->post('company'),
			'email'=>$this->input->post('email_address'),
			'password'=>md5($this->input->post('password')),
                        'stage_name'=>$this->input->post('stage'),
                        'created_date'=>date('Y-m-d H:i:s')
			);
                
                // add user in "users" table
		$this->db->insert('users',$data);
                
                // get primary id of last inserted user from "users" table
                $insert_id = $this->db->insert_id();
                
                //load email library for send account activation email to user
                $this->load->library('email');
                $config['mailtype'] = 'html';
                $config['protocol'] = 'sendmail';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $this->email->initialize($config);
                $this->email->from('arun@avpmatrix.com', 'Arun');
                $this->email->to($this->input->post('email_address')); 

                $this->email->subject('Account Activation');
                $this->email->message('please activate your account. <a href="http://v04.avpmatrix.com/profile/activate/'.$insert_id.'">Activate</a>');	

                $this->email->send();
                
                $newdata = array(
                	   	'user_registration'=> 'success',
                    	
                   );
            	$this->session->set_userdata($newdata);
                return $insert_id;
        }
        
        /**
        * Function for add user in his/her user's list when user account is already signup in site
        *
        * @access public
        * 
        * @return insert id of user if user add successfully, OR false if user is not added successfully
        **/  
        public function adduser_list() {
            $user_email = $this->input->post('useremail');
            $this->db->select('id');
            $this->db->where("email",$user_email);
            $this->db->limit(1);
            $query=$this->db->get("users");
            $userData=  array();
            //check if users account is already created or not, if created then information is save in "user_relation" table
            if($query->num_rows()>0)
            {
                $resArray=$query->result_array();
               
                foreach ($resArray as $res){
                    $data = array(
                        'parent_userid' => $this->session->userdata('user_id'),
                        'child_userid' => $res['id'],
                        'added_date' => date('Y-m-d H:i:s'),
                     );
                }
                $this->db->insert('user_relation',$data);
                $insert_id = $this->db->insert_id();
                $this->addSales($insert_id, '1');
                return $insert_id;
            }
            else{
                return false;
            }
            
        }
        /**
        * Function for add user in his/her user's list when user account is not avaliable in site and save user information is "users" table
        *
        * @access public
        * 
        **/
        public function addnewuser_list() {
            $data = array(
                        'user_name'=>$this->input->post('email_address'),
                        'first_name'=>$this->input->post('first_name'),
                        'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('email_address'),
			'password'=>md5($this->input->post('password')),
                        'stage_name'=>$this->input->post('stage'),
                        'is_active'=>'1',
                     );
            //first information is save in "users" table
            $this->db->insert('users',$data);
            
            //get primary id from "users" table
            $insert_id = $this->db->insert_id();
            $data = array(
                        'parent_userid' => $this->session->userdata('user_id'),
                        'child_userid' => $insert_id,
                        'added_date' => date('Y-m-d H:i:s'),
                     );
            
            //now user relation is save in user_relation table
            $this->db->insert('user_relation',$data);
        }
        
        /**
        * Function for add user in his/her user's list when user account is not available in site and just add email of user in "users" table
        *
        * @access public
        * 
        * @return insert id of user if user add successfully, OR false if user is not added successfully
        **/
        public function addnewuseremail_list() {
            $data = array(
			'email'=>$this->input->post('useremail'),
                        'is_active'=>'1',
                     );
            $this->db->insert('users',$data);
            $insert_id = $this->db->insert_id();
            $data = array(
                        'parent_userid' => $this->session->userdata('user_id'),
                        'child_userid' => $insert_id,
                        'added_date' => date('Y-m-d H:i:s'),
                     );
            $this->db->insert('user_relation',$data);
            return $insert_id;
        }
        
        /**
         * Function for check username in "users" tabe
         *
         * @param string $uname username in signup form
         * 
         * @access public
         * 
         * @return string if username is not find in "users" table then "Available" and if find then "Not Available" 
        **/  
        
        public function check_username($uname) {
            $this->db->where("user_name",$uname);
            
            $query=$this->db->get("users");
            if($query->num_rows()>0)
            {
                return 'Not Available';
            }
            else{
                return 'Available';
            }
        }
        
        /**
         * Function for check email in "users" tabe
         *
         * @param string $email user's email in signup form
         * 
         * @access public
         * 
         * @return string if email is not find in "users" table then "Available" and if find then "Not Available" 
        **/
        public function check_useremail($email) {
            $this->db->where("email",$email);
            if($this->session->userdata('user_id')!=''){
                $this->db->where("id !=",$this->session->userdata('user_id'));
            }
            $query=$this->db->get("users");
            /* after we've made the queries from the database, we will store them inside a variable called $data, and return the variable to the controller */
            if($query->num_rows()>0 || !filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                return 'Not Available';
            }
            else{
                return 'Available';
            }
        }
        
        /**
         * Function for update profile of user
         *
         * @access public
         * 
        **/
        public function update_Profile() {
            $newPass = $this->input->post('new_pass');
            $currPass = $this->input->post('curr_pass');
            
            //check old password
            $oldPass = $this->db->select('password')->get_where('users', array('id' => $this->session->userdata('user_id')))->row()->password;
            
            //check if old password and current password is match then password is also updated
            if($newPass!='' && $oldPass!='' && $oldPass==md5($currPass)){
                $data = array(
                    'email' => $this->input->post('email_address'),
                    'password' =>md5($this->input->post('new_pass')),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company_name' => $this->input->post('company'),
                    'stage_name' => $this->input->post('stage'),
                 );
            }
            else{
                $data = array(
                    'email' => $this->input->post('email_address'),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company_name' => $this->input->post('company'),
                    'stage_name' => $this->input->post('stage'),
                 );
            }
            

            $this->db->where('id', $this->session->userdata('user_id'));
            $this->db->update('users', $data); 
        }
        
        /**
         * Function for get all details from "users" tabel
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
        **/
        public function user_details($userid) {
            $this->db->where('id', $userid);
            $this->db->select('first_name, last_name, company_name, stage_name, email');
            $q = $this->db->get('users');
            $data=  array();
            /* after we've made the queries from the database, we will store them inside a variable called $data, and return the variable to the controller */
            if($q->num_rows() > 0)
            {
              // we will store the results in the form of class methods by using $q->result()
              // if you want to store them as an array you can use $q->result_array()
              foreach ($q->result_array() as $row)
              {
                $data['details'] = $row;
              }
            }
            return $data;
        }
        
        
        /**
         * Function for get listing of users whose add by login user
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
        **/
        
        public function user_listing($userid) {       
            $is_admin=  $this->checkUserAdmin($userid);
            //$is_admin = $this->db->select('is_admin')->get_where('users', array('id' => $userid))->row()->is_admin;
            $data=  array();
            if($is_admin==1):
                $this->db->select('first_name, last_name, role, id as child_userid, email, stage_name, created_date as added_date');
                $this->db->from('users');
                $this->db->where("id !=", $userid);
                $query = $this->db->get();
            else:
                $this->db->select('a.first_name, a.last_name, b.role, b.child_userid, a.email, a.stage_name, b.added_date');
                $this->db->from('users as a');
                $this->db->join('user_relation b', 'a.id = b.child_userid', 'left'); 
                $this->db->where('b.parent_userid', $userid);
                $query = $this->db->get();
            endif;
            
            $data = $query->result_array();
            return $data;

        }
        
        /**
         * Function for change role in "users" and "user_relation" table
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @param int $role_value user's role value that you want to change
         * 
         * @access public
         * 
         * @return TRUE if role changed successfully 
        **/
        
        public function change_role($userid, $role_value) {
            if($this->input->post('projectid')>0){
                $data=array('role'=>$role_value);
                $this->db->where('id', $userid);
                $update=$this->db->update('project_users',$data);
            }
            else {
                // to find user is admin or not
                $is_admin=  $this->checkUserAdmin($this->session->userdata('user_id'));
                //$is_admin = $this->db->select('is_admin')->get_where('users', array('id' => $this->session->userdata('user_id')))->row()->is_admin;
                $data=array('role'=>$role_value);

                // if user is admin then update in "users" table 
                if($is_admin==1):
                    $this->db->where('id', $userid);
                    $update=$this->db->update('users',$data);

                // if user is admin then update in "user_relation" table 
                else:
                    $this->db->where('child_userid', $userid);
                    $this->db->where('parent_userid', $this->session->userdata('user_id'));
                    $update=$this->db->update('user_relation',$data);
                endif;
            }
            
            return $update;
        }
        
        /**
         * Function for add project of user in "projects" table
         *
         * @param string $filename is thumbnail image name of project, default is empty
         * 
         * @access public
         * 
         * @return primary id of project from "projects" table 
        **/
        public function add_project($filename='', $fileExt='jpg') {
            if($filename!=''){
//                $fileArr=explode('.', $filename);
//                $file=$fileArr[0];
//                $filename=$file.'_thumb.png';
                $parts = explode('.', $filename);
                $last = array_pop($parts);
                $parts = array(implode('.', $parts), $last);
                $filename=$parts[0].'_thumb.'.$fileExt;
            }
            $data = array(
                    'userid' => $this->session->userdata('user_id'),
                    'identifier' => $this->input->post('identifier'),
                    'title' => $this->input->post('title'),
                    'thumbnail' => $filename,
                    'added_date'=>date('Y-m-d H:i:s'),
                    'modified_date'=>date('Y-m-d H:i:s')
                 );
            
            if($this->input->post('view_only_mode')!=''){
                $data['view_only_mode']=$this->input->post('view_only_mode');
            }
            $this->db->insert('projects',$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        
        /**
         * Function for add project folder of user in "projects" table
         *
         * @param string $foldername is thumbnail image name of project, default is empty
         * 
         * @access public
         * 
         * @return primary id of project from "projects" table 
        **/
        public function add_project_folder($foldername) {
           
            $data = array(
                    'userid' => $this->session->userdata('user_id'),
                    'identifier' => strtolower(str_replace(' ', '', $foldername)).time(),
                    'title' => $foldername,
                    'added_date'=>date('Y-m-d H:i:s'),
                    'modified_date'=>date('Y-m-d H:i:s')
                 );
            
            
            $this->db->insert('projects',$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        
        /**
         * Function for update project of user in "projects" table
         *
         * @param string $filename is thumbnail image name of project, default is empty
         * 
         * @access public
         * 
         * @return primary id of project from "projects" table 
        **/
        public function update_project($filename='', $fileupload='no', $ext='jpg') {
            if($filename!='' && $fileupload=='yes'){
//                $fileArr=explode('.', $filename);
//                $file=$fileArr[0];
//                $filename=$file.'_thumb.png';
                $parts = explode('.', $filename);
                $last = array_pop($parts);
                $parts = array(implode('.', $parts), $last);
                $filename=$parts[0].'_thumb.'.$ext;
            }
            $data = array(
                    'userid' => $this->session->userdata('user_id'),
                    'identifier' => $this->input->post('identifier'),
                    'title' => $this->input->post('title'),
                    'thumbnail' => $filename,
                    'modified_date'=>date('Y-m-d H:i:s')
                 );
            
            if($this->input->post('view_only_mode')!=''){
                $data['view_only_mode']=$this->input->post('view_only_mode');
            }
            else{
                $data['view_only_mode']='0';
            }
            
            $this->db->where('id', $this->input->post('project_id'));
            $this->db->update('projects',$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        
        /**
         * Function for listing user's project from "projects" table
         *
         * @param int $userid user's primary id from "users" table
         * @param int $roleid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
        **/
        
        public function user_projects($userid, $roleid='') {
            if($roleid>1){
                //$roleid=$roleid-1;
                $roleid=(string) $roleid;
                $this->db->select('a.role, b.identifier, b.id, b.title, b.thumbnail');
                $this->db->from('project_users as a');
                $this->db->join('projects b', 'a.project_id = b.id', 'left'); 
                $this->db->where('a.user_id', $userid);
                $this->db->where('a.role', $roleid);
                $this->db->where('b.status', '1');
                $this->db->order_by("b.sort_number", "asc"); 
                $query = $this->db->get(); 
            }
            else {
                $this->db->where('userid', $userid);            
                $this->db->where('status', '1');
                $this->db->select('id, identifier, title, thumbnail');
                $this->db->order_by("sort_number", "asc"); 
                $query = $this->db->get('projects');
            }   
            
            $data=  array();
            //check number of rows of "projects" table
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
            }
            return $data;
        }
        
        /**
         * Function for check identifer from "projects" table
         *
         * @param string $identifier identifier in add project form
         * 
         * @access public
         * 
         * @return string if identifier is not find in "projects" table then "Available" and if find then "Not Available" 
        **/
        
        public function check_identifier($identifier) {
            $this->db->where("identifier",$identifier);
            
            $query=$this->db->get("projects");
            
            //check number of rows of "projects" table
            if($query->num_rows()>0)
            {
                return 'Not Available';
            }
            else{
                return 'Available';
            }
        }
        
        /**
         * Function for listing user's participated projects from "projects" and "project_users" table
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
        **/
        public function project_participated($userid) {
            $this->db->select('a.role, b.identifier, b.id, b.title, b.thumbnail');
            $this->db->from('project_users as a');
            $this->db->join('projects b', 'a.project_id = b.id', 'left'); 
            $this->db->where('a.user_id', $userid);
            $query = $this->db->get();
            $data=  array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
            }
            return $data;
        }
        
        /**
         * Function for remove user
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
        **/
        public function remove_user($userid) {
            $is_admin=  $this->checkUserAdmin($this->session->userdata('user_id'));
            //$is_admin = $this->db->select('is_admin')->get_where('users', array('id' => $this->session->userdata('user_id')))->row()->is_admin;
            if($is_admin==1):
                $this->db->where('id', $userid);
                $this->db->delete('users'); 
            else:
                $this->db->delete('user_relation', array('parent_userid' => $this->session->userdata('user_id'), 'child_userid'=>$userid)); 
            endif;
            
        }
        
        /**
         * Function for get all project of user
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
        **/
        public function allProjects($userid){
            $query="SELECT a.id, a.identifier FROM `projects` as a 
                    LEFT JOIN `project_users` as b
                    ON a.id=b.project_id
                    WHERE (a.userid='".$userid."' || (b.user_id='".$userid."' && b.role='4')) group by a.id";
            //$this->db->where('a.role', $roleid);
            $q=$this->db->query($query);
            $data=$q->result_array();
            //$data = $this->db->get();
            return $data;
       }
       
       /**
         * Function for get all project of user
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
        **/
        public function subscriptions($notInclude='') {
            if($notInclude!=''){
                $this->db->where("id !=", $notInclude);
            }
             $query = $this->db->get('subscription');
             $data=  array();
             //check number of rows of query
             if($query->num_rows() > 0)
             {
                 $data = $query->result_array();
             }
             return $data;
        }
       
        /**
         * Function for add sales of user in "sales" table
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return insert id of sales
         * 
        **/
        public function addSales($userid, $subscriptionId, $invoice='') {
            if($subscriptionId==1){
                $data = array(
                        'userid' => $userid,
                        'invoice'=>$invoice,
                        'subscription_id' => $subscriptionId,
                        'order_date' => date('Y-m-d H:i:s'),
                        'activation_date' => date('Y-m-d H:i:s'),
                        'expiry_date' => date('Y-m-d H:i:s', strtotime('+1 month')),
                        'status' => '1'
                    );
            }
            else{
                $data = array(
                        'userid' => $userid,
                        'invoice'=>$invoice,
                        'subscription_id' => $subscriptionId,
                        'order_date' => date('Y-m-d H:i:s')
                    );
            }
            
            $this->db->insert('sales', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        
        /**
         * Function for check users limit accordingly user's subscription package
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return "yes" or "no" only. If yes then user can add more users or if not then user can not add more users
         * 
        **/
        public function checkUserLimit($userid){
            $is_admin=  $this->checkUserAdmin($userid);
            if($is_admin==1){
                return $addUser='yes';
            }
            else{
                
                $qry="SELECT b.users, (SELECT COUNT( id ) 
                        FROM  `user_relation` 
                        WHERE parent_userid =".$userid.") as useradded FROM `sales` as a
                        JOIN subscription as b
                        ON a.subscription_id=b.id
                        WHERE a.status='1' && a.expiry_date>='".date('Y-m-d H:i:s')."' && a.userid=".$userid."
                        ORDER BY a.expiry_date DESC limit 1";
                $query=$this->db->query($qry);
                //$data = $query->result_array();
                //check number of rows of query
                if($query->num_rows() > 0)
                {
                    $row = $query->row(); 
                    $avaliableUsers= $row->users;
                    $addedUsers=$row->useradded;
                    if($addedUsers<$avaliableUsers){
                        $addUser='yes';
                    }
                    else{
                        $addUser='no';
                    }
                }
                else{
                    $addUser='no';
                }
                
            }
            return $addUser;
        }
        
        /**
         * Function check login user is admin or not
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return admin or not
         * 
        **/
        public function checkUserAdmin($userid){
            $is_admin = $this->db->select('is_admin')->get_where('users', array('id' => $userid))->row()->is_admin;
            return $is_admin;
        }
        
        /**
         * Function for get users subscription list
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
         * 
        **/
        public function getUserSubscriptions($userid){
            $this->db->select('a.*, b.name, a.invoice, b.users, b.traffic_storage');
            $this->db->from('sales as a');
            $this->db->join('subscription b', 'a.subscription_id = b.id'); 
            $this->db->where('a.userid', $userid);
            $query = $this->db->get();
            $data=  array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
            }
            return $data;
        }
        
        /**
         * Function for get remaining list of users for subscription
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
         * 
        **/
        public function getRemainingUsers($userid) {
            $qry="SELECT SUM(users) as users, (SELECT COUNT( id ) 
                        FROM  `user_relation` 
                        WHERE parent_userid =".$userid.") as useradded FROM `sales` as a
                        JOIN subscription as b
                        ON a.subscription_id=b.id
                        WHERE a.status='1' && a.expiry_date>='".date('Y-m-d H:i:s')."' && a.userid=".$userid."
                        ORDER BY a.expiry_date DESC limit 1";
            $query=$this->db->query($qry);
            //$data = $query->result_array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $row = $query->row(); 
                $avaliableUsers= $row->users;
                $addedUsers=$row->useradded;
                $remianingUsers= $avaliableUsers-$addedUsers;
                $remianingUsers=($remianingUsers>0) ? $remianingUsers : 0;
            }
            else{
                $remianingUsers= 0;
            }
            return $remianingUsers;
        }
        
        /**
         * Function for get remaining traffic
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
         * 
        **/
        function getRemianingTraffic($userid){
            $videoSize = $this->db->select_sum('size')->get_where('user_videos', array('userid' => $userid))->row()->size;
            $fileSize = $this->db->select_sum('size')->get_where('user_files', array('userid' => $userid))->row()->size;
            $imageSize = $this->db->select_sum('size')->get_where('album_images', array('userid' => $userid))->row()->size;
            $totalSize=$videoSize+$fileSize+$imageSize;
            return $totalSize;
        }
        
        /**
         * Function for get project size
         *
         * @param int $userid user's primary id from "users" table
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
         * 
        **/
        function getProjectSize($projectId){
            $qry='SELECT SUM(`size`) as sizee FROM (SELECT SUM(`size`) as size FROM user_videos WHERE video_folder_id IN(SELECT id FROM videos_folder WHERE project_id='.$projectId.') 
                    UNION SELECT SUM(`size`) as size FROM user_files WHERE file_folder_id IN(SELECT id FROM files_folder WHERE project_id='.$projectId.') 
                    UNION SELECT sum(`size`) as size FROM `album_images` where album_id IN (select id from user_albums WHERE project_id='.$projectId.')) as temp';
            $query=$this->db->query($qry);
            //$data = $query->result_array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $row= $query->row();  
                $size=$row->sizee;
                return $size;
                //die('size='.$size);
            }
            else{
                return '0';
            }
        }
                
        function getprojectList($userid, $getall='no'){
            $this->db->select('id, identifier, title, added_date, status');
            $this->db->from('projects');
            $this->db->where('userid', $userid);
            if($getall=='no'){
                $this->db->where('status', '1');
            }
            $query = $this->db->get();
            $data=  array();
            $newArray=  array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
                $newArray=  array();
                $i=1;
                foreach ($data as $key => $value) {
                    $newArray[$i]['id']=$value['id'];
                    $newArray[$i]['identifier']=$value['identifier'];
                    $newArray[$i]['title']=$value['title'];
                    $newArray[$i]['added_date']=$value['added_date'];
                    $newArray[$i]['status']=$value['status'];
                    $newArray[$i]['size']=  $this->getProjectSize($value['id']);
                    $i++;
                }
                
            }
            
            return $newArray;
        }
        /**
         * Function for get rename folders
         *
         * @param int $id of folder
         * @param string $name of folder
         * @param string $type of folder
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
         * 
        **/
        function renamefolder($id, $name, $type){
            $userid = $this->session->userdata('user_id');
            switch ($type){
                case 'project':
                    $data=  array('title'=>$name, 'modified_date'=>date('Y-m-d H:i:s'));
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('projects', $data);
                    return $this->db->affected_rows();
                break;
            
                case 'album-files':
                    $data=  array('name'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('files_folder', $data);
                    return $this->db->affected_rows();
                break;
                
                case 'album-videos':
                    $data=  array('name'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('videos_folder', $data);
                    return $this->db->affected_rows();
                break;
                
                case 'album-images':
                    $data=  array('name'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('user_albums', $data);
                    return $this->db->affected_rows();
                break;
            
                case 'files':
                    $data=  array('title'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('user_files', $data);
                    return $this->db->affected_rows();
                break;
                
                case 'videos':
                    $data=  array('title'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('user_videos', $data);
                    return $this->db->affected_rows();
                break;
                
                case 'images':
                    $data=  array('title'=>$name);
                    $this->db->where('id', $id);
                    $this->db->where('userid', $userid);
                    $this->db->update('album_images', $data);
                    return $this->db->affected_rows();
                break;
            }
            
        }
        
        function chkuserProject($projectid){
            $userid = $this->session->userdata('user_id');
            
            $query = $this->db->query('SELECT id FROM projects WHERE userid="'.$userid.'" && id="'.$projectid.'"');
            $num= $query->num_rows();
            if($num>0){
                return true;
            }
            else{
                return false;
            }
        }
        
        function getProjectAlbumSize($albumid, $type){
            switch ($type) {
                case 'files':
                    $qry='SELECT SUM(size) as size FROM `user_files` where file_folder_id="'.$albumid.'"';
                break;

                case 'videos':
                    $qry='SELECT SUM(size) as size FROM `user_videos` where video_folder_id="'.$albumid.'"';
                break;
            
                case 'images':
                    $qry='SELECT SUM(size) as size FROM `album_images` where album_id="'.$albumid.'"';
                break;
            }
            
            $query=$this->db->query($qry);
            if($query->num_rows() > 0)
            {
                $row= $query->row();  
                $size=$row->size;
                return $size;
                //die('size='.$size);
            }
            else{
                return '0';
            }
        }
        
        function getprojectListAlbums($projectid){
            $qry='SELECT id, name, added_date, status, "files" as type FROM files_folder WHERE project_id='.$projectid.'
                    UNION ALL
                    SELECT id, name, added_date, status, "videos" as type FROM videos_folder WHERE project_id='.$projectid.'
                    UNION ALL
                    SELECT id, name, added_date, status, "images" as type FROM user_albums WHERE project_id='.$projectid;
            $query=$this->db->query($qry);
            $data=  array();
            $newArray=  array();
            //check number of rows of query
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
                
                $i=1;
                foreach ($data as $key => $value) {
                    $newArray[$i]['id']=$value['id'];
                    $newArray[$i]['name']=$value['name'];
                    $newArray[$i]['type']=$value['type'];                    
                    $newArray[$i]['status']=$value['status'];                    
                    $newArray[$i]['added_date']=$value['added_date'];                    
                    $newArray[$i]['size']=  $this->getProjectAlbumSize($value['id'], $value['type']);
                    $i++;
                }
            }
            return $newArray;
            
        }
        
        function insertNewProjectAlbum($projectid, $name, $type){
            if($type=='images'){
                $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('user_albums', array('project_id' => $projectid))->row()->maxnum;
                $sort_num=$maxsort+1;
                $data = array(
                    'name' =>$name,
                    'project_id' => $projectid,
                    'userid' => $this->session->userdata('user_id'),
                    'added_date' => date('Y-m-d H:i:s'),
                    'sort_number' => $sort_num,
                 );
                $this->db->insert('user_albums',$data);
            }

            //if data save for videos folders then get last sort number from "videos_folder" tables
            else if($type=='videos'){
                $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('videos_folder', array('project_id' => $projectid))->row()->maxnum;
                $sort_num=$maxsort+1;
                $data = array(
                    'name' =>$name,
                    'project_id' => $projectid,
                    'userid' => $this->session->userdata('user_id'),
                    'added_date' => date('Y-m-d H:i:s'),
                    'sort_number' => $sort_num,
                );
                $this->db->insert('videos_folder',$data);
            }

            //if data save for files folders then get last sort number from "files_folder" tables
            else if($type=='files'){
                $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('files_folder', array('project_id' => $projectid))->row()->maxnum;
                $sort_num=$maxsort+1;
                $data = array(
                    'name' =>$name,
                    'project_id' => $projectid,
                    'userid' => $this->session->userdata('user_id'),
                    'added_date' => date('Y-m-d H:i:s'),
                    'sort_number' => $sort_num,
                );
                $this->db->insert('files_folder',$data);
            }

        }
        
        function getAlbumAssets($projectid, $albumid, $type, $userid){
            $data=  array();
            switch ($type) {
                case 'images':
                    //$qry='SELECT id, title as name, size, added_date, aliases, `status`, "images" as type FROM album_images where album_id="'.$albumid.'" && userid="'.$userid.'"';
                    $qry='SELECT id, title as name, size, added_date, aliases, `status`, "images" as type FROM album_images where album_id="'.$albumid.'"';
                    $query=$this->db->query($qry);
                    //check number of rows of query
                    if($query->num_rows() > 0){
                        $data = $query->result_array();
                    }
                    
                break;
            
                case 'videos':
                    //$qry='SELECT id, title as name, size, added_date, aliases, `status`, "videos" as type FROM user_videos where video_folder_id="'.$albumid.'" && userid="'.$userid.'"';
                    $qry='SELECT id, title as name, size, added_date, aliases, `status`, "videos" as type FROM user_videos where video_folder_id="'.$albumid.'"';
                    $query=$this->db->query($qry);
                    //check number of rows of query
                    if($query->num_rows() > 0){
                        $data = $query->result_array();
                    }

                break;
            
                case 'files':
                    //$qry='SELECT id, title as name, size, added_date, aliases, `status`, "files" as type FROM user_files where file_folder_id="'.$albumid.'" && userid="'.$userid.'" && size>0';
                    $qry='SELECT id, title as name, size, added_date, aliases, `status`, "files" as type FROM user_files where file_folder_id="'.$albumid.'" && size>0';
                    $query=$this->db->query($qry);
                    //check number of rows of query
                    if($query->num_rows() > 0){
                        $data = $query->result_array();
                    }

                break;
            }
            
            return $data;
        }
        
        function getProjectAlbums($type, $projectid){
            switch ($type) {
                case 'files':
                    $qry='SELECT id, name FROM files_folder WHERE project_id='.$projectid;
                break;
            
                case 'videos':
                    $qry='SELECT id, name FROM videos_folder WHERE project_id='.$projectid;
                break;
            
                case 'images':
                    $qry='SELECT id, name FROM user_albums WHERE project_id='.$projectid;
                break;
            
            }
            $data=  array();
            $query=$this->db->query($qry);
            if($query->num_rows() > 0)
            {
                $data = $query->result_array();
            }
            return $data ;
        }
        
        
    function getWebPagesList($userid){
        $this->db->select('id, foldername, created_date');
        $this->db->from('wepages');
        $this->db->where('userid', $userid);
        $this->db->where('status', '1');
        $query = $this->db->get();
        $data=  array();
        //check number of rows of query
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    
    function save_aliases($userid, $projectid, $type, $albumid, $fileid){
        switch ($type) {
            case 'files':
                $tablename='user_files';
            break;
                
            case 'videos':
                $tablename='user_videos';
            break;
                
            case 'images':
                $tablename='album_images';
                $maxsort = $this->db->select("max(sort_num) as maxnum")->get_where('album_images', array('album_id' => $albumid))->row()->maxnum;
                $sort_num=$maxsort+1;
            break;
        }
        
        $dataQry='SELECT * FROM '.$tablename.' WHERE id="'.$fileid.'"';
        
        $query = $this->db->query($dataQry);
        $row=  array();
        $insert_id=0;
        if ($query->num_rows() > 0){
            
           $row = $query->row(); 
           if($type=='images'){
               $data=  array('album_id'=>$albumid, 'thumbnail'=>$row->thumbnail, 'full_image'=>$row->full_image, 'sort_num'=>$sort_num, 'added_date'=>date('Y-m-d H:i:s'), 'title'=>$row->title, 'additional_info'=>$row->additional_info, 'image_version'=>$row->image_version, 'userid'=>$userid, 'size'=>$row->size, 'aliases'=>$fileid);
           }
           else if($type=='files'){
               $data=  array('file_folder_id'=>$albumid, 'thumbnail'=>$row->thumbnail, 'file'=>$row->file, 'added_date'=>date('Y-m-d H:i:s'), 'title'=>$row->title, 'additional_info'=>$row->additional_info, 'size'=>$row->size, 'crocdoc_id'=>$row->crocdoc_id, 'userid'=>$userid, 'aliases'=>$fileid);
           }
           else if($type=='videos'){
               $data=  array('video_folder_id'=>$albumid, 'thumbnail'=>$row->thumbnail, 'video'=>$row->video, 'added_date'=>date('Y-m-d H:i:s'), 'title'=>$row->title, 'additional_info'=>$row->additional_info, 'size'=>$row->size, 'vimeo_id'=>$row->vimeo_id, 'userid'=>$userid, 'aliases'=>$fileid);
           }
           
           $this->db->insert($tablename,$data);
           $insert_id = $this->db->insert_id();
           
        }
        return $insert_id;
    }
}

?>