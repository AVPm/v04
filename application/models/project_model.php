<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * project model class file
 *
 */

class Project_model extends CI_Model {
    
    public function __construct()
    {
        /*
         * call the constructor of CI
         */
        parent::__construct();
    }
    
    /**
     * Function for list user for a particular project
     * @param int $projectId primary id of project
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/  
    
    public function project_users($projectId) {        
        $this->db->select('a.id, a.project_id, a.user_id, a.added_date, a.role, b.first_name, b.last_name, b.stage_name, b.email');
        $this->db->from('project_users as a');
        $this->db->join('users b', 'a.user_id = b.id', 'left'); 
        $this->db->where('a.project_id ', $projectId);
        $query = $this->db->get();
        $data=  array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
     * Function for get project title
     * @param int $projectId primary id of project
     *
     * @access public
     * 
     * @return $title of project
     **/  
    public function project_title($projectId) {
        $title = $this->db->select('title')->get_where('projects', array('id' => $projectId))->row()->title;
        return $title;
    }
    
    /**
     * Function for add data in "project_users" table
     * 
     * @param int $userId primary id of a user
     * @param int $projectId primary id of a project
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "project_users" table
     **/  
    public function add_userProject($userId, $projectId) {
        $data = array(
                'project_id' => $projectId,
                'user_id' => $userId,
                'role' => '0',
                'added_date' => date('Y-m-d H:i:s'),
             );
        $this->db->insert('project_users',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    /**
     * Function for remove data in "project_users" table
     * 
     * @param int $userId primary id of a user
     * @param int $projectId primary id of a project
     *
     * @access public
     * 
     * @return $delete if query run successfull
     **/
    public function remove_userProject($userid, $projectId) {
        $this->db->where('user_id', $userid);
        $this->db->where('project_id', $projectId);
        $delete = $this->db->delete('project_users');
        return $delete;
    }
    
    /**
     * Function for get user album listing of images from "user_albums" table or videos from "videos_folder" table
     * 
     * @param int $userId primary id of a user
     * @param int $projectId primary id of a project
     * @param string $table name of table.
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_user_albums($userid, $projectId, $table='') {
//        $this->db->where('userid', $userid);
//        $this->db->where('project_id', $projectId);
//        $this->db->order_by("sort_number", "asc"); 
//        $query = $this->db->get($table);
//        
//        comment by arun due to modification regarding view module without login
        //$query=  $this->db->query("SELECT * FROM ".$table." where project_id='".$projectId."' && (userid='".$userid."' || (SELECT count(id) FROM project_users WHERE user_id='".$userid."' && project_id='".$projectId."' && role='4')>0) ORDER BY sort_number");
        $data=  array();
        if($table!=''){
            $query=  $this->db->query("SELECT * FROM ".$table." where project_id='".$projectId."' && `status`='1' ORDER BY sort_number");
            $data = $query->result_array();
        }
        /*
        if(count($data)==0){
            $this->db->select('a.*');
            $this->db->join('project_users b', 'a.id= b.project_id', 'left'); 
            $this->db->where('b.userid', $userid);
            $this->db->where('a.userid', $userid);
        }
         * 
         */
        return $data;
    }
    
    
    /**
     * Function for save data in "user_albums" OR "videos_folder" table
     * 
     * @param int $projectId primary id of a project
     * 
     * @param string $filename name of file that you want to save
     * 
     * @param string $action set for videos and images
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "user_albums" table
     **/
    public function save_album($projectId, $filename='', $action, $ext='jpg') {
        if($filename!=''){
            /*
            $fileArr=explode('.', $filename);
            $file=$fileArr[0];
            $filename=$file.'_thumb.png';
            $filename=$upload_data['upload_data']['file_name'];
             * 
             */
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            if($parts[0]==''){
                $filename=$filename.'_thumb.'.$ext;
            }
            else{
                $filename=$parts[0].'_thumb.'.$ext;
            }
            //$filename=$parts[0].'_thumb.jpg';
           // $fileExt=$parts[1];
        }
        
        //if data save for images albums then get last sort number from "user_albums" tables
        if($action=='images'){
            $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('user_albums', array('project_id' => $projectId))->row()->maxnum;
        }
        
        //if data save for videos folders then get last sort number from "videos_folder" tables
        else if($action=='videos'){
            $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('videos_folder', array('project_id' => $projectId))->row()->maxnum;
        }
        
        //if data save for files folders then get last sort number from "files_folder" tables
        else if($action=='files'){
            $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('files_folder', array('project_id' => $projectId))->row()->maxnum;
        }
        
        //if data save for files folders then get last sort number from "files_folder" tables
        else if($action=='webpages'){
            $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('webpages_albums', array('project_id' => $projectId))->row()->maxnum;
        }
        
        $sort_num=$maxsort+1;
        
        //data for "user_albums" tables
        if($action=='images'){
            $data = array(
                'name' =>$this->input->post('album_name'),
                'description' => $this->input->post('album_info'),
                'project_id' => $projectId,
                'userid' => $this->session->userdata('user_id'),
                'thumbnail' => $filename,
                'added_date' => date('Y-m-d H:i:s'),
                'sort_number' => $sort_num,
                'upscale' => $this->input->post('upscale')
             );
        $this->db->insert('user_albums',$data);
        }
        
        //data for "videos_folder" or "files_folder" tables
        else if($action=='videos' || $action=='files' || $action=='webpages'){
            $data = array(
                'name' =>$this->input->post('folder_name'),
                'project_id' => $projectId,
                'userid' => $this->session->userdata('user_id'),
                'thumbnail' => $filename,
                'added_date' => date('Y-m-d H:i:s'),
                'sort_number' => $sort_num,
            );
            if($action=='videos')
                $this->db->insert('videos_folder',$data);
            
            if($action=='files')
                $this->db->insert('files_folder',$data);
            
            if($action=='webpages')
                $this->db->insert('webpages_albums',$data);
        }
        
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    /**
     * Function for edit information of albums in "user_albums" table
     * 
     * @param string $filename name of image file 
     *
     * @access public
     * 
     * @return update 
     **/
    function edit_album($filename, $type, $ext='jpg') {
        if($filename!=''){
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            if($parts[0]==''){
                $filename=$filename.'_thumb.'.$ext;
            }
            else{
                $filename=$parts[0].'_thumb.'.$ext;
            }
        }
        else{
            $filename=  $this->input->post('album_thumbnail_hidden');
        }
        if($this->input->post('deletethumb')=='delete'){
            $filename='';
        }
        
        //data for insertion in "album_images" table
        if($type=='images'){
            $data = array(
                'name' =>$this->input->post('edit_album_name'),
                'description' => $this->input->post('edit_album_description'),
                'upscale' =>  $this->input->post('upscaleimages'),
                'thumbnail' => $filename,
             );
            $tablename='user_albums';
        }
        else if($type=='videos' || $type=='files' || $type=='webpages'){
            $data = array(
                'name' =>$this->input->post('edit_album_name'),
                'thumbnail' => $filename,
             );
            if($type=='videos')
                $tablename='videos_folder';
            else if($type=='files')
                $tablename='files_folder';
            else
                $tablename='webpages_albums';
        }
        
        $id=$this->input->post('album_hidden');
        $this->db->where('id', $id);
        $this->db->update($tablename, $data);
        //$insert_id = $this->db->insert_id();
        //return $insert_id;
    }
    
    /**
     * Function for save images of albums in "album_images" table
     * 
     * @param int $albumid primary id of album
     * @param string $filename name of image file 
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "album_images" table
     **/
    function save_album_image($albumid, $filename, $filesize, $ext='jpg') {
        if($filename!=''){
            $orgFile=$filename;
            /*
            $fileArr=explode('.', $filename);
            $file=$fileArr[0];
            $filename=$file.'_thumb.png';
             * 
             */
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            if($parts[0]==''){
                $filename=$filename.'_thumb.'.$ext;
            }
            else{
                $filename=$parts[0].'_thumb.'.$ext;
            }
            //$filename=$parts[0].'_thumb.jpg';
            //$version_imageId=($this->input->post('image_version')=='yes') ? $this->input->post('imageid_version') : 0;
            //$version_imageId=$_REQUEST['image_version'];
            $version_imageId=0;
            if($_REQUEST['image_version']=='yes')
                $version_imageId=$_REQUEST['imageid_version'];
        }
        
        $maxsort = $this->db->select("max(sort_num) as maxnum")->get_where('album_images', array('album_id' => $albumid))->row()->maxnum;
        $sort_num=$maxsort+1;
        //data for insertion in "album_images" table
        $data = array(
                'album_id' =>$albumid,
                'full_image' => $orgFile,
                'title' => $orgFile,
                'sort_num' => $sort_num,
                'thumbnail' => $filename,
                'added_date' => date('Y-m-d H:i:s'),
                'image_version' => $version_imageId,
                'userid' => $this->session->userdata('user_id'),
                'size'=>$filesize
             );
        
        //insert data in "album_images" table
        $this->db->insert('album_images',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    
    /**
     * Function for save videos in "user_videos" table
     * 
     * @param int $albumid primary id of project folder
     * @param string $filename name of image file 
     * @param string $filesize size of image file 
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "user_videos" table
     **/
    function save_album_video($albumid, $filename, $filesize) {
        $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('user_videos', array('video_folder_id' => $albumid))->row()->maxnum;
        $maxsort=$maxsort+1;
        //data for insertion in "album_images" table
        $data = array(
                'video_folder_id' =>$albumid,
                'video' => $filename,
                'title' => $filename,
                'added_date' => date('Y-m-d H:i:s'),
                'userid'=>$this->session->userdata('user_id'),
                'size'=>$filesize,
                'sort_number'=>$maxsort
             );
        
        //insert data in "album_images" table
        $this->db->insert('user_videos',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    /**
     * Function for save videos in "user_files" table
     * 
     * @param int $albumid primary id of project folder
     * @param string $filename name of file 
     * @param string $filesize size of file 
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "user_files" table
     **/
    function save_album_files($albumid, $filenameEncrypt, $filename, $filesize, $crocdocId, $thumbnail) {
        
        //data for insertion in "album_images" table
        $maxsort = $this->db->select("max(sort_number) as maxnum")->get_where('user_files', array('file_folder_id' => $albumid))->row()->maxnum;
        $maxsort=$maxsort+1;
        $data = array(
                'file_folder_id' =>$albumid,
                'file' => $filenameEncrypt,
                'title' => $filename,
                'added_date' => date('Y-m-d H:i:s'),
                'userid'=>$this->session->userdata('user_id'),
                'crocdoc_id'=>$crocdocId,
                'size'=>$filesize,
                'thumbnail'=>$thumbnail,
                'sort_number'=>$maxsort
             );
        
        //insert data in "album_images" table
        $this->db->insert('user_files',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    
    /**
     * Function for save webpages in "webpages" table
     * 
     * @param int $albumid primary id of project folder
     * @param string $filename name of file 
     * @param string $filesize size of file 
     *
     * @access public
     * 
     * @return $insert_id is last insertion id in "user_files" table
     **/
    function save_webpages($albumid, $filename, $filesize) {
        
        //data for insertion in "album_images" table
        $parts = explode('.', $filename);
        $last = array_pop($parts);
        $parts = array(implode('.', $parts), $last);
        $file=$parts[0];
        $fileExt=$parts[1];
        $data = array(
                'albumid' =>$albumid,
                'foldername' => $file,
                'title' => $file,
                'added_date' => date('Y-m-d H:i:s'),
                'modified_date' => date('Y-m-d H:i:s'),
                'userid'=>$this->session->userdata('user_id'),
                'size'=>$filesize,
             );
        
        //insert data in "album_images" table
        $this->db->insert('webpages',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    /**
     * Function for edit videos in "edit_video_info" table
     * 
     * @param int $videoid primary id of project folder video
     * @param string $filename name of image file 
     *
     * @access public
     * 
     **/
    function edit_video_info($videoid, $filename, $vimeoVideoThumb, $ext='jpg', $vimeoimg = '') {
        if($filename!=''){
            $orgFile=$filename;
            /*
            $fileArr=explode('.', $filename);
            $file=$fileArr[0];
            $filename=$file.'_thumb.png';
             * 
             */
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            $filename=$parts[0].'_thumb.'.$ext;
        }
        else{
            $filename=$this->input->post('video_thumbnail_hidden');
        }
        
        if($this->input->post('deletethumb')=='delete'){
            $filename='';
            $vimeoimg = '';
        }
        if($vimeoVideoThumb!='no'){
            $filename=$vimeoVideoThumb;
        }
        
        //$filename=($filename!='') ? $filename : $this->input->post('video_thumbnail_hidden');
        //echo 'file='.$filename;
        //die();
        //data for updation in "album_images" table
        $data = array(
                'thumbnail' =>$filename,
                'title' => $this->input->post('video_title'),
                'additional_info' => $this->input->post('additional_info'),
                'vimeo_thumbnail' => $vimeoimg
             );
        
        $this->db->where('id', $videoid);
        $this->db->update('user_videos', $data);
    }
    
    /**
     * Function for edit videos in "edit_video_info" table
     * 
     * @param int $videoid primary id of project folder video
     * @param string $filename name of image file 
     *
     * @access public
     * 
     **/
    function edit_webpage_info($id, $filename, $ext='jpg') {
        if($filename!=''){
            $orgFile=$filename;
            /*
            $fileArr=explode('.', $filename);
            $file=$fileArr[0];
            $filename=$file.'_thumb.png';
             * 
             */
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            $filename=$parts[0].'_thumb.'.$ext;
        }
        else{
            $filename=$this->input->post('video_thumbnail_hidden');
        }
        
        if($this->input->post('deletethumb')=='delete'){
            $filename='';
        }
        
        
        //$filename=($filename!='') ? $filename : $this->input->post('video_thumbnail_hidden');
        //echo 'file='.$filename;
        //die();
        //data for updation in "album_images" table
        $data = array(
                'thumbnail' =>$filename,
                'title' => $this->input->post('video_title'),
                'additional_info' => $this->input->post('additional_info'),
                'iframe_height' => $this->input->post('iframe_height'),
                'iframe_width' => $this->input->post('iframe_width'),
             );
        
        $this->db->where('id', $id);
        $this->db->update('webpages', $data);
    }
    
    
    /**
     * Function for edit videos in "edit_file_info" table
     * 
     * @param int $videoid primary id of project folder video
     * @param string $filename name of image file 
     *
     * @access public
     * 
     **/
    function edit_file_info($fileid, $filename, $crocdocVideoThumb, $ext='jpg') {
        if($filename!=''){
            $orgFile=$filename;
            
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            $filename=$parts[0].'_thumb.'.$ext;
        }
        else{
            $filename=$this->input->post('video_thumbnail_hidden');
        }
        
        if($this->input->post('deletethumb')=='delete'){
            $filename='';
        }
        
        if($crocdocVideoThumb!='no'){
            $filename=$crocdocVideoThumb;
        }
        
        //data for updation in "album_images" table
        $data = array(
                'thumbnail' =>$filename,
                'title' => $this->input->post('file_title'),
                'additional_info' => $this->input->post('additional_info'),
             );
        
        $this->db->where('id', $fileid);
        $this->db->update('user_files', $data);
    }
    
    /**
     * Function for show listing from "album_images" table
     * 
     * @param int $currAlbumId primary id of a album
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_albums_images($currAlbumId) {
        $this->db->where('album_id', $currAlbumId);
        $this->db->where('status', '1');
        $this->db->order_by("sort_num", "asc"); 
        $query = $this->db->get('album_images');
        $data=  array();
        $data = $query->result_array();
        return $data;
    }
    
    /**
     * Function for show listing from "user_videos" table
     * 
     * @param int $currAlbumId primary id of a project
     * @param string $sortColumn column name of field
     * @param string $sortby sorting order desc, asc
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_project_videos($currAlbumId, $sortColumn='', $sortby='') {
        switch ($sortColumn) {
            case 'name':
                $orderbyField='a.title';
            break;
        
            case 'user':
                $orderbyField='b.stage_name';
                break;
            
            case 'date':
                $orderbyField='a.added_date';
                break;
            
            case 'size':
                $orderbyField='a.size';
                break;
            default:
                $orderbyField='a.sort_number';
            break;
        }
        $this->db->select('a.*, b.stage_name');
        $this->db->join('users b', 'a.userid = b.id', 'left'); 
        $this->db->where('video_folder_id', $currAlbumId);
        $this->db->where('status', '1');
        $this->db->where('size >', 0);
        
        $ordeby=($sortby!='') ? $sortby : 'asc';
        $this->db->order_by($orderbyField, $ordeby); 
        $query = $this->db->get('user_videos as a');
        $data=  array();
        $data = $query->result_array();
        return $data;
    }
    
    /**
     * Function for show listing from "webpages" table
     * 
     * @param int $currAlbumId primary id of a project
     * @param string $sortColumn column name of field
     * @param string $sortby sorting order desc, asc
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_webpages($currAlbumId, $sortColumn='', $sortby='') {
        switch ($sortColumn) {
            case 'name':
                $orderbyField='a.title';
            break;
        
            case 'user':
                $orderbyField='b.stage_name';
                break;
            
            case 'date':
                $orderbyField='a.added_date';
                break;
            
            case 'size':
                $orderbyField='a.size';
                break;
            default:
                $orderbyField='a.title';
            break;
        }
        $this->db->select('a.*, b.stage_name');
        $this->db->join('users b', 'a.userid = b.id', 'left'); 
        $this->db->where('a.albumid', $currAlbumId);
        $this->db->where('a.status', '1');
        
        $orderby=($sortby!='') ? $sortby : 'asc';
        $this->db->order_by($orderbyField, $orderby); 
        $query = $this->db->get('webpages as a');
        $data=  array();
        //echo $sql = $this->db->last_query();        die();
        $data = $query->result_array();
        //echo '<pre>'; print_r($data); die;
        return $data;
    }
    
    /**
     * Function for show listing from "user_files" table
     * 
     * @param int $currAlbumId primary id of a project
     * @param string $sortColumn column name of field
     * @param string $sortby sorting order desc, asc
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_project_files($currAlbumId, $sortColumn='', $sortby='') {
        switch ($sortColumn) {
            case 'name':
                $orderbyField='a.title';
            break;
        
            case 'user':
                $orderbyField='b.stage_name';
                break;
            
            case 'date':
                $orderbyField='a.added_date';
                break;
            
            case 'size':
                $orderbyField='a.size';
                break;
            default:
                $orderbyField='sort_number';
            break;
        }
        $this->db->select('a.*, b.stage_name');
        $this->db->join('users b', 'a.userid = b.id', 'left'); 
        $this->db->where('file_folder_id', $currAlbumId);
        $this->db->where('size >', 0);
        $this->db->where('status', '1');
        
        $ordeby=($sortby!='') ? $sortby : 'asc';
        $this->db->order_by($orderbyField, $ordeby); 
        $query = $this->db->get('user_files as a');
        $data=  array();
        $data = $query->result_array();
        return $data;
    }
    
    /**
     * Function for get listing of comments from "comments" table
     * 
     * @param int $albumid primary id of a album
     * 
     * @param int $type , 1-images, 2-videos
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_albums_comments($albumid, $type) {
        $this->db->select('a.id, a.content_id, a.user_id, a.added_date, a.comment, b.stage_name');
        $this->db->from('comments as a');
        $this->db->join('users b', 'a.user_id = b.id', 'left'); 
        $this->db->where('a.content_id', $albumid);
        $this->db->where('a.content_type', $type);
        $this->db->order_by("a.id", "desc"); 
        $query = $this->db->get();
        $data=  array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
     * Function for get listing of comments from "comments" table
     * 
     * @param int $albumid primary id of a album
     * 
     * @param int $type , 1-images, 2-videos, 3-files, 4-video files, 5-image files, 6-files file
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/
    public function list_all_comments($table, $albumid, $type) {
        switch ($table) {
            case 'videos':
                $inqry='SELECT id FROM `user_videos` WHERE video_folder_id ="'.$albumid.'"';

                break;
            
            case 'files':
                $inqry='SELECT id FROM `user_files` WHERE file_folder_id ="'.$albumid.'"';

                break;
            
            case 'images':
                $inqry='SELECT id FROM `album_images` WHERE album_id ="'.$albumid.'"';

                break;
            
            case 'webpages':
                $inqry='SELECT id FROM `webpages` WHERE albumid ="'.$albumid.'"';

                break;
        }
        $qry='SELECT a.id, a.content_id, a.user_id, a.added_date, a.comment, b.stage_name FROM `comments` as a
                LEFT JOIN users as b
                ON a.user_id = b.id
                WHERE content_type="'.$type.'" && content_id IN('.$inqry.')
                ORDER BY a.content_id ASC, a.id DESC';
        $query=$this->db->query($qry);
        $data = $query->result_array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
     * Function for remove project data from "project_users", "projects", "user_albums", "album_images" table
     * 
     * @param int $projectId primary id of a project
     *
     * @access public
     * 
     * @return $delete if query run successfull
     **/
    public function remove_project_data($projectId, $action='delete') {
        if($action=='delete'){
            $data = array(
               'status' => '0',
            );        
        }
        else{
            $data = array(
               'status' => '1',
            );        
        }
        
        //delete from projects table
        $this->db->where('id', $projectId);
        //$delete = $this->db->delete('projects');
        $delete = $this->db->update('projects', $data);
        
        // delete from project_users table
        $this->db->where('project_id', $projectId);
        $delete = $this->db->delete('project_users');
        
        $albumIds = $this->db->select("GROUP_CONCAT(id) as albumIds")->get_where('user_albums', array('project_id' => $projectId))->row()->albumIds;
        
        $videosFolderIds = $this->db->select("GROUP_CONCAT(id) as videosFolderIds")->get_where('videos_folder', array('project_id' => $projectId))->row()->videosFolderIds;
        
        $filesFolderIds = $this->db->select("GROUP_CONCAT(id) as filesFolderIds")->get_where('files_folder', array('project_id' => $projectId))->row()->filesFolderIds;
        
        // delete from user_albums table
        
        $this->db->where('project_id', $projectId);
        //$delete = $this->db->delete('user_albums');
        $delete = $this->db->update('user_albums', $data);
        
        // delete from videos_folder table
        
        $this->db->where('project_id', $projectId);
        //$delete = $this->db->delete('videos_folder');
        $delete = $this->db->update('videos_folder', $data);
        
        // delete from files_folder table
        
        $this->db->where('project_id', $projectId);
        //$delete = $this->db->delete('files_folder');
        $delete = $this->db->update('files_folder', $data);
        
        
        // delete from album_images table
        $albumIdsArr=explode(',', $albumIds);
        $this->db->where_in('album_id', $albumIdsArr);
        //$delete = $this->db->delete('album_images');
        $delete = $this->db->update('album_images', $data);
        
        // delete from user_videos table
        $videoFolderIdsArr=explode(',', $videosFolderIds);
        if(count($videoFolderIdsArr)>0){
            $this->db->where_in('video_folder_id', $videoFolderIdsArr);
            //$delete = $this->db->delete('user_videos');
            $delete = $this->db->update('user_videos', $data);
        }
        
        // delete from user_files table
        $fileFolderIdsArr=explode(',', $filesFolderIds);
        if(count($videoFolderIdsArr)>0){
            $this->db->where_in('file_folder_id', $fileFolderIdsArr);
            //$delete = $this->db->delete('user_files');
            $delete = $this->db->update('user_files', $data);
        }
        
        if($action=='delete'){
            $last_remove_array =  array('type'=>'project', 'table'=>'projects', 'id'=>$projectId);
            $this->session->set_userdata('last_remove', $last_remove_array);
        }
        
        return $delete;
    }
    
    /**
     * Function for show details of current view project
     * @param int $projectid primary id of project
     *
     * @access public
     * 
     * @return array Containing a multidimensional associative array with the recordsets
     **/  
    function view_project_detail($projectid) {
        $this->db->select('identifier, title, thumbnail, view_only_mode');
        $this->db->from('projects');
        $this->db->where('id ', $projectid);
        $query = $this->db->get();
        $data=  array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
    * Function for remove album
    *
    * @param int $albumid primary id from "user_albums" table
    * 
    * @access public
    * 
    **/
    
    public function remove_album($albumid, $action='delete', $fromTable='') {
        if($action=='delete'){
            $data = array(
               'status' => '0',
            );        
        }
        else{
            $data = array(
               'status' => '1',
            );        
        }
        $this->db->where('id', $albumid);
        $table=  ($this->input->post('table')!='') ? $this->input->post('table') : $fromTable;
        if($table=='images'){
            //$this->db->delete('user_albums');
            $this->db->update('user_albums', $data);
            
            $this->db->where('album_id', $albumid);
            //$this->db->delete('album_images'); 
            $this->db->update('album_images', $data); 
            
            $projectid = $this->db->select('project_id')->get_where('user_albums', array('id' => $albumid))->row()->project_id;
        }
        else if($table=='videos'){
            //$this->db->delete('videos_folder');
            $this->db->update('videos_folder', $data);
            
            $this->db->where('video_folder_id', $albumid);
            //$this->db->delete('user_videos'); 
            $this->db->update('user_videos', $data); 
            
            $projectid = $this->db->select('project_id')->get_where('videos_folder', array('id' => $albumid))->row()->project_id;
        }
        else if($table=='webpages'){
            //$this->db->delete('webpages_albums');
            $this->db->update('webpages_albums', $data);
            
            $this->db->where('albumid', $albumid);
            //$this->db->delete('webpages'); 
            $this->db->update('webpages', $data); 
            
            $projectid = $this->db->select('project_id')->get_where('webpages_albums', array('id' => $albumid))->row()->project_id;
        }
        else if($table=='files'){
            //$this->db->delete('files_folder');
            $this->db->update('files_folder', $data);
            
            $this->db->where('file_folder_id', $albumid);

            $this->db->update('user_files', $data); 
            
            $projectid = $this->db->select('project_id')->get_where('files_folder', array('id' => $albumid))->row()->project_id;
        }
        if($action=='delete'){
            $last_remove_array =  array('type'=>'album', 'table'=>$table, 'id'=>$albumid, 'projectid'=>$projectid);
            $this->session->set_userdata('last_remove', $last_remove_array);
        }

    }
   
   
   /**
    * Function for remove album comments
    *
    * @param int $commentid primary id from "comments" table
    * 
    * @access public
    * 
    **/
    public function remove_comment($commentid) {
        $this->db->where('id', $commentid);
        $this->db->delete('comments'); 
   }
   
   
   /**
    * Function for remove album images
    *
    * @param string $type like videos, images or files
    * @param int $imageid primary id from "album_images" table
    * 
    * @access public
    * 
    **/
    public function remove_album_image($type, $imageid, $action='delete') {
        if($action=='delete'){
            $data = array(
               'status' => '0',
            );        
        }
        else{
            $data = array(
               'status' => '1',
            );        
        }
        $this->db->where('id', $imageid);
        //$this->db->or_where('aliases', $imageid); 
        switch ($type) {
            case 'images':
               // die('die');
                $this->db->or_where('aliases', $imageid); 
                $this->db->or_where('image_version', $imageid); 
                //$this->db->delete('album_images'); 
                $this->db->update('album_images', $data); 
                //echo $str = $this->db->last_query();
                //die('die');
                $albumid = $this->db->select('album_id')->get_where('album_images', array('id' => $imageid))->row()->album_id;
                $projectid = $this->db->select('project_id')->get_where('user_albums', array('id' => $albumid))->row()->project_id;
                break;

            case 'videos':
                $this->db->or_where('aliases', $imageid); 
                //$this->db->delete('user_videos'); 
                $this->db->update('user_videos', $data); 
                $albumid = $this->db->select('video_folder_id')->get_where('user_videos', array('id' => $imageid))->row()->video_folder_id;
                $projectid = $this->db->select('project_id')->get_where('videos_folder', array('id' => $albumid))->row()->project_id;
                break;
            
            case 'files':
                $this->db->or_where('aliases', $imageid); 
                //$this->db->delete('user_files'); 
                $this->db->update('user_files', $data); 
                $albumid = $this->db->select('file_folder_id')->get_where('user_files', array('id' => $imageid))->row()->file_folder_id;
                $projectid = $this->db->select('project_id')->get_where('files_folder', array('id' => $albumid))->row()->project_id;
                break;
            
            case 'webpages':
                //$this->db->delete('webpages'); 
                $this->db->update('webpages', $data);
                $albumid = $this->db->select('albumid')->get_where('webpages', array('id' => $imageid))->row()->albumid;
                $projectid = $this->db->select('project_id')->get_where('webpages_albums', array('id' => $albumid))->row()->project_id;
                break;
        }
        
        if($action=='delete'){
            $last_remove_array =  array('type'=>'assets', 'table'=>$type, 'id'=>$imageid, 'albumid'=>$albumid, 'projectid'=>$projectid);
            $this->session->set_userdata('last_remove', $last_remove_array);
        }
                
   }
   
   /**
    * Function for update image details in "album_images"
    *
    * @param int $imageid primary id from "album_images" table
    * 
    * @access public
    * 
    **/
   function album_image_details($imageid) {
       $data = array(
                'title' =>$this->input->post('image_title'),
                'additional_info' => $this->input->post('additional_info'),
             );
        $id=$this->input->post('album_imageid_hidden');
        $this->db->where('id', $id);
        $this->db->update('album_images',$data);
   }
   
   
   /**
    * Function for update vimeo_id field in "user_videos"
    *
    * @param int $videoid primary id of video from "user_videos" table
    * @param int $vimeoid vimeo id from vimeo.com
    * 
    * @access public
    * 
    **/
   function update_vimeo_id($videoid, $vimeoid) {
        
        $data = array(
                'vimeo_id' =>$vimeoid,
             );
        $this->db->where('id', $videoid);
        $this->db->update('user_videos',$data);
   }
   
   /**
    * Function for add comments of albums in "comments" table
    *
    * @param int $albumid primary id from "user_albums" table
    * 
    * @param int $type , 1-images, 2-videos, 3-files
    * 
    * @access public
    * 
    **/
   function add_album_comment($albumid, $type) {
        $data = array(
            'content_id' =>$this->input->post('album_comment_hidden'),
            'user_id' => $this->session->userdata('user_id'),
            'comment'=>$this->input->post('comment'),
            'added_date'=>date('Y-m-d H:i:s'),
            'content_type'=>$type
        );
        $this->db->insert('comments',$data);
        $insert_id = $this->db->insert_id();
   }
   
   /**
    * Function for edit comments of albums in "comments" table
    *
    * @param int $commentid primary id from "comments" table
    * 
    * @access public
    * 
    **/
   function edit_album_comment($commentid) {
        $data = array(
            'comment'=>$this->input->post('comment_edit'),
        );
        $this->db->where('id', $commentid);
        $this->db->update('comments',$data);
   }
   
   /**
    * Function for sorting albums in "user_albums" table
    *
    * @access public
    * 
    * @return true if sorting successfully done 
    **/
   function sort_albums(){
       $albumid= $this->input->post('id');
       $type= $this->input->post('type');
       $albumid=str_replace("id_", '', $albumid);
       $albumidArr=explode(',', $albumid);
       //echo '<pre>'.$type; print_r($albumidArr); 
       $i=1;
       if($type=='images'){
           $tablename='user_albums';
       }
       else if($type=='videos'){
           $tablename='videos_folder';
       }
       else if($type=='files'){
           $tablename='files_folder';
       }
       else if($type=='projects'){
           $tablename='projects';
       }
       else if($type=='webpages'){
           $tablename='webpages_albums';
       }
       else if($type=='user_files'){
           $tablename='user_files';
       }
       else if($type=='user_videos'){
           $tablename='user_videos';
       }
       foreach ($albumidArr as $id){
            $data = array(
                'sort_number'=>$i
            );
            // echo 'sort_number='.$i;
             
            $this->db->where('id', $id);
            $this->db->update($tablename, $data);
            //echo $str = $this->db->last_query();
            $i++;
       }
       die;
       return true;
   }
   
   /**
    * Function for sorting albums images in "album_images" table
    *
    * @access public
    * 
    * @return true if sorting successfully done 
    **/
   function sort_albums_images(){
       $albumid=  $this->input->post('id');
       $albumid=str_replace("id_", '', $albumid);
       $albumidArr=explode(',', $albumid);
       $i=1;
       foreach ($albumidArr as $id){
            $data = array(
                'sort_num'=>$i,
            );
            $this->db->where('id', $id);
            $this->db->update('album_images',$data);
            $i++;
       }
       return true;
   }
   
   /**
    * Function for search  content from 
    *
    * @param int $albumid primary id from "user_albums" table
    * 
    * @param int $type , 1-images, 2-videos, 3-files
    * 
    * @access public
    * 
    **/
   function search_data($search_txt, $project, $type, $limit, $sortfield, $sort){
       $userid=$this->session->userdata('user_id');
       $where='';
       $join='';
       /*
        * arun comment
       if($project==''){
           $where='c.userid='.$userid.' || (d.user_id='.$userid.' && d.role="4") && ';
           $join='LEFT JOIN project_users as d
            ON c.id=d.project_id ';
       }
        * 
        */
        
        if($search_txt!='')
           $where.="tmp.title like '%".$search_txt."%' || tmp.additional_info like '%".$search_txt."%' && ";
        
        if($project!=''){
            $where.="tmp.project_id='".$project."' && ";
        }
        
        else{
            $query="SELECT a.id FROM `projects` as a 
                    LEFT JOIN `project_users` as b
                    ON a.id=b.project_id
                    WHERE (a.userid='".$userid."' || (b.user_id='".$userid."' && b.role='4')) group by a.id";
            //$this->db->where('a.role', $roleid);
            $q=$this->db->query($query);
            //$data=$q->result_array();
            $projectidArr=  array();
            foreach ($q->result() as $row)
            {
                array_push($projectidArr, $row->id);
            }
            
            $project=implode(',', $projectidArr);
            if($project!='')
                $where.="tmp.project_id IN (".$project.") && ";
        }
         
        $data=  array();
        if($project!=''){
            if($type!='')
            $where.="tmp.type='".$type."' && ";
        
            if($where!='')
                $where='WHERE '.$where;

            $where=trim($where, '&& ');

            $limit='limit '.$limit;

            $orderby='order by '.$sortfield.' '.$sort;

            $qry="SELECT tmp.*, b.stage_name, c.identifier FROM (
                (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, a.thumbnail, b.project_id, a.album_id as albumid, 'images' as type, 
                    IF((SELECT count(id) FROM files_seen WHERE userid=".$userid." && fileid=a.id && type='images')>0, 'yes', 'no') as seen
                FROM album_images as a
                JOIN user_albums as b
                ON a.album_id=b.id)
                UNION ALL
                (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, a.thumbnail, b.project_id, a.file_folder_id as albumid, 'files' as type, 
                    IF((SELECT count(id) FROM files_seen WHERE userid=".$userid." && fileid=a.id && type='files')>0, 'yes', 'no') as seen
                FROM user_files as a
                JOIN files_folder as b
                ON a.file_folder_id=b.id)
                UNION ALL
                (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, (IF(a.vimeo_thumbnail != '', a.vimeo_thumbnail, a.thumbnail)) as thumbnail, b.project_id, a.video_folder_id as albumid, 'videos' as type, 
                    IF((SELECT count(id) FROM files_seen WHERE userid=".$userid." && fileid=a.id && type='videos')>0, 'yes', 'no') as seen 
                FROM user_videos as a
                JOIN videos_folder as b
                ON a.video_folder_id=b.id)
                ) as tmp
                JOIN users as b
                ON tmp.userid=b.id
                JOIN projects as c
                ON tmp.project_id=c.id ".$join." ".$where." ".$orderby." ".$limit;
            $query=$this->db->query($qry);
            $data = $query->result_array();
        }
        
        return $data;
        
        
        /*
         * query
         * SELECT tmp.*, b.stage_name, c.identifier FROM (
            (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, a.thumbnail, b.project_id, a.album_id as albumid, 'images' as type, 
                IF((SELECT count(id) FROM files_seen WHERE userid=a.userid && fileid=a.id && type='images')>0, 'yes', 'no') as seen
            FROM album_images as a
            JOIN user_albums as b
            ON a.album_id=b.id)
            UNION ALL
            (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, a.thumbnail, b.project_id, a.file_folder_id as albumid, 'files' as type, 
                IF((SELECT count(id) FROM files_seen WHERE userid=a.userid && fileid=a.id && type='files')>0, 'yes', 'no') as seen
            FROM user_files as a
            JOIN files_folder as b
            ON a.file_folder_id=b.id)
            UNION ALL
            (SELECT a.id, a.title, a.additional_info, DATE_FORMAT(a.added_date,'%Y %b %d | %h:%i %p') as added_date, a.userid, a.thumbnail, b.project_id, a.video_folder_id as albumid, 'videos' as type, 
                IF((SELECT count(id) FROM files_seen WHERE userid=a.userid && fileid=a.id && type='videos')>0, 'yes', 'no') as seen 
            FROM user_videos as a
            JOIN videos_folder as b
            ON a.video_folder_id=b.id)
            ) as tmp
            JOIN users as b
            ON tmp.userid=b.id
            JOIN projects as c
            ON tmp.project_id=c.id limit 10
         */
   }
   
   /**
    * Function for insert seen file 
    *
    * @param int $fileid primary id from file
    * 
    * @param int $type , like images, videos, files
    * 
    * @access public
    * 
    **/
   public function insertSeen($fileid, $userid, $type) {
        $this->db->select('id');
        $this->db->from('files_seen');
        $this->db->where('userid ', $userid);
        $this->db->where('fileid ', $fileid);
        $this->db->where('type ', $type);
        $query = $this->db->get();
        //$data=  array();
        //check number of rows of "files_seen" table
        if($query->num_rows() == 0)
        {
            $data = array(
                'userid' =>$userid,
                'fileid' => $fileid,
                'type'=>$type,
                'date'=>date('Y-m-d H:i:s'),
            );
            $this->db->insert('files_seen',$data);
            $insert_id = $this->db->insert_id();
            //$data = $query->result_array();
        }
       
   }
   
   /**
    * Function for list of project blogs 
    *
    * @param int $projectId primary id from project
    * 
    * @access public
    * 
    **/
    function blog($projectId){
        $this->db->select('a.id, a.user_id, a.comment, a.added_date, b.stage_name');
        $this->db->from('blog as a');
        $this->db->join('users b', 'a.user_id = b.id', 'left'); 
        $this->db->where('a.project_id', $projectId);
        $this->db->order_by("a.id", "desc"); 
        $query = $this->db->get();
        $data=  array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0){
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
    * Function for add blog comment in "blog" table
    *
    * @param int $userid primary id from login user 
    * @param int $projectId primary id from project
    * @param string $comment comment of project
    * 
    * @access public
    * 
    **/
    function add_blog_comment($userid, $projectid, $comment){
        $data = array(
            'user_id' => $userid,
            'project_id' =>$projectid,
            'comment'=>$comment,
            'added_date'=>date('Y-m-d H:i:s')
        );
        $this->db->insert('blog',$data);
        return $insert_id = $this->db->insert_id();
    }
    
    /**
    * Function for edit blog comment 
    *
    * @param int $userid primary id from login user 
    * @param int $commentid primary id from comment
    * @param string $comment comment of project
    * 
    * @access public
    * 
    **/
    function edit_blog_comment($userid, $commentid, $comment){
        $data = array(
            'comment'=>$comment,
        );
        $this->db->where('id', $commentid);
        $this->db->where('user_id', $userid);
        $this->db->update('blog',$data);
        return $insert_id = $this->db->insert_id();
    }
    
    /**
    * Function for remove blog comment 
    *
    * @param int $commentid primary id from blog
    * 
    * @access public
    * 
    **/
    function removeCommentBlog($commentId, $userid){
        $this->db->where('id', $commentId);
        $this->db->where('user_id', $userid);
        $delete = $this->db->delete('blog');
    }
    
    public function newBlogComments($projectid, $commentid) {
        $data=  array();
        //check number of rows of "projects" table
        //$x=true;
       // while($x) {
            $this->db->select('a.id, a.user_id, a.comment, a.added_date, b.stage_name');
            $this->db->from('blog as a');
            $this->db->join('users b', 'a.user_id = b.id', 'left'); 
            $this->db->where('a.project_id', $projectid);
            $this->db->where('a.id >', $commentid);
            $this->db->order_by("a.id", "desc"); 
            $query = $this->db->get();
            if($query->num_rows() > 0){
                //$x=false;
                $data = $query->result_array();
                
            }
            return $data;
       // } 
        
        
    }
    
    public function getBlogComment($projectid, $commentid) {
        $this->db->select('comment');
        $this->db->from('blog');
        $this->db->where('project_id', $projectid);
        $this->db->where('id', $commentid);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $data = $query->result_array();
        }
        return $data;
    }
    
    public function checkProjectStatus($projectid, $userid){
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->where('id', $projectid);
        $this->db->where('userid', $userid);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $status= 'owner';
        }
        else{
            $this->db->select('role');
            $this->db->from('project_users');
            $this->db->where('project_id', $projectid);
            $this->db->where('user_id', $userid);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                $data = $query->row();
                $statusval=$data->role;
                switch ($statusval) {
                    case '1':
                        $status= 'none';
                    break;
                    case '2':
                        $status= 'viewer';
                    break;
                    case '3':
                        $status= 'contributor';
                    break;
                    case '4':
                        $status= 'co-owner';
                    break;
                }
            }
            else{
                $status= 'none';
            }
        }
        return $status;
    }
    
    function undo_last_remove(){
        $last_remove_array=$this->session->userdata('last_remove');
        if(is_array($last_remove_array)){
            $last_remove_array=$this->session->userdata('last_remove');
            //$last_remove_array =  array('type'=>'assets', 'table'=>$type, 'id'=>$imageid);
            switch($last_remove_array['type']){
                case 'project':
                    $this->remove_project_data($last_remove_array['id'], 'undo');
                break;
            
                case 'album':
                    $this->remove_album($last_remove_array['id'], 'undo', $last_remove_array['table']);
                break;
            
                case 'assets':
                    $this->remove_album_image($last_remove_array['table'], $last_remove_array['id'], 'undo');
                break;
            }
        }
    }
    
    function getRefreshToken(){
        $this->db->select('refresh_token');
        //$this->db->from('box_token');
        $this->db->order_by("id", "desc"); 
        $this->db->limit(1);
        $query = $this->db->get('box_token');
        $data = $query->row();
        return $data->refresh_token;
    }
    
    function insertRefreshToken($accessToken, $refreshToken){
        $data = array(
                'status' => 'used'        
             );
        $this->db->update('box_token',$data);
        
        $data = array(
                'token' => $accessToken,
                'refresh_token' => $refreshToken                
             );
        $this->db->insert('box_token',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
}
?>