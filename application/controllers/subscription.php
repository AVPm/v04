<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Subscription controller class file
 *
 */
class Subscription extends CI_Controller{
	public function __construct(){
            /*
            * call the constructor of CI
            */
            parent::__construct();
            /*
            * call the subscription model file
            */
            $this->load->model('subscription_model');
	}
        
        /**
         * Function for get details of subscription pack
         *
         * @access public
         * 
         * @return details in array 
        **/
        public function getdetails() {
            $id= $this->input->post('id');
            $details= $this->subscription_model->getPackDetails($id);
            echo json_encode($details);
        }
        
        
        public function buy($packid) {
            if($this->session->userdata('user_id')>0){
                $this->session->set_flashdata('place_order', 'Your Order has been successfully placed');
                
                //place order of user in "sales" table
                $invoice=date("His").rand(1234, 9632);
                $orderId=  $this->subscription_model->insertOrder($packid, $invoice);
                
                
                
                $userid=$this->session->userdata('user_id');
                
                $subscriptionId=$packid;
                
                
                //$orderId= $this->user_model->addSales($userid, $subscriptionId, $invoice);
                
                if($subscriptionId>1){
                    $subscriptionName = $this->db->select('name')->get_where('subscription', array('id' => $subscriptionId))->row()->name;
                    $amount = $this->db->select('price')->get_where('subscription', array('id' => $subscriptionId))->row()->price;
                    $userDetails=  $this->subscription_model->user_details($userid);
                    //echo '<pre>'; print_r($userDetails); die();
                    $form='<form action="'.base_url("includes/paypal/paypal.php?sandbox=1").'" method="post" id="paypalForm"> 
                                <input type="hidden" name="action" value="process" />
                                <input type="hidden" name="cmd" value="_cart" /> 
                                <input type="hidden" name="currency_code" value="USD" />
                                <input type="hidden" name="invoice" value="'.$invoice.'" />
                                <input type="hidden" name="product_id" value="'.$subscriptionId.'" />
                                <input type="hidden" name="product_name" value="'.$subscriptionName.'" />
                                <input type="hidden" name="product_quantity" value="1" />
                                <input type="hidden" name="product_amount" value="'.$amount.'" />
                                <input type="hidden" name="payer_fname" value="'.$userDetails['details']['first_name'].'" />
                                <input type="hidden" name="payer_lname" value="'.$userDetails['details']['last_name'].'" />
                                <input type="hidden" name="payer_email" value="'.$userDetails['details']['email'].'" />
                            </form>
                            <script>document.getElementById("paypalForm").submit();</script>';
                    echo $form;
                    die();
                }
                
                redirect('user/subscriptions');
            }
            else{
                redirect('user');
            }
            
        }
        
    
        public function activate($invoice) {
            if($this->session->userdata('user_id')>0){
                if($invoice!=''){
                    $userid=$this->session->userdata('user_id');
                    $subscriptionId = $this->db->select('subscription_id')->get_where('sales', array('invoice' => $invoice))->row()->subscription_id;
                    $subscriptionName = $this->db->select('name')->get_where('subscription', array('id' => $subscriptionId))->row()->name;
                    $amount = $this->db->select('price')->get_where('subscription', array('id' => $subscriptionId))->row()->price;
                    $userDetails=  $this->subscription_model->user_details($userid);
                    //echo '<pre>'; print_r($userDetails); die();
                    $form='<form action="'.base_url("includes/paypal/paypal.php?sandbox=1").'" method="post" id="paypalForm"> 
                                <input type="hidden" name="action" value="process" />
                                <input type="hidden" name="cmd" value="_cart" /> 
                                <input type="hidden" name="currency_code" value="USD" />
                                <input type="hidden" name="invoice" value="'.$invoice.'" />
                                <input type="hidden" name="product_id" value="'.$subscriptionId.'" />
                                <input type="hidden" name="product_name" value="'.$subscriptionName.'" />
                                <input type="hidden" name="product_quantity" value="1" />
                                <input type="hidden" name="product_amount" value="'.$amount.'" />
                                <input type="hidden" name="payer_fname" value="'.$userDetails['details']['first_name'].'" />
                                <input type="hidden" name="payer_lname" value="'.$userDetails['details']['last_name'].'" />
                                <input type="hidden" name="payer_email" value="'.$userDetails['details']['email'].'" />
                            </form>
                            <script>document.getElementById("paypalForm").submit();</script>';
                    echo $form;
                    die();
                }
            }
            else{
                redirect('user');
            }
        }
       
}
?>