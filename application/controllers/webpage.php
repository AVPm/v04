<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * web pages controller class file
 *
 */

//include_once($_SERVER['DOCUMENT_ROOT'].'/includes/paypal/paypal.php');

class Webpage extends CI_Controller{
	public function __construct()
	{
            /*
            * call the constructor of CI
            */
            parent::__construct();
            /*
            * call the user model file
            */
            $this->load->model('user_model');
            
            /*
             * call image crope library
             */
            $this->load->library('unzip');
	}
        
        public function index(){
            die('coming soon');
        }
        
        public function extract() {
            // Optional: Only take out these files, anything else is ignored
            $this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf'));

            // Give it one parameter and it will extract to the same folder
            $this->unzip->extract('uploads/webpages/timber.rar');

            // or specify a destination directory
            //$this->unzip->extract('uploads/my_archive.zip', '/path/to/directory/');
        }
}
?>