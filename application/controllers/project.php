<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * project controller class file
 *
 */
include_once(constant('FILE_INCLUDE_PATH').'/includes/vimeo.php');
include_once(constant('FILE_INCLUDE_PATH').'/includes/DocumentManager.php');
class Project extends CI_Controller{
    public $client_id;
    public $client_secret;
	public function __construct(){
            /*
            * call the constructor of CI
            */
            parent::__construct();
            /*
            * call the project model file
            */
            $this->load->model('project_model');
            $this->load->helper('download');
            $this->load->library('unzip');
            
            $this->client_id = 'o3s2ds1jp86wwi9blik0dkm7ne2408is';
            $this->client_secret = 'xbK4F1n3Nimr4PCJDvY79uvsh4G7gOjr';
            /*
            if(($this->session->userdata('user_id')=="")){
                redirect('/user/login/', 'refresh');
            }
             * 
             */
	}
        
        /**
         * Function for load top menu
         *
         * @access public
         * 
         * @return top menu array 
        **/
        
        public function topMenu($projectId=''){
            //$menu=array('/project/images/'.$projectId=>'images', '/project/videos/'.$projectId=>'videos', '/project/files/'.$projectId=>'files', '/project/plans/'.$projectId=>'plans');
            $menu=array('project/images/'.$projectId=>'images', 'project/videos/'.$projectId=>'videos', 'project/files/'.$projectId=>'files', 'project/webpages/'.$projectId=>'webpages');
            if($this->session->userdata('user_id')>0)
                $menu['user/subscriptions']='subscriptions';
            return $menu;
        }
        
        
        /**
         * Function for check user login or not
         *
         * @access public
         * 
        **/
        
        public function checkUserLogin(){
            if(($this->session->userdata('user_id')=="")){
                redirect('/user/login/', 'refresh');
            }
        }
        
        
        /**
         * Function for check project view only mode available or not
         *
         * @access public
         * 
         * @return true if available or false if not available 
        **/
        
        public function checkViewOnlyMode($projectId){
            $view_mode = $this->db->select('view_only_mode')->get_where('projects', array('id' => $projectId))->row()->view_only_mode;
            $view=($view_mode==1) ? 'true' : 'false';
            return $view;
        }

        /**
         * Function for load main view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * 
         * @access public
         * 
         * @return project view html from project_view.php 
        **/
        public function view($projectId) {
            $this->checkUserLogin();
            $userid=$this->session->userdata('user_id');
            //for add comments in album
            if($this->input->post('projectid_blog_hidden')){
                $update=$this->project_model->add_blog_comment($userid, $this->input->post('projectid_blog_hidden'), $this->input->post('comment'));
                redirect(current_url());
            }
            
            //for edit comments of album
            else if($this->input->post('commentid_hidden')){
                $update=$this->project_model->edit_blog_comment($userid, $this->input->post('commentid_hidden'), $this->input->post('comment_edit'));
                redirect(current_url());
            }

            
            //die('Page under construction');
            $this->load->model('user_model');
            //$this->project_model->view_detail($projectId);
            /*
             * $data['title'] => means title of web page and value is project title from "projects" table
             * $data['project_userid'] => get all user id whose are added with project from "project_users" table
             * $data['user_listing'] => get user listing of login user from "user_relation" table
             * $data['user_projects'] => get user project list of login user from "projects" table
             * $data['jsArray'] => get all js files to show on page
             * $data['cssArray'] => get all css files to show on page
             * $data['main_content'] => main html file for view
             * $data['data'] => to show data on web page
             */
            $roleid='1';
            $roleid2='';
            if($this->session->userdata('project_role')!=''){
                $roleid=$this->session->userdata('project_role');
            }
            if($this->session->userdata('project_role2')!=''){
                $roleid2=$this->session->userdata('project_role2');
            }
            $data['title']= $this->project_model->project_title($projectId);
            
            $data['project_users']=  $this->project_model->project_users($projectId);

            $data['user_listing']=  $this->user_model->user_listing($userid);

            $data['user_projects']= $this->user_model->user_projects($userid, $roleid);
            
            //$data['project_participated']= $this->user_model->project_participated($this->session->userdata('user_id'));
            if($roleid2!='' && $roleid!=$roleid2){
                $data['project_participated']= $this->user_model->user_projects($userid, $roleid2);
            }
            else{
                $data['project_participated']='';
            }
            
            $all_projects= $this->user_model->allProjects($userid);
            
            $blog= $this->project_model->blog($projectId);
            $lastCommentid='';
            $lastComment='';
            foreach ($blog as $key => $value) {
                if($value['user_id']==$userid){
                    $lastCommentid=$value['id'];
                    $lastComment=$value['comment'];
                    break;
                }
            }
            
            $projectStatus=  $this->project_model->checkProjectStatus($projectId, $userid);
            $data['view_project_detail']=$this->project_model->view_project_detail($projectId);
            
            $data['top_menu']= $this->topMenu($projectId);
            $user_stagename = $this->db->select('stage_name')->get_where('users', array('id' => $userid))->row()->stage_name;
            $data['main_content']='project_view';
            $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
            $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
            $data['data']=  array('projectId'=>$projectId, 'roleid'=>$roleid, 'all_projects'=>$all_projects, 'blog'=>$blog, 'stage_name'=>$user_stagename, 'last_commentid'=>$lastCommentid, 'last_comment'=>$lastComment, 'project_status'=>$projectStatus);
            $this->load->view('template', $data);
        }
        
        /**
         * Function for add user in project's user list
         * 
         * @param int $userid is the primary id of a user comes from "users" table of that user who you want to add in list of project's user
         * @param int $projectid is the primary id of a project comes from "projects" table
         *
         * @access public
         * 
         * @print message if user added successfully or not
         * 
        **/
        function adduserproject($userid, $projectid) {  
            $this->checkUserLogin();
            $adduser=$this->project_model->add_userProject($userid, $projectid);
            if($adduser>0):
                echo 'User added successfully';
            else:
                echo 'There is some problem to add this user';
            endif;
        }
        
        /**
         * Function for remove user from project's user list
         * 
         * @param int $userid is the primary id of a user comes from "users" table of that user who you want to add in list of project's user
         * @param int $projectid is the primary id of a project comes from "projects" table
         *
         * @access public
         * 
         * @print message if user deleted successfully or not
         * 
        **/
        function removeuserproject($userid, $projectid) {
            $this->checkUserLogin();
            $removeuser=$this->project_model->remove_userProject($userid, $projectid);
            if($removeuser):
                echo 'User deleted successfully';
            else:
                echo 'There is some problem to delete this user';
            endif;
        }
       
        
        /**
         * Function for view projects on top accordingly role type
         * 
         * @param int $roleid is the role id of a user
         *
         * @access public
         * 
         * @return load welcome function of user controller
         * 
        **/
        public function role($roleid='', $roleid2='') {  
            $this->checkUserLogin();
            //load user controler file
            $this->load->library('../controllers/user');
            $this->user->welcome($roleid, $roleid2);
            //$this->load->model('user_model');
            //$this->user_model->welcome($roleid);
        }
        
        /**
         * Function for load images view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * 
         * @access public
         * 
         * @return project view html from project_view.php 
        **/
        public function images($projectId, $albumid='', $imageid='', $imageversion='') {
            $view=$this->checkViewOnlyMode($projectId);
            if($view=='false'){
                $this->checkUserLogin();
            }
            $userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
            //die($albumid);
            //if(($this->session->userdata('user_id')!="")){
                if($this->input->post('album_name')){
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //$thumbExt=  $this->input->post('thumbnail_type');
                        $thumbExt='png';
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', $thumbExt);
                    }
                    $insertId=$this->project_model->save_album($projectId, $filename, 'images', $thumbExt);
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                else if($this->input->post('album_hidden')){
                    
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //$thumbExt=  $this->input->post('thumbnail_type');
                        $thumbExt='png';
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', $thumbExt);
                    }
                    $insertId=$this->project_model->edit_album($filename, 'images', $thumbExt);
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                else if($this->input->post('album_imageid_hidden')){
                    $update=$this->project_model->album_image_details($this->input->post('album_imageid_hidden'));
                    redirect(current_url());
                }
                
                //for add comments in album
                else if($this->input->post('album_comment_hidden')){
                    $update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '5');
                    redirect(current_url());
                }
                
                //for edit comments of album
                else if($this->input->post('commentid_hidden')){
                    $update=$this->project_model->edit_album_comment($this->input->post('commentid_hidden'));
                    redirect(current_url());
                }
                
                $data['title']= $this->project_model->project_title($projectId);
                $data['top_menu']=  $this->topMenu($projectId);
                $data['main_content']='project_images_view';
                $data['selected_menu']='images';
                //$data['jsArray']=array('dashboard');
                $data['cssArray']=array('dashboard', 'images_view');
                
                //get listing of albums of images
                $albums = $this->project_model->list_user_albums($userid, $projectId, 'user_albums');
                
                if($albumid==''){
                    if(!empty($albums)){
                        $newarr = $albums[0];
                        $albumid=$newarr['id'];
                    }
                }
                
                @$image_upscale = $this->db->select('upscale')->get_where('user_albums', array('id' => $albumid))->row()->upscale;
                
                $data['image_upscale']=($image_upscale==0) ? 'false' : 'true';
                
                // get listing of all images of a album
                $album_images=$this->project_model->list_albums_images($albumid);
                
                
                $album_images_versions=  array();
                $i=0;
                foreach ($album_images as $key=>$value){
                    if($value['image_version']>0){
                        //array_push($album_images_versions, $value['image_version']);
                        $album_images_versions[$i]=  array('image_id'=>$value['id'], 'image_version_id'=>$value['image_version'], 'full_image'=>$value['full_image']);
                        $i++;
                    }
                }
                
                $data['album_images_versions']=$album_images_versions;
                $largeImg=  array();
                $largeImg['imageid']='';
                $largeImg['fullimage']='';
                $data['image_thumb']='';
                $data['image_title']='';
                $data['image_info']='';
                
                //$imageid='';
                if($imageid==''){
                    if(!empty($album_images)){                        
                        $newarr = $album_images[0];
                        $largeImg['imageid']=$newarr['id'];
                        $largeImg['fullimage']=$newarr['full_image'];
                        $data['image_thumb']=$newarr['thumbnail'];
                        $data['image_title']=$newarr['title'];
                        $data['image_info']=$newarr['additional_info'];
                        $imageid=$newarr['id'];
                    }
                }
                else{
                    //die('dsd');
                    $largeImg['imageid']=$imageid;
                    foreach ($album_images as $key => $value) {
                        if($value['id']==$imageid){
                            $largeImg['fullimage']=$value['full_image'];
                            $data['image_thumb']=$value['thumbnail'];
                            $data['image_title']=$value['title'];
                            $data['image_info']=$value['additional_info'];
                            break;
                        }
                    }
                }
                // get stage name of user
                if($userid>0){
                    $user_stagename = $this->db->select('stage_name')->get_where('users', array('id' => $this->session->userdata('user_id')))->row()->stage_name;
                }
                else{
                    $user_stagename='';
                }
                
                $data['user_stagename']=$user_stagename;
                $data['album_images']=$album_images;
                $data['projectid']=$projectId;
                $data['albumid']=$albumid;
                $data['imageid']=$imageid;
                
                /*
                // get listing of all comments of a album 
                if($imageid>0)
                    $album_comments=$this->project_model->list_albums_comments($imageid, '5');
                else
                    $album_comments='';
                
                $data['album_comments']=$album_comments;
                 * 
                 */
                
                // get listing of all comments of a album
                $newArray=  array();
                if($imageid>0){
                    $album_comments=$this->project_model->list_all_comments('images',$albumid, '5');
                    $newArray=  array();
                    foreach ($album_comments as $key => $value) {
                        //if(array_key_exists($value['content_id'], $newArray)){
                            $newArray[$value['content_id']][]=  array('id'=>$value['id'], 'user_id'=>$value['user_id'], 'added_date'=>$value['added_date'], 'comment'=>$value['comment'], 'stage_name'=>$value['stage_name']);
                        //}

                    }
                }
                    //$album_comments=$this->project_model->list_albums_comments($fileid, '6');
                
                $data['album_comments']=$newArray;
                
               // $data['largeImage']=$largeImg['fullimage'];
               // 
               // data for old template
                //$data['data']=  array('projectId'=>$projectId, 'albumsArr'=>$albums, 'largeImgArr'=>$largeImg, 'curr_albumid'=>$albumid);
                
                //data for new template
                $data['projectId']=$projectId;
                $data['albumsArr']=$albums;
                $data['largeImgArr']=$largeImg;
                $data['curr_albumid']=$albumid;
                $projectStatus=  $this->project_model->checkProjectStatus($projectId, $userid);
                $data['project_status']=$projectStatus;
                if($imageversion=='imageversion') $imageversion='yes';
                //else $imageversion='no';
                
                $data['imageversion']=$imageversion;
               
                // insertion of seen images in "files_seen" table
                if($imageid!='' && $userid>0){
                    $insert_seen=  $this->project_model->insertSeen($imageid, $userid, 'images');
                }
                
                //$this->load->view('template_projects', $data);
                $this->load->view('images_view', $data);
           // }
            /*
            else {
                redirect('/user/login/', 'refresh');
            }
             * 
             */
            
        }
        
        /**
         * Function for load images in a album
         *
         * @param string $action means what action you want to perform
         * 
         * @access public
         * 
        **/
        function album($action){
            ini_set( 'memory_limit', '2000M' );
            ini_set('upload_max_filesize', '2000M');  
            ini_set('post_max_size', '2000M');  
            ini_set('max_input_time', 36000);  
            ini_set('max_execution_time', 36000);
            
            $this->checkUserLogin();
            if($action=='imageupload'){
                //die('dsds');
                //load user controler file
                $this->load->library('../controllers/user');
                //call file upload function
                $upload_data=$this->user->do_upload('file');
                
                $filename='';
                if(isset($upload_data['upload_data'])){
                    $filename=$upload_data['upload_data']['file_name'];
                    $fileSize=$upload_data['upload_data']['file_size'];
                    $fileType=$upload_data['upload_data']['file_type'];
                    $full_path=$upload_data['upload_data']['full_path'];
                    $file_path=$upload_data['upload_data']['file_path'];
                    $parts = explode('.', $filename);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $file=$parts[0];
                    $fileExt=strtolower($parts[1]);
                   // $imagewidth=$upload_data['upload_data']['image_width'];
                   // $imageheight=$upload_data['upload_data']['image_height'];
                    // resize of image of project as thumbnail with 160x160 size
                    //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','160','160', $imagewidth, $imageheight);
                    $ext='png';
                    $this->user->image_crop_resize('160', '160', $filename, $file, $fileExt, 'little', $ext);
                    $this->user->image_crop_resize('80', '50', $filename, $file, $fileExt, 'thumbnail', $ext);
                    
                    $albumid=$this->input->post('album_id');
                    $imageId=$this->project_model->save_album_image($albumid, $filename, $fileSize, $ext);
                    $returnArray=  array('files');
                    $returnArray['files'][]=array('name'=>$filename, 'size'=>$fileSize, 'type'=>$fileType, 'url'=>base_url('uploads/'.$filename), 'thumbnailUrl'=>base_url('uploads/'.$file.'_80x50_thumb.'.$ext), 'deleteUrl'=>base_url('project/removealbumimage/'.$imageId.'/images/'.$filename), 'deleteType'=>'DELETE');
                    echo json_encode($returnArray);
                }
                
                
                die();
                //$insertId=$this->project_model->save_album($projectId, $filename);
               // redirect(current_url());
            }
            
            
            //vimeo video upload code
            else if($action=='videoupload'){
                error_reporting(0);
                //die('dsds');
                //load user controler file
                $this->load->library('../controllers/user');
                //include_once(constant('FILE_INCLUDE_PATH').'/includes/vimeo.php');
                //load vimeo library
               // $this->load->library('vimeo');
                //call file upload function
                $upload_data=$this->user->do_upload('file');
                //die('hhhj');
                
                $filename='';
                $filesize='';
                if(isset($upload_data['upload_data'])){
                    $filename=$upload_data['upload_data']['file_name'];
                    $filesize=$upload_data['upload_data']['file_size'];
                    $fileType=$upload_data['upload_data']['file_type'];
                    $full_path=$upload_data['upload_data']['full_path'];
                    $file_path=$upload_data['upload_data']['file_path'];
                    
                    $parts = explode('.', $filename);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    
                    $fileArr=pathinfo($filename);
                    
                    //$file=$parts[0];
                    $file=$fileArr['filename'];
                    //$fileExt=strtolower($parts[1]);
                    $fileExt=strtolower($fileArr['extension']);
                   // rename(constant('FILE_INCLUDE_PATH')."/uploads/".$filename, constant('FILE_INCLUDE_PATH')."/uploads/my_file.txt");
                   // $imagewidth=$upload_data['upload_data']['image_width'];
                   // $imageheight=$upload_data['upload_data']['image_height'];
                    // resize of image of project as thumbnail with 160x160 size
                    //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','160','160', $imagewidth, $imageheight);
                    //$this->user->image_crop_resize('160', '160', $filename, $file, $fileExt, 'little');
                }
                $albumid=$this->input->post('album_id');
                $videoid=$this->project_model->save_album_video($albumid, $filename, $filesize);
                
                
                ///*
                // UPLOAD TO VIMEO
                // based on snippet taken from :  https://raw.githubusercontent.com/vimeo/vimeo-php-lib/master/upload.php
                //die('---s');
                if (in_array($fileExt, array('mp4','flv','swf','m4v','ogg','webm', 'mov', 'mkv', 'avi'))) {

                        //$CONSUMER_KEY        = "f1033f450945f8833a463bc5161146b293682e23";
                        $CONSUMER_KEY        = "f1033f450945f8833a463bc5161146b293682e23";
                        //$CONSUMER_SECRET     = "a0b176bcf56944660724ca0e8079dd5e865a1f8c";
                        $CONSUMER_SECRET     = "a0b176bcf56944660724ca0e8079dd5e865a1f8c";
                        //$ACCESS_TOKEN        = "11a547c7cdffe21af22a9f3d73670680";
                        //$ACCESS_TOKEN        = "96f195c9cfddce0ae9fa11702d7bfb6f";
                        $ACCESS_TOKEN        = "ca3e110d442d77533e697ce16fc6295d";
                        //$ACCESS_TOKEN_SECRET = "f6a490f51ef464efce8497f1ff25bd7cb6383b06";
                        $ACCESS_TOKEN_SECRET = "2a0cdd354bc15657d34efb9ff460d54aeed235f5";
                        
                        //$vimeo = new phpVimeo($CONSUMER_KEY, $CONSUMER_SECRET, $ACCESS_TOKEN, $ACCESS_TOKEN_SECRET);
                        $vimeo = new Vimeo($CONSUMER_KEY, $CONSUMER_SECRET, $ACCESS_TOKEN);
                        $pathfullfile=constant('FILE_INCLUDE_PATH').'/uploads/'.$filename;
                        //die('sasas=='.$pathfullfile);
                        $vimeo_data = $vimeo->upload($pathfullfile);
                        
                        $vimeo_data_arr = explode('/', $vimeo_data);
                        $vimeo_id = end($vimeo_data_arr);
                        //echo '<pre>'; print_r($vimeo_id); die('-----s');
                        //$pdo = $GLOBALS['pdoDB'];

                        if ($vimeo_id > 0) {
                            //$vimeo->call('vimeo.videos.setPrivacy', array('privacy' => 'nobody', 'video_id' => $vimeo_id));
                            //$vimeo->call('vimeo.videos.setPrivacy', array('privacy' => 'disable', 'video_id' => $vimeo_id));
                            //$vimeo->call('vimeo.videos.setEmbed', array('allow_hd_embed' => 'checked', 'video_id' => $vimeo_id));
                            //$vimeo->call('vimeo.videos.setTitle', array('title' => $filename, 'video_id' => $vimeo_id));
                            //$vimeo->call('vimeo.videos.setDescription', array('description' => $name, 'video_id' => $video_id));
                            //$video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $vimeo_id));
                            //$vimeoimg = $video->video[0]->thumbnails->thumbnail[2]->_content;
                            //$imgPath = constant('FILE_INCLUDE_PATH').'/uploads/'.$vimeo_id.'.jpg';
                            //file_put_contents($imgPath, file_get_contents($vimeoimg));
                            $this->project_model->update_vimeo_id($videoid, $vimeo_id);
                        }
                        else {
                            echo("Upload to Vimeo was not successful!");
                        }
                    rename(constant('FILE_INCLUDE_PATH')."/uploads/".$filename, constant('FILE_INCLUDE_PATH')."/uploads/video_".$videoid.".".$fileExt);
                }
                
                $returnArray=  array('files');
                $returnArray['files'][]=array('name'=>$filename, 'size'=>$filesize, 'type'=>$fileType, 'url'=>base_url('uploads/video_'.$videoid.'.'.$fileExt), 'deleteUrl'=>base_url('project/removealbumimage/'.$videoid.'/videos/'.$filename), 'deleteType'=>'DELETE');
                
                echo json_encode($returnArray);
                
                die();
                 
                //$insertId=$this->project_model->save_album($projectId, $filename);
               // redirect(current_url());
            }
            else if($action=='fileupload'){
                //load user controler file
                $this->load->library('../controllers/user');
                //call file upload function
                $fileArr=pathinfo($_FILES['file']['name']);
                $ext=$fileArr['extension'];
                $upload_data=$this->user->do_upload('file', $ext);
                //echo '<pre>'; print_r($upload_data); die;
                $filename='';
                $filesize='';
                $filenameEncrypt='';
                $crocdocThumb='';
                $crocdocId='';
                
                if(isset($upload_data['upload_data'])){
                    //die('file_up');
                    $filename=$upload_data['upload_data']['file_name'];
                    $filesize=$upload_data['upload_data']['file_size'];
                    $fileType=$upload_data['upload_data']['file_type'];
                    $full_path=$upload_data['upload_data']['full_path'];
                    $file_path=$upload_data['upload_data']['file_path'];
                    $parts = explode('.', $filename);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $file=$parts[0];
                    $fileExt=strtolower($parts[1]);
                    $fileNameOnly = md5($file.date('Y-m-d H:i:s'));
                    $filenameEncrypt=$fileNameOnly.'.'.$fileExt;
                    
                    rename(constant('FILE_INCLUDE_PATH')."/uploads/".$filename, constant('FILE_INCLUDE_PATH')."/uploads/".$filenameEncrypt);
                    
                    // Upoad to Crocodoc API
                    
                    $file_ext_array=array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt');
                    $img_arr = array('jpg', 'jpeg', 'gif', 'png');
                    //if(in_array($fileExt, $file_ext_array)){
//                        $md = new DocumentManager;
//                        $crocdocId = $md->uploadFile(constant('FILE_INCLUDE_PATH').'/uploads/'.$filenameEncrypt);
                        //$crocdocId = $this->boxUploadFile(constant('FILE_INCLUDE_PATH').'/uploads/'.$filenameEncrypt);
                        //flush(); //this sends the output to the client. You may also need ob_flush();
                        //sleep(10); //wait 10 seconds
//                        for($i=0; $i<=2; $i++){
//                            $crocdocThumb= $this->getFileThumbCrocdoc($crocdocId, 'png', $fileExt);
//                        }
                    //}
                    if($fileExt == 'pdf'){
                        //echo constant('FILE_INCLUDE_PATH').'/uploads/'.$filenameEncrypt; die;
                        $im = new imagick(constant('FILE_INCLUDE_PATH').'/uploads/'.$filenameEncrypt.'[0]');
                        $im->setImageFormat('jpg');
                        //file_put_contents ("test_2.jpg", $im); 
                        $crocdocThumb = $this->session->userdata('user_id'). rand().time().'.jpg';
                        file_put_contents(constant('FILE_INCLUDE_PATH').'/uploads/'.$crocdocThumb ,$im); 
                        //header('Content-Type: image/jpeg');
                        //echo $im; die;
                        //sleep(10);
                    }    
                    
                    if(in_array(strtolower($fileExt), $img_arr)){
                        $parts = explode('.', $filenameEncrypt);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        //echo '$filenameEncrypt='.$filenameEncrypt; die;
                        $this->user->image_crop_resize('300', '300', $filenameEncrypt, $file, $fileExt);
                        $crocdocThumb = $file.'_thumb.jpg';
                    }
                    
                }
                //die('file_not_up');
                    $albumid=$this->input->post('album_id');
                    $fileid=$this->project_model->save_album_files($albumid, $filenameEncrypt, $filename, $filesize, $crocdocId, $crocdocThumb);
                
                    $returnArray=  array('files');
                    $returnArray['files'][]=array('name'=>$filename, 'size'=>$filesize, 'type'=>@$fileType, 'url'=>base_url('uploads/'.$filenameEncrypt), 'deleteUrl'=>base_url('project/removealbumimage/'.$fileid.'/files/'.$filename), 'deleteType'=>'DELETE', 'fileid'=>$fileid);
                
                    echo json_encode($returnArray);
                
                die();
            }
            
            else if($action=='webpageupload'){
                //die('dsds');
                //load user controler file
                $this->load->library('../controllers/user');
                //call file upload function
                $upload_data=$this->user->do_upload('file', 'zip');
                
                $filename='';
                if(isset($upload_data['upload_data'])){
                    $filename=$upload_data['upload_data']['file_name'];
                    $fileSize=$upload_data['upload_data']['file_size'];
                    $fileType=$upload_data['upload_data']['file_type'];
                    $full_path=$upload_data['upload_data']['full_path'];
                    $file_path=$upload_data['upload_data']['file_path'];
                    $parts = explode('.', $filename);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $file=$parts[0];
                    $fileExt=strtolower($parts[1]);
                   // $imagewidth=$upload_data['upload_data']['image_width'];
                   // $imageheight=$upload_data['upload_data']['image_height'];
                    // resize of image of project as thumbnail with 160x160 size
                    //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','160','160', $imagewidth, $imageheight);
                   // $this->user->image_crop_resize('160', '160', $filename, $file, $fileExt, 'little');
                   // $this->user->image_crop_resize('80', '50', $filename, $file, $fileExt, 'thumbnail');
                    
                    $albumid=$this->input->post('album_id');
                    $imageId=$this->project_model->save_webpages($albumid, $filename, $fileSize);
                    //rename(constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$filename, constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$file.'_'.$imageId.'.'.$fileExt);
                    //$this->extract($file.'_'.$imageId.'.'.$fileExt, $imageId);
                    $this->extract($filename, $imageId);
                    $returnArray=  array('files');
                    $returnArray['files'][]=array('name'=>$filename, 'size'=>$fileSize, 'type'=>$fileType, 'url'=>base_url('uploads/webpages/'.$filename), 'deleteUrl'=>base_url('project/removealbumimage/'.$imageId.'/webpages/'.$filename), 'deleteType'=>'DELETE');
                    echo json_encode($returnArray);
                }
                
                
                die();
                //$insertId=$this->project_model->save_album($projectId, $filename);
               // redirect(current_url());
            }
        }
        
        /**
         * Function for extract zip folder of webpages
         *
         * @param string $foldername name of zip folder
         * 
         * @access public
         * 
         * @return 
        **/
        public function extract($foldername, $id) {
            
            // Optional: Only take out these files, anything else is ignored
            $this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'swf', 'htm'));

            // Give it one parameter and it will extract to the same folder
            $this->unzip->extract('uploads/webpages/'.$foldername);
            $parts = explode('.', $foldername);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            $file=$parts[0];
            $fileExt=strtolower($parts[1]);
            rename(constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$foldername, constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$file.'_'.$id.'.'.$fileExt);
            $foldername=$this->db->select('foldername')->get_where('webpages', array('id' => $id))->row()->foldername;
            rename(constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$foldername, constant('FILE_INCLUDE_PATH')."/uploads/webpages/".$foldername.'_'.$id);
            

            // or specify a destination directory
            //$this->unzip->extract('uploads/my_archive.zip', '/path/to/directory/');
        }
        
        /**
         * Function for getthumbnail of crocdocfile
         *
         * @param string $crocdocId crocdoc file id
         * 
         * @access public
         * 
         * @return $crocdocThumb thumbnail of crocdoc file
        **/
        function getFileThumbCrocdoc($crocdocId, $ext, $file){
            $refreshToken = $this->project_model->getRefreshToken();
            $accessToken = $this->getAccessTokenByRefresh($refreshToken);
            
            $imageVideoExtArr = array('jpg', 'jpeg', 'png', 'gif', 'mp4', 'mov', 'avi', 'flv', 'wmv', '3gp', 'ogg', 'webm', 'wav', 'mpg', 'mpeg', 'm2v', 
                                        'vob', 'mkv', 'tiff', 'bmp');
            if(in_array($file, $imageVideoExtArr)){
                
                $url = "https://api.box.com/2.0/files/$crocdocId/thumbnail.png?min_height=256&min_width=256";
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$accessToken
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $data = curl_exec($ch);
                curl_close($ch);
            }
            else{
                $url = 'https://api.box.com/2.0/files/'.$crocdocId.'?fields=representations';
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$accessToken,
                    'x-rep-hints: [png?dimensions=1024x1024]'
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $response = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($response);
                $url_template = $response->representations->entries[0]->content->url_template;
                $url_template = explode('/', $url_template);
                array_pop($url_template);
                $url_template = implode('/', $url_template); 


                $url = $url_template.'/1.png';            

                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$accessToken,
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $data = curl_exec($ch);
                curl_close($ch);

            }
            $image = imagecreatefromstring($data);
            ob_start();
            imagepng($image);
            $data = ob_get_clean();


            file_put_contents(constant('FILE_INCLUDE_PATH').'/uploads/'.$crocdocId.'.png',$data);
            $this->user->image_crop_resize('300', '300', $crocdocId.'.png', $crocdocId, 'png', 'little', $ext);
            unlink(constant('FILE_INCLUDE_PATH').'/uploads/'.$crocdocId.'.png');
            rename(constant('FILE_INCLUDE_PATH')."/uploads/".$crocdocId.'_thumb.'.$ext, constant('FILE_INCLUDE_PATH')."/uploads/".$crocdocId.".".$ext);
            return $crocdocThumb=$crocdocId.'.'.$ext;
            
        }
        
        /**
         * Function for remove all details of project
         *
         * @param string $projectId primary id of a project from "projects" table
         * 
         * @access public
         * 
         * @return redirect to dashboard url 
        **/
        public function remove($projectId, $redirect='yes') {
            $this->checkUserLogin();
            $this->project_model->remove_project_data($projectId);
            if($redirect=='yes'){
                redirect('/');
            }
        }
        
        /**
         * Function for undo all details of project
         *
         * @param string $projectId primary id of a project from "projects" table
         * 
         * @access public
         * 
         * @return redirect to dashboard url 
        **/
        public function undo($projectId) {
            $this->checkUserLogin();
            $this->project_model->remove_project_data($projectId, 'undo');
                //redirect('/');
        }
        
        /**
         * Function for update project details
         * 
         * @access public
         * 
         * @return redirect to project view 
        **/
        public function update() {
            $this->checkUserLogin();
            $this->load->model('user_model');
            $this->load->library('../controllers/user');
            $fileupload='no';
            $filename=$this->input->post('thumbnail');
            if($this->input->post('project_id')!=''){
                if($_FILES['upload_image']['name']!=''){
                    $fileupload='yes';
                    $upload_data=$this->user->do_upload('upload_image');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                       // $imagewidth=$upload_data['upload_data']['image_width'];
                       // $imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 160X160 size

                        //old image resize code

                        //$this->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','160','160');
                        $this->user->image_crop_resize('300', '300', $filename, $file, $fileExt, 'little', 'png');
                    }
                }
            }
            //call add_project function of user model to add project information in "projects" table
            $this->user_model->update_project($filename, $fileupload, 'png');
           // unlink('./uploads/' .$filename);
            //$this->session->set_flashdata('add_project_msg', 'Your project is successfully added');
            redirect('/project/view/'.$this->input->post('project_id'), 'refresh');
        }
        
        
        /**
         * Function for remove album from "user_albums" table or "album_images" table
         * 
         * @access public
         * 
         * 
        **/
        public function removealbum() {
            $albumid = $this->input->post('albumid');
            $this->project_model->remove_album($albumid);
        }
        
        /**
         * Function for undo album from "user_albums" table or "album_images" table
         * 
         * @access public
         * 
         * 
        **/
        public function undoalbum() {
            $albumid = $this->input->post('albumid');
            $this->project_model->remove_album($albumid, 'undo');
        }
        
        /**
         * Function for remove album comments from "comments" table or "album_images" table
         * 
         * @access public
         * 
         * 
        **/
        public function removecomment() {
            $commentid = $this->input->post('commentid');
            $this->project_model->remove_comment($commentid);
        }
        
        /**
         * Function for remove album images from "album_images" table
         * 
         * @access public
         * 
         * 
        **/
        public function removealbumimage($imageid='', $type='', $filename='') {
            $imageid = ($imageid!='') ? $imageid : $this->input->post('imageid');
            $type= ($type!='') ? $type : $this->input->post('type');
            $this->project_model->remove_album_image($type, $imageid);
            if($filename!=''){
                $array=  array($filename=>true);
                echo json_encode($array);
                die();
            }
        }
        
        /**
         * Function for undo album images from "album_images" table
         * 
         * @access public
         * 
         * 
        **/
        public function undoalbumimage($imageid='', $type='', $filename='') {
            $imageid = ($imageid!='') ? $imageid : $this->input->post('imageid');
            $type= ($type!='') ? $type : $this->input->post('type');
            $this->project_model->remove_album_image($type, $imageid, 'undo');
            if($filename!=''){
                $array=  array($filename=>true);
                echo json_encode($array);
                die();
            }
        }
        
        /**
         * Function for sorting albums
         * 
         * @access public
         * 
         * @return true if sorting successfully done 
        **/
        public function sortalbums() {
            $sort=$this->project_model->sort_albums();
            return true;
        }
        
        /**
         * Function for sorting albums images
         * 
         * @access public
         * 
         * @return true if sorting successfully done 
        **/
        public function sortalbumsimages() {
            $sort=$this->project_model->sort_albums_images();
            return true;
        }
        
        
        /**
         * Function for load videos view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * @param string $albumid primary id of a video albums from "videos_folder" table
         * @param string $videoid primary id of a video from "user_videos" table
         * @param string $sortColumn column name that you want to sort
         * @param string $sortby sorting value like asc, desc
         * 
         * @access public
         * 
         * @return video view html from videos_view.php 
        **/
        public function videos($projectId, $albumid='', $videoid='', $sortColumn='', $sortby='') {
//            $sortby = ($sortby == '') ? 'DESC' : $sortby;
//            $sortColumn = ($sortColumn == '') ? 'date' : $sortColumn;
            //echo '$sortby='.$sortby.', $sortColumn='.$sortColumn; die;
            $view=$this->checkViewOnlyMode($projectId);
            if($view=='false'){
                $this->checkUserLogin();
            }
            $userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
           // if(($this->session->userdata('user_id')!="")){
                if($this->input->post('folder_name')){
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    $insertId=$this->project_model->save_album($projectId, $filename, 'videos', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit video project
                else if($this->input->post('album_hidden')){
                    
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    
                    $insertId=$this->project_model->edit_album($filename, 'videos', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit video of a project
                else if($this->input->post('project_videoid_hidden')){
                    //die('ssss');
                    $videoid=$this->input->post('project_videoid_hidden');
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    $vimeoVideoThumb='no';
                    $ext='png';
                    if(isset($upload_data['upload_data'])){
                        $filename=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('300', '300', $filename, $file, $fileExt, 'little', $ext);
                    }
                    
                    if($this->input->post('vimeothumb')=='vimeo'){
                        $vimeo_id = $this->db->select('vimeo_id')->get_where('user_videos', array('id' => $videoid))->row()->vimeo_id;                        
                        //$CONSUMER_KEY        = "f1033f450945f8833a463bc5161146b293682e23";
                        $CONSUMER_KEY        = "f1033f450945f8833a463bc5161146b293682e23";
                        //$CONSUMER_SECRET     = "a0b176bcf56944660724ca0e8079dd5e865a1f8c";
                        $CONSUMER_SECRET     = "a0b176bcf56944660724ca0e8079dd5e865a1f8c";
                        //$ACCESS_TOKEN        = "11a547c7cdffe21af22a9f3d73670680";
                        //$ACCESS_TOKEN        = "96f195c9cfddce0ae9fa11702d7bfb6f";
                        $ACCESS_TOKEN        = "ca3e110d442d77533e697ce16fc6295d";
                        //$ACCESS_TOKEN_SECRET = "f6a490f51ef464efce8497f1ff25bd7cb6383b06";
                        $ACCESS_TOKEN_SECRET = "2a0cdd354bc15657d34efb9ff460d54aeed235f5";

                        //$vimeo = new phpVimeo($CONSUMER_KEY, $CONSUMER_SECRET, $ACCESS_TOKEN, $ACCESS_TOKEN_SECRET);
                        // $vimeo = new Vimeo($CONSUMER_KEY, $CONSUMER_SECRET, $ACCESS_TOKEN);
                        // $video = $vimeo->call('vimeo.videos.getInfo', array('video_id' => $vimeo_id));
                        // echo '<pre>'; print_r($video); die('----s')
//                        $vimeoimg = $video->video[0]->thumbnails->thumbnail[2]->_content;
                        $imgPath = constant('FILE_INCLUDE_PATH').'/uploads/'.$vimeo_id.'.'.$ext;
                        
                        $vimeoimg = $this->getVimeoThumb($vimeo_id);
                        
                        file_put_contents($imgPath, file_get_contents($vimeoimg));
                        $this->user->image_crop_resize('300', '300', $vimeo_id.'.'.$ext, $vimeo_id, 'jpg', 'little', $ext);
                        $vimeoVideoThumb=$vimeo_id.'_thumb.'.$ext;
                    }
                    
                    $insertId=$this->project_model->edit_video_info($videoid, $filename, $vimeoVideoThumb, $ext, $vimeoimg);
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //for add comments in album
                else if($this->input->post('album_comment_hidden')){
                    //$update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '2');
                    $update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '4');
                    redirect(current_url());
                }
                
                //for edit comments of album
                else if($this->input->post('commentid_hidden')){
                    $update=$this->project_model->edit_album_comment($this->input->post('commentid_hidden'));
                    redirect(current_url());
                }
                
                $data['projectid']=$projectId;
                $data['projectId']=$projectId;
                $albums = $this->project_model->list_user_albums($userid, $projectId, 'videos_folder');
                if($albumid==''){
                    if(!empty($albums)){
                        $newarr = $albums[0];
                        $albumid=$newarr['id'];
                    }
                }
                $data['title']= $this->project_model->project_title($projectId);
                $data['top_menu']=  $this->topMenu($projectId);
                $data['selected_menu']='videos';
                $data['albumsArr']=$albums;
                //$data['largeImgArr']=$largeImg;
                $data['curr_albumid']=$albumid;
                // get stage name of user
                if($userid>0){
                    $user_stagename = $this->db->select('stage_name')->get_where('users', array('id' => $userid))->row()->stage_name;
                }
                else{
                    $user_stagename='';
                }
                $data['user_stagename']=$user_stagename;
                
                // get listing of all videos of a album
                $project_videos=$this->project_model->list_project_videos($albumid, $sortColumn, $sortby);
                
                $data['project_videos']=$project_videos;
                
                
                $largeImg=  array();
                $data['video_thumb']='';
                $data['vimeo_thumbnail']='';
                $data['video_title']='';
                $data['video_info']='';
                $data['vimeo_id']='';
                
                //$imageid='';
                if($videoid==''){
                    if(!empty($project_videos)){                        
                        $newarr = $project_videos[0];
                        $data['video_thumb']=$newarr['thumbnail'];
                        $data['vimeo_thumbnail']=$newarr['vimeo_thumbnail'];
                        $data['video_title']=$newarr['title'];
                        $data['video_info']=$newarr['additional_info'];
                        $data['vimeo_id']=$newarr['vimeo_id'];
                        $data['video_name']=$newarr['video'];
                        $videoid=$newarr['id'];
                    }
                }
                else{
                    //die('dsd');
                    foreach ($project_videos as $key => $value) {
                        if($value['id']==$videoid){
                            $data['video_thumb']=$value['thumbnail'];
                            $data['vimeo_thumbnail']=$value['vimeo_thumbnail'];
                            $data['video_title']=$value['title'];
                            $data['video_info']=$value['additional_info'];
                            $data['vimeo_id']=$value['vimeo_id'];
                            $data['video_name']=$value['video'];
                            break;
                        }
                    }
                }
                $data['videoid']=$videoid;
                $data['sortColumn']=$sortColumn;
                $data['sortby']=$sortby;
                
                $projectStatus=  $this->project_model->checkProjectStatus($projectId, $userid);
                $data['project_status']=$projectStatus;
                // get listing of all comments of a album 
                //$album_comments=$this->project_model->list_albums_comments($videoid, '4');
                //$album_comments=$this->project_model->list_albums_comments($videoid, '4');
                $album_comments=$this->project_model->list_all_comments('videos', $albumid, '4');
                $newArray=  array();
                foreach ($album_comments as $key => $value) {
                    //if(array_key_exists($value['content_id'], $newArray)){
                        $newArray[$value['content_id']][]=  array('id'=>$value['id'], 'user_id'=>$value['user_id'], 'added_date'=>$value['added_date'], 'comment'=>$value['comment'], 'stage_name'=>$value['stage_name']);
                    //}
                    
                }
//                echo '<pre>';
//                print_r($newArray);
//                die();
                
                $data['album_comments']=$newArray;
                
                //call video module view
                
                // insertion of seen videos in "files_seen" table
                if($videoid!=''){
                    $insert_seen=  $this->project_model->insertSeen($videoid, $userid, 'videos');
                }
                
                $this->load->view('videos_view', $data);
                //die('under construction');
           // }
           // else {
           //     redirect('/user/login/', 'refresh');
           // }
        }
        
        
        function getVimeoThumb($id)
        {
            // $url = 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/'.$id;
            // $json = json_decode(file_get_contents($url . $id));
            // $thumb_image = $json->thumbnail_url;
            // die('---s'.$thumb_image);
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://vimeo.com/api/oembed.json?url=https://vimeo.com/".$id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Referer: REFERER_URL"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $responseArray = json_decode($response);
            return $thumb_image = $responseArray->thumbnail_url;
            // die('---s'.$thumb_image);

            // if ($err) {
            // echo "cURL Error #:" . $err;
            // } else {
            // echo $response;
            // }
            // die('---s');
           // die('----'.$response);
            //$vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
            //echo '<pre>'; print_r($vimeo); die('---ss');
            //return $small = $vimeo[0]['thumbnail_large'];
    //        echo $medium = $vimeo[0]['thumbnail_medium'];
    //        echo $large = $vimeo[0]['thumbnail_large'];
        }
        
        
        /**
         * Function for load webpages view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * @param string $albumid primary id of a video albums from "videos_folder" table
         * @param string $videoid primary id of a video from "user_videos" table
         * @param string $sortColumn column name that you want to sort
         * @param string $sortby sorting value like asc, desc
         * 
         * @access public
         * 
         * @return webpage view html from webpages_view.php 
        **/
        public function webpages($projectId, $albumid='', $videoid='', $sortColumn='', $sortby='') {
            if(!$projectId){
                redirect('user');
            }
            $view=$this->checkViewOnlyMode($projectId);
            if($view=='false'){
                $this->checkUserLogin();
            }
            $userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
           // if(($this->session->userdata('user_id')!="")){
                if($this->input->post('folder_name')){
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    $insertId=$this->project_model->save_album($projectId, $filename, 'webpages', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit video project
                else if($this->input->post('album_hidden')){
                    
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    
                    $insertId=$this->project_model->edit_album($filename, 'webpages', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit video of a project
                else if($this->input->post('project_videoid_hidden')){
                    //die('ssss');
                    $videoid=$this->input->post('project_videoid_hidden');
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    //$vimeoVideoThumb='no';
                    if(isset($upload_data['upload_data'])){
                        $filename=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('300', '300', $filename, $file, $fileExt, 'little', 'png');
                    }
                    
                    
                    
                    $insertId=$this->project_model->edit_webpage_info($videoid, $filename, 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //for add comments in album
                else if($this->input->post('album_comment_hidden')){
                    //$update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '2');
                    $update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '7');
                    redirect(current_url());
                }
                
                //for edit comments of album
                else if($this->input->post('commentid_hidden')){
                    $update=$this->project_model->edit_album_comment($this->input->post('commentid_hidden'));
                    redirect(current_url());
                }
                
                $data['projectid']=$projectId;
                $data['projectId']=$projectId;
                $albums = $this->project_model->list_user_albums($userid, $projectId, 'webpages_albums');
                if($albumid==''){
                    if(!empty($albums)){
                        $newarr = $albums[0];
                        $albumid=$newarr['id'];
                    }
                }
                $data['title']= $this->project_model->project_title($projectId);
                $data['top_menu']=  $this->topMenu($projectId);
                $data['selected_menu']='webpages';
                $data['albumsArr']=$albums;
                //$data['largeImgArr']=$largeImg;
                $data['curr_albumid']=$albumid;
                // get stage name of user
                if($userid>0){
                    $user_stagename = $this->db->select('stage_name')->get_where('users', array('id' => $userid))->row()->stage_name;
                }
                else{
                    $user_stagename='';
                }
                $data['user_stagename']=$user_stagename;
                
                // get listing of all images of a album
                $project_videos=$this->project_model->list_webpages($albumid, $sortColumn, $sortby);
                
                $data['project_videos']=$project_videos;
                
                
                $largeImg=  array();
                $data['video_thumb']='';
                $data['vimeo_thumbnail']='';
                $data['video_title']='';
                $data['video_info']='';
                $data['vimeo_id']='';
                $data['iframe_height']='';
                $data['iframe_width']='';
                
                //$imageid='';
                if($videoid==''){
                    if(!empty($project_videos)){                        
                        $newarr = $project_videos[0];
                        $data['video_thumb']=$newarr['thumbnail'];
                        $data['vimeo_thumbnail']=$newarr['vimeo_thumbnail'];
                        $data['video_title']=$newarr['title'];
                        $data['video_info']=$newarr['additional_info'];
                        //$data['vimeo_id']=$newarr['vimeo_id'];
                        $data['video_name']=$newarr['foldername'];
                        $data['iframe_height']=$newarr['iframe_height'];
                        $data['iframe_width']=$newarr['iframe_width'];
                        $videoid=$newarr['id'];
                    }
                }
                else{
                    //die('dsd');
                    foreach ($project_videos as $key => $value) {
                        if($value['id']==$videoid){
                            $data['video_thumb']=$value['thumbnail'];
                            $data['vimeo_thumbnail']=$value['vimeo_thumbnail'];
                            $data['video_title']=$value['title'];
                            $data['video_info']=$value['additional_info'];
                            //$data['vimeo_id']=$value['vimeo_id'];
                            $data['video_name']=$value['foldername'];
                            $data['iframe_height']=$value['iframe_height'];
                            $data['iframe_width']=$value['iframe_width'];
                            break;
                        }
                    }
                }
                $data['videoid']=$videoid;
                $data['sortColumn']=$sortColumn;
                $data['sortby']=$sortby;
                
                $projectStatus=  $this->project_model->checkProjectStatus($projectId, $userid);
                $data['project_status']=$projectStatus;
                // get listing of all comments of a album 
                //$album_comments=$this->project_model->list_albums_comments($videoid, '4');
                //$album_comments=$this->project_model->list_albums_comments($videoid, '4');
                $album_comments=$this->project_model->list_all_comments('webpages', $albumid, '7');
                $newArray=  array();
                foreach ($album_comments as $key => $value) {
                    //if(array_key_exists($value['content_id'], $newArray)){
                        $newArray[$value['content_id']][]=  array('id'=>$value['id'], 'user_id'=>$value['user_id'], 'added_date'=>$value['added_date'], 'comment'=>$value['comment'], 'stage_name'=>$value['stage_name']);
                    //}
                    
                }
//                echo '<pre>';
//                print_r($newArray);
//                die();
                
                $data['album_comments']=$newArray;
                
                //call video module view
                
                
                $this->load->view('webpages_view', $data);
                //die('under construction');
           // }
           // else {
           //     redirect('/user/login/', 'refresh');
           // }
        }
        
        /**
         * Function for download videos files
         *
         * @param string $type type of download file like video or files
         * @param string $fileid primary id of a videos or files from "user_videos" or "user_files" table
         * 
         * @access public
         * 
         * @return project view html from project_view.php 
        **/
        public function download($type, $fileid) {
            $this->checkUserLogin();
            if($type=='video')
                $filename = $this->db->select('video')->get_where('user_videos', array('id' => $fileid))->row()->video;
            
            else if($type=='file')
                $filename = $this->db->select('file')->get_where('user_files', array('id' => $fileid))->row()->file;
                
            else if($type=='image')
                $filename = $this->db->select('full_image')->get_where('album_images', array('id' => $fileid))->row()->full_image;
            
            else if($type=='webpage')
                $filename = $this->db->select('foldername')->get_where('webpages', array('id' => $fileid))->row()->foldername;
                
            $parts = explode('.', $filename);
            $last = array_pop($parts);
            $parts = array(implode('.', $parts), $last);
            $file=$parts[0];
            $fileExt=strtolower($parts[1]);
            
            if($type=='video'){
                $downloadfile=constant('FILE_INCLUDE_PATH').'/uploads/video_'.$fileid.'.'.$fileExt;
            }
            
            else if($type=='file'){
                $downloadfile=constant('FILE_INCLUDE_PATH').'/uploads/'.$filename;
            }
            
            else if($type=='image'){
                $downloadfile=constant('FILE_INCLUDE_PATH').'/uploads/'.$filename;
            }
            
            else if($type=='webpage'){
                $downloadfile=constant('FILE_INCLUDE_PATH').'/uploads/webpages/'.$filename.'_'.$fileid.'.zip';
                $filename=explode('_', $filename);
                $filename=$filename[0].'.zip';
            }
            
            if(file_exists($downloadfile)):
                $data=file_get_contents($downloadfile);
                force_download($filename, $data);
            endif;
            
            //$mimetype= mime_content_type($downloadfile);
            //die('mimetype='.$mimetype);
            // We'll be outputting a PDF
            //header('Content-type: '.$mimetype);

            // It will be called downloaded.pdf
            //header('Content-Disposition: attachment; filename="'.$filename.'"');

            // The PDF source is in original.pdf
            //readfile($downloadfile);
        }
        
        /**
         * Function for load files view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * @param string $albumid primary id of a video albums from "files_folder" table
         * @param string $fileid primary id of a video from "user_files" table
         * @param string $sortColumn column name that you want to sort
         * @param string $sortby sorting value like asc, desc
         * 
         * @access public
         * 
         * @return project view html from project_view.php 
        **/
        public function files($projectId, $albumid='', $fileid='', $sortColumn='', $sortby='') {
//            $sortby = ($sortby == '') ? 'DESC' : $sortby;
//            $sortColumn = ($sortColumn == '') ? 'date' : $sortColumn;
            $view=$this->checkViewOnlyMode($projectId);
            if($view=='false'){
                $this->checkUserLogin();
            }
            $userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
            //if(($this->session->userdata('user_id')!="")){
                if($this->input->post('folder_name')){
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('250', '250', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    $insertId=$this->project_model->save_album($projectId, $filename, 'files', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit file project
                else if($this->input->post('album_hidden')){
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    // check if user upload any image as thumbnail in project
                    if(isset($upload_data['upload_data'])){
                        $filename_org=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename_org);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        $filename=time().rand();
                        //die('---kjk');
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('300', '300', $filename_org, $filename, $fileExt, 'little', 'png');
                    }
                    
                    $insertId=$this->project_model->edit_album($filename, 'files', 'png');
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //edit video of a project
                else if($this->input->post('project_fileid_hidden')){
                    //die('ssss');
                    $fileid=$this->input->post('project_fileid_hidden');
                    //load user controler file
                    $this->load->library('../controllers/user');
                    //call file upload function
                    $upload_data=$this->user->do_upload('edit_thumbnail');
                    $filename='';
                    $ext='png';
                    // check if user upload any image as thumbnail in project
                    $crocdocVideoThumb='no';
                    if(isset($upload_data['upload_data']) && $this->input->post('crocdocthumb')==''){
                        $filename=$upload_data['upload_data']['file_name'];
                        $parts = explode('.', $filename);
                        $last = array_pop($parts);
                        $parts = array(implode('.', $parts), $last);
                        $file=$parts[0];
                        $fileExt=strtolower($parts[1]);
                        //$imagewidth=$upload_data['upload_data']['image_width'];
                        //$imageheight=$upload_data['upload_data']['image_height'];
                        // resize of image of project as thumbnail with 98x98 size
                        //$this->user->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','98','98', $imagewidth, $imageheight);
                        //die('dd');
                        $this->user->image_crop_resize('300', '300', $filename, $file, $fileExt, 'little', $ext);
                    }
                    
                    //crocdoc image thumbnail
                    
                    if($this->input->post('crocdocthumb')=='crocdoc'){
                        $crocdoc_id = $this->db->select('crocdoc_id')->get_where('user_files', array('id' => $fileid))->row()->crocdoc_id;
                        
                        $file = $this->db->select('file')->get_where('user_files', array('id' => $fileid))->row()->file;
                        $fileArr = pathinfo(strtolower($file));
                        //$crocdocVideoThumb= $this->getFileThumbCrocdoc($crocdoc_id, 'png', $fileArr['extension']);
                        for($i=0; $i<=2; $i++){
                            $crocdocVideoThumb= $this->getFileThumbCrocdoc($crocdoc_id, 'png', $fileArr['extension']);
                        }
                    }
                     
                    
                    $insertId=$this->project_model->edit_file_info($fileid, $filename, $crocdocVideoThumb, $ext);
                   // unlink('./uploads/' .$filename);
                    redirect(current_url());
                }
                
                //for add comments in album
                else if($this->input->post('album_comment_hidden')){
                    $update=$this->project_model->add_album_comment($this->input->post('album_comment_hidden'), '6');
                    redirect(current_url());
                }
                
                //for edit comments of album
                else if($this->input->post('commentid_hidden')){
                    $update=$this->project_model->edit_album_comment($this->input->post('commentid_hidden'));
                    redirect(current_url());
                }
                
                $data['projectid']=$projectId;
                $data['projectId']=$projectId;
                $albums = $this->project_model->list_user_albums($userid, $projectId, 'files_folder');
                if($albumid==''){
                    if(!empty($albums)){
                        $newarr = $albums[0];
                        $albumid=$newarr['id'];
                    }
                }
                $data['title']= $this->project_model->project_title($projectId);
                $data['top_menu']=  $this->topMenu($projectId);
                $data['selected_menu']='files';
                $data['albumsArr']=$albums;
                //$data['largeImgArr']=$largeImg;
                $data['curr_albumid']=$albumid;
                // get stage name of user
                if($userid>0){
                    $user_stagename = $this->db->select('stage_name')->get_where('users', array('id' => $userid))->row()->stage_name;
                }
                else{
                    $user_stagename='';
                }
                $data['user_stagename']=$user_stagename;
                
                
                // get listing of all files of a album
                $project_files=$this->project_model->list_project_files($albumid, $sortColumn, $sortby);
                //echo '<pre>'; print_r($project_files); die;
                $data['project_files']=$project_files;
                
                $largeImg=  array();
                $data['file_thumb']='';
                $data['file_title']='';
                $data['file_info']='';
                $data['file_crocdoc_id']='';
                
                //$imageid='';
                if($fileid==''){
                    if(!empty($project_files)){                        
                        $newarr = $project_files[0];
                        $data['file_thumb']=$newarr['thumbnail'];
                        $data['file_title']=$newarr['title'];
                        $data['file_info']=$newarr['additional_info'];
                        $data['file_crocdoc_id']=$newarr['crocdoc_id'];
                        $data['file_name']=$newarr['file'];
                        $fileid=$newarr['id'];
                    }
                }
                else{
                    //die('dsd');
                    foreach ($project_files as $key => $value) {
                        if($value['id']==$fileid){
                            $data['file_thumb']=$value['thumbnail'];
                            $data['file_title']=$value['title'];
                            $data['file_info']=$value['additional_info'];
                            $data['file_crocdoc_id']=$value['crocdoc_id'];
                            $data['file_name']=$value['file'];
                            break;
                        }
                    }
                }
                $data['fileid']=$fileid;
                
                // get listing of all comments of a album
                $newArray=  array();
                if($fileid>0){
                    $album_comments=$this->project_model->list_all_comments('files',$albumid, '6');
                    $newArray=  array();
                    foreach ($album_comments as $key => $value) {
                        //if(array_key_exists($value['content_id'], $newArray)){
                            $newArray[$value['content_id']][]=  array('id'=>$value['id'], 'user_id'=>$value['user_id'], 'added_date'=>$value['added_date'], 'comment'=>$value['comment'], 'stage_name'=>$value['stage_name']);
                        //}

                    }
                }
                    //$album_comments=$this->project_model->list_albums_comments($fileid, '6');
                
                $data['album_comments']=$newArray;
                
                $data['sortColumn']=$sortColumn;
                $data['sortby']=$sortby;
                $projectStatus=  $this->project_model->checkProjectStatus($projectId, $userid);
                $data['project_status']=$projectStatus;
                //call video module view
                
                // insertion of seen files in "files_seen" table
                if($fileid!='' && $userid>0){
                    $insert_seen=  $this->project_model->insertSeen($fileid, $this->session->userdata('user_id'), 'files');
                }
                $this->load->view('files_view', $data);
            //}
            /*
            else{
                redirect('/user/login/', 'refresh');
            }
             * 
             */
        }
        
        /**
         * Function for load html of crocdoc file
         *
         * @param string $crocdocid id of crocdoc file of view
         * 
         * @access public
         * 
         * @return project view html from crocdoc_file
        **/
        public function crocdocfile($crocdocid){
		header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
//            $md = new DocumentManager;
//            $sessionkey = $md->createSession($crocdocid);
//            $fileview = 'https://crocodoc.com/view/'.$sessionkey;
//            $content = file_get_contents($fileview);
//            $content = str_replace('</head>','<style>.toolbar .current-page.active, .toolbar .current-page:hover{background-color:unset !important; border:1px solid transparent !important} 
//                                    .toolbar .current-page{color:#000 !important; text-shadow:none !important;} 
//                                    .toolbar{display:block !important; position:relative !important; background:#525252 !important; border-style:none !important} 
//                                    .toolbar .logo{background:none !important;} 
//                                    .docviewer .doc{height:96% !important; background:none repeat scroll 0 0 #353535 !important;}
//                                    </style></head>', $content);
//            $content=str_replace('Powered by', '', $content);
//            echo $content;
            $refreshToken = $this->project_model->getRefreshToken();
            $accessToken = $this->getAccessTokenByRefresh($refreshToken);
		$html='<head>
                        <meta http-Equiv="Cache-Control" Content="no-cache" />
                        <meta http-Equiv="Pragma" Content="no-cache" />
                        <meta http-Equiv="Expires" Content="0" />
                    </head>';
            $html.='<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>';
            $html.='<script src="https://cdn01.boxcdn.net/platform/preview/1.43.0/en-US/preview.js"></script>';
            $html.='<link rel="stylesheet" href="https://cdn01.boxcdn.net/platform/preview/1.43.0/en-US/preview.css" />';
            $html.='<div class="preview-container" style="height:100%;width:100%"></div>
                <style>.pdfViewer, .bp-loaded{background: #000000 !important}</style><script>
                    var preview = new Box.Preview();
                    preview.show("'.$crocdocid.'", "'.$accessToken.'", {
                        container: ".preview-container",
                        header: "none",
                        });
                    </script>';
            echo $html;
            //$this->load->view('crocdoc_file');
        }
        
        function search(){
            $search_txt=  $this->input->post('search_txt');
            $project= $this->input->post('project');
            $type=  $this->input->post('type');
            $limit= $this->input->post('show');
            $sorting= $this->input->post('sort');
            switch ($sorting) {
                case 'oldest_first':
                    $sortfield='added_date';
                    $sort='asc';
                    break;
                
                case 'user':
                    $sortfield='stage_name';
                    $sort='desc';
                    break;
                
                case 'project':
                    $sortfield='identifier';
                    $sort='desc';
                    break;

                default:
                    $sortfield='added_date';
                    $sort='desc';
                    break;
            }
            $result=  $this->project_model->search_data($search_txt, $project, $type, $limit, $sortfield, $sort);
            echo json_encode($result);
        }
        
        /**
         * Function for remove blog comment
         *
         * @access public
         * 
        **/
        public function removeBlogComment() {
            $commentId=  $this->input->post('commentid');
            $deleteComment=  $this->project_model->removeCommentBlog($commentId, $this->session->userdata('user_id'));
        }
        
        
        public function getNewBlogComment() {
            $result=  array();
            $projectid=  $this->input->post('projectid');
            $commentid=  $this->input->post('commentid');
            $data=  $this->project_model->newBlogComments($projectid, $commentid);
            foreach ($data as $key=>$value) {
                if (isset($value['added_date'])) {
                    $value['added_date']=date('d.m.y, h:i a', strtotime($value['added_date']));
                }
                $result[$key]=$value;
            }
            echo json_encode($result);
        }
        
        public function editComment() {
            $projectid=  $this->input->post('projectid');
            $commentid=  $this->input->post('commentid');
            $comment=  $this->project_model->getBlogComment($projectid, $commentid);
            
            echo json_encode($comment);
        }
        
        /**
         * Function for load plan view of a project in details
         *
         * @param string $projectId primary id of a project from "projects" table
         * @param string $albumid primary id of a video albums from "files_folder" table
         * @param string $fileid primary id of a video from "user_files" table
         * @param string $sortColumn column name that you want to sort
         * @param string $sortby sorting value like asc, desc
         * 
         * @access public
         * 
         * @return project view html from project_view.php 
        **/
      public function plans($projectId, $albumid='', $fileid='', $sortColumn='', $sortby='') {
            $data['title']= $this->project_model->project_title($projectId);
            $data['top_menu']=  $this->topMenu($projectId);
            $data['selected_menu']='plans';
            $this->load->view('plans_view', $data);
      }
      
    public function lastundo(){ 
        $last_remove_array=$this->session->userdata('last_remove');
        //echo '<pre>'; print_r($last_remove_array); die('sss');
        if(is_array($last_remove_array)){
            $this->project_model->undo_last_remove();
            //$last_remove_array =  array('type'=>'assets', 'table'=>$type, 'id'=>$imageid);
            switch($last_remove_array['type']){
                case 'project':
                    $redirect='project/view/'.$last_remove_array['id'];
                break;

                case 'album':
                    $redirect='project/'.$last_remove_array['table'].'/'.$last_remove_array['projectid'].'/'.$last_remove_array['id'];
                break;

                case 'assets':
                    $redirect='project/'.$last_remove_array['table'].'/'.$last_remove_array['projectid'].'/'.$last_remove_array['albumid'].'/'.$last_remove_array['id'];
                    //$this->remove_album_image($last_remove_array['table'], $last_remove_array['id'], 'undo');
                break;
            }
            $this->session->unset_userdata('last_remove');
            redirect($redirect);
        }          
    }
    
    function getAccessTokenByRefresh($refreshToken){
//        $client_id = 'o3s2ds1jp86wwi9blik0dkm7ne2408is';
//        $client_secret = 'xbK4F1n3Nimr4PCJDvY79uvsh4G7gOjr';
        $defaultOptions = array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_VERBOSE        => true,
                CURLOPT_HEADER         => false,
                CURLINFO_HEADER_OUT    => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => false,
        );

        $ch = curl_init('https://www.box.com/api/oauth2/token');
        $options = $defaultOptions;
        $options[CURLOPT_POST] = true;
        $options[CURLOPT_POSTFIELDS] = array(
                'grant_type'=>'refresh_token',
                'refresh_token'=>$refreshToken,
                'client_id'=>$this->client_id,
                'client_secret'=>$this->client_secret
        );
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        $responseArr = json_decode($response, true);
        //echo 'access_token='.$responseArr['access_token'].', refresh_token='.$responseArr['refresh_token']; die;
        $this->project_model->insertRefreshToken($responseArr['access_token'], $responseArr['refresh_token']);
        return $responseArr['access_token'];
    }
    
    public function boxUploadFile($file){
        //echo '$file='.$file; die;
//        $client_id = 'o3s2ds1jp86wwi9blik0dkm7ne2408is';
//        $client_secret = 'xbK4F1n3Nimr4PCJDvY79uvsh4G7gOjr'; 
//        $refreshTokenArr = getAccessTokenByRefresh($refreshToken, $client_id, $client_secret);  //echo '<pre>'; print_r($refreshToken); die('----s');
//
//        $_SESSION['access_token'] = $refreshTokenArr['access_token'];
//        $_SESSION['refresh_token'] = $refreshTokenArr['refresh_token'];
//        
//        $refreshToken = getAccessTokenByRefresh($refreshToken, $client_id, $client_secret);
//        $accessToken = $_SESSION['access_token'];
        
        $refreshToken = $this->project_model->getRefreshToken();
        $url = "https://upload.box.com/api/2.0/files/content";
        $accessToken = $this->getAccessTokenByRefresh($refreshToken);
        //$file_upload = $_FILES['file']['tmp_name'];
        //$file_upload = $file;
        $fileArr = pathinfo($file);
        $filename = $fileArr['basename'];
        $type = mime_content_type($file);
        $json = json_encode(array(
                               // 'name' => $_FILES['file']['name'], 
                                'name' => $filename, 
                                'parent' => array('id' => '0')
                            ));
        $fields = array(
                      'attributes' => $json,
                      //'file'=>new CurlFile($_FILES['file']['tmp_name'],$_FILES['file']['type'],$_FILES['file']['name'])
                      'file'=>new CurlFile($file,$type,$filename)
                  );

        try {
            
            
            
            
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.$accessToken, 
                'Content-Type:multipart/form-data'
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            $response = curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
        $response = json_decode($response, true);
        //echo '<pre>'; print_r($response); echo '</pre>';
        return $fileId = $response['entries'][0]['id'];
    }
}
?>