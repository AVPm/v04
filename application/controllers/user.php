<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * user controller class file
 *
 */

//include_once($_SERVER['DOCUMENT_ROOT'].'/includes/paypal/paypal.php');

class User extends CI_Controller{
	public function __construct()
	{
            /*
            * call the constructor of CI
            */
            parent::__construct();
            /*
            * call the user model file
            */
            $this->load->model('user_model');
            
            
            /*
             * call image crope library
             */
           // $this->load->library('image_cropper');
	}
        
        /**
         * Index function of user controller
         *
         * @access public
         * 
         **/  
	public function index()
	{
            $this->db->cache_delete_all();
            //ini_set('display_errors', 1);
            //die('user controller testing');
            if(($this->session->userdata('user_name')!=""))
            {
                // if user is already login then welocme function call
                $this->session->unset_userdata('project_role');
                $this->session->unset_userdata('project_role2');
                $this->welcome();
            }
            else{
                //else show registration page to user
                /*
                 * $data['title'] => means title of home page
                 * $data['main_content'] => main html file for view
                 * $data['data'] => to show data on web page
                 */
                $data['title']= 'Welcome to AVPmatrix';
                $activate_msg=($this->session->flashdata('success_msg')!='') ? $this->session->flashdata('success_msg') : '';
                //$data['main_content']='registration_view';
                $data['jsArray']=array('home');
                $data['cssArray']=array('mainpage'); 
                $data['subscriptions']= $this->user_model->subscriptions();
                
               // $data['data']=  array('activate_msg'=>$activate_msg);
                $data['activate_msg']= $activate_msg;
                
                // finally load template with all data
                //$this->load->view('template', $data);
                $this->load->view('mainpage', $data);
            }
	}
        
        /**
         * Function for check username in "users" tabe
         *
         * @access public
         * 
         * @print return variable from check_username function from user model
        **/
        public function checkusername(){
            $uname = $this->input->post('username');
            echo $this->user_model->check_username($uname);
        }
        
        /**
         * Function for check user's email in "users" tabe
         *
         * @access public
         * 
         * @print return variable from check_useremail function from user model
        **/
        public function checkuseremail(){
            $email = $this->input->post('useremail');
            echo $this->user_model->check_useremail($email);
        }
        
        /**
         * Function for update user profile
         *
         * @access public
         * 
        **/
        public function updateProfile() {
            //call update_profile function from user model
            $this->user_model->update_profile();
            
            // set success message in session for just once
            $this->session->set_flashdata('profile_update_success', 'Your profile is updated');
            
            // redirect to login funtion in user controller after profile update
            redirect(base_url('user/login/'), 'refresh');
        }
        
        /**
         * Function for show welcome page after user login
         * 
         * @param int $roleid is for get projects according role id of user, default is empty
         *
         * @access public
         * 
        **/
	public function welcome($roleid='1', $roleid2='4')
	{

            /*
             * $data['title'] => means title of web page
             * $data['user_details'] => get detail of login user from "users" table
             * $data['user_listing'] => get user listing of login user from "user_relation" table
             * $data['user_projects'] => get user project list of login user from "projects" table
             * $data['jsArray'] => get all js files to show on page
             * $data['cssArray'] => get all css files to show on page
             * $data['main_content'] => main html file for view
             * $data['data'] => to show data on web page
             */
            if($roleid!=''){
                $newdata = array(
                    'project_role'=> $roleid,
                    'project_role2'=>$roleid2
                 );
                $this->session->set_userdata($newdata);
            }
            
            $roleid2 = ($roleid2!='') ? $roleid2 : '';
            
            $data['title']= 'Welcome';
            $data['user_details']= $this->user_model->user_details($this->session->userdata('user_id'));

            $data['user_listing']= $this->user_model->user_listing($this->session->userdata('user_id'));
            
            $data['user_projects']= $this->user_model->user_projects($this->session->userdata('user_id'), $roleid);
            
            //get all products of user
            $all_projects= $this->user_model->allProjects($this->session->userdata('user_id'));
            
            //get add user limit of a user
            $userLimit= $this->user_model->checkUserLimit($this->session->userdata('user_id'));
            
            //$data['project_participated']= $this->user_model->project_participated($this->session->userdata('user_id'));
            if($roleid2!='' && $roleid!=$roleid2){
                $data['project_participated']= $this->user_model->user_projects($this->session->userdata('user_id'), $roleid2);
            }
            else{
                $data['project_participated']='';
            }

            $data['main_content']='welcome_view';
            $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min', 'jquery.ui.touch-punch.min');
            $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
            $data['data']=array('roleid'=>$roleid, 'roleid2'=>$roleid2, 'all_projects'=>$all_projects, 'userLimit'=>$userLimit);
            $this->load->view('template', $data);
	}
        
        /**
         * Function for user login
         *
         * @access public
         * 
        **/
	public function login()
	{
            // check if user add project
            if($this->input->post('identifier')!=''){
                $upload_data=$this->do_upload('upload_image');
                $filename='';
                // check if user upload any image as thumbnail in project
                if(isset($upload_data['upload_data'])){
                    $filename=$upload_data['upload_data']['file_name'];
                    $parts = explode('.', $filename);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $file=$parts[0];
                    $fileExt=$parts[1];
                   // $imagewidth=$upload_data['upload_data']['image_width'];
                   // $imageheight=$upload_data['upload_data']['image_height'];
                    // resize of image of project as thumbnail with 160X160 size
                    
                    //old image resize code
                    
                    //$this->resize_image('./uploads/' . $upload_data['upload_data']['file_name'],  './uploads/'.$file.'_thumb.png','160','160');
                     
                    $this->image_crop_resize('300', '300', $filename, $file, $fileExt, 'little', 'png');
                }
                
                //call add_project function of user model to add project information in "projects" table
                $this->user_model->add_project($filename, 'png');
               // unlink('./uploads/' .$filename);
                //$this->session->set_flashdata('add_project_msg', 'Your project is successfully added');
                redirect('/user/login/', 'refresh');
            }
		$email=$this->input->post('email');
		$password=md5($this->input->post('pass'));

		$result=$this->user_model->login($email,$password);
		if($result) $this->welcome();
		else        $this->index();
	}
        
        /**
         * Function for file upload in server
         *
         * @param string $filename name of image file
         * 
         * @access public
         * 
         * @return array Containing a multidimensional associative array with the recordsets 
        **/
        function do_upload($filename='', $ext='')
	{
            ini_set( 'memory_limit', '2000M' );
            ini_set('upload_max_filesize', '2000M');  
            ini_set('post_max_size', '2000M');  
            ini_set('max_input_time', 36000);  
            ini_set('max_execution_time', 36000);
            $ext = strtolower($ext);
            //die('filename='.$filename.', ext='.$ext);
            if($ext=='zip'){
                $config['upload_path'] = './uploads/webpages/';
            }
            else{
                $config['upload_path'] = './uploads/';
            }
            
            $config['max_size'] = '1000000000';
            if($ext=='psd'){
                $config['allowed_types'] = '*';
            }
            else{
                $config['allowed_types'] = 'gif|jpg|png|jpeg|mp4|avi|mpeg|avi|pdf|doc|docx|xlsx|txt|text|xl|xls|ppt|pptx|word|psd|zip|flv|mkv|mov';
            }
            
            //$config['max_size']	= '100';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $config['overwrite']=TRUE;
            //echo '<pre>'; print_r($config['allowed_types']); die;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload($filename))
            {
                    $data = array('error' => $this->upload->display_errors());
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                
            }
            return $data;
	}
        
        /**
	 * Image processor
	 * @param int $nw, Resize to this width
	 * @param int $nh, Resize to this height
	 * @param string $source, Image name on the server
	 * @param string $stype, Source image type (jpg, etc)
	 * @param string $type, big: you want to crop the original image, else: you want to resize the image
	 */
	function image_crop_resize($nw, $nh, $source, $filename, $stype, $type = "little", $ext='jpg') {
		//header('Content-type: image/jpg');
		
		$nw = (int)$nw;
		$nh = (int)$nh;
		
		$source = constant('FILE_INCLUDE_PATH').'/uploads/'.$source;
                
               // print_r($source) ;
               // die();
		
		$size = getimagesize($source);
		
		$w = $size[0];
		$h = $size[1];
                $stype = strtolower($stype);
                $ext = strtolower($ext);
		switch($stype)
		{
			case 'gif':
				$simg = imagecreatefromgif($source);
				break;
			case 'jpg':
                        case 'jpeg':
				$simg = imagecreatefromjpeg($source);
				break;
			case 'png':
				$simg = imagecreatefrompng($source);
                break;
            default:
				$simg = imagecreatefrompng($source);
				break;
			// case 'webp':
			// 	$simg = imagecreatefromwebp($source);
			// 	break;
		}
		$dimg = imagecreatetruecolor($nw, $nh);
		
		/**
		 * Here we get the center of image
		 */
		$wm = $w/$nw;
		$hm = $h/$nh;
		$h_height = $nh/2;
		$w_height = $nw/2;
		$ow = ($nw - $w) / 2;
		$oh = ($nh - $h) / 2;		
		if($type == "big")
		{
			imagecopy($dimg,$simg,0,0,-$ow,-$oh,$nw,$nh);
			imagejpeg($dimg);
		}
		else
		{
			/**
			 * If image is horizontal
			 */
			if($w> $h)
			{
				$adjusted_width = $w / $hm;
				$half_width = $adjusted_width / 2;
				$int_width = $half_width - $w_height;
				imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
			}
			/**
			 * If image is vertical
			 */
			elseif(($w <$h) || ($w == $h))
			{
				$adjusted_height = $h / $wm;
				$half_height = $adjusted_height / 2;
				$int_height = $half_height - $h_height;
				imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
			}
			else
			{
				imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
			}
			//imagejpeg($dimg, './uploads/'.$filename.'_thumb'.'jpg');
                        if($type=='thumbnail'){
                            switch ($ext) {
                                case 'png':
                                    imagepng($dimg, constant('FILE_INCLUDE_PATH').'/uploads/'.$filename.'_'.$nw.'x'.$nh.'_thumb.'.$ext);
                                break;

                                default:
                                    imagejpeg($dimg, constant('FILE_INCLUDE_PATH').'/uploads/'.$filename.'_'.$nw.'x'.$nh.'_thumb.'.$ext);
                                break;
                            }
                        }
                            //imagejpeg($dimg, constant('FILE_INCLUDE_PATH').'/uploads/'.$filename.'_'.$nw.'x'.$nh.'_thumb.'.$ext);
                        else{
                            switch ($ext) {
                                case 'png':
                                    imagepng($dimg, constant('FILE_INCLUDE_PATH').'/uploads/'.$filename.'_thumb.'.$ext);
                                break;

                                default:
                                    imagejpeg($dimg, constant('FILE_INCLUDE_PATH').'/uploads/'.$filename.'_thumb.'.$ext);
                                break;
                            }
                            
                        }
		}
	}
       
        
        /**
         * Function for resize image file and save on server
         *
         * @param string $sourcePath source path of image file
         * @param string $desPath destination path of image file
         * @param string $width set width of image, default is 160
         * @param string $height set height of image, default is 160
         * 
         * @access public
         * 
         * @return TRUE if file is successfully resized else FALSE 
        **/
        function resize_image($sourcePath, $desPath, $width = '160', $height = '160', $imageWidth, $imageHeight)
        {
            $this->load->library('image_lib');
            $this->image_lib->clear();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $sourcePath;
            $config['new_image'] = $desPath;
            $config['quality'] = '100%';
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            //$config['thumb_marker'] = '';
            $config['width'] = $width;
            $config['height'] = $height;
            $dim = (intval($imageWidth) / intval($imageHeight)) - ($width / $height);
            $config['master_dim'] = ($dim > 0)? "height" : "width";
            //$this->load->library('image_lib');
            //$this->image_lib->initialize($image_config);
            $this->image_lib->initialize($config);

            if ($this->image_lib->resize()){
                $this->imageCrop($height, $width, $desPath);
                return true;
            }
                
            return false;
        }
        
        /**
         * Function for crop image file
         *
         * @param string $height set height of image
         * @param string $width set width of image
         * @param string $imageSrc source path of image file
         * 
         * @access public
         * 
         * @return TRUE if file is successfully resized else FALSE 
        **/
        function imageCrop($height, $width, $imageSrc) {
            $image_config['image_library'] = 'gd2';
            $image_config['source_image'] = $imageSrc;
            $image_config['new_image'] = $imageSrc;
            $image_config['quality'] = "100%";
            $image_config['maintain_ratio'] = FALSE;
            $image_config['width'] = $width;
            $image_config['height'] = $height;
            $image_config['x_axis'] = '0';
            $image_config['y_axis'] = '0';

            $this->image_lib->clear();
            $this->image_lib->initialize($image_config); 

            if (!$this->image_lib->crop()){
                    return false;
            }else{
                return true;
            }
        }
        
        /**
         * Function for thank you page after registration is successfully done
         *
         * @access public
         * 
        **/
	public function thank()
	{
		$data['title']= 'Thank';
		$this->load->view('header_view',$data);
		$this->load->view('thank_view.php', $data);
		$this->load->view('footer_view',$data);
	}
        
        /**
         * Function for registration page
         *
         * @access public
         * 
        **/
	public function registration()
	{
            /*
             * server side validation check all mandatory fields
             */
            
            $this->load->library('form_validation');
            // field name, error message, validation rules
            //$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email_address', 'Your Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');
            $this->form_validation->set_rules('stage', 'Stage', 'trim|required|xss_clean');

            // if any field is not validate
            if($this->form_validation->run() == FALSE)
            {
                    $this->index();
            }
            else
            {
                //if all field validate properly then user is successfully added in "users" table 
                $userid=$this->user_model->add_user();
                
                $subscriptionId=$this->input->post('subscriptionId');
                
                //place order of user in "sales" table
                $invoice=date("His").rand(1234, 9632);
                $orderId= $this->user_model->addSales($userid, $subscriptionId, $invoice);
                
                if($subscriptionId>1){
                    $subscriptionName = $this->db->select('name')->get_where('subscription', array('id' => $subscriptionId))->row()->name;
                    $amount = $this->db->select('price')->get_where('subscription', array('id' => $subscriptionId))->row()->price;
                    $form='<form action="'.base_url("includes/paypal/paypal.php?sandbox=1").'" method="post" id="paypalForm"> 
                                <input type="hidden" name="action" value="process" />
                                <input type="hidden" name="cmd" value="_cart" /> 
                                <input type="hidden" name="currency_code" value="USD" />
                                <input type="hidden" name="invoice" value="'.$invoice.'" />
                                <input type="hidden" name="product_id" value="'.$subscriptionId.'" />
                                <input type="hidden" name="product_name" value="'.$subscriptionName.'" />
                                <input type="hidden" name="product_quantity" value="1" />
                                <input type="hidden" name="product_amount" value="'.$amount.'" />
                                <input type="hidden" name="payer_fname" value="'.$this->input->post('first_name').'" />
                                <input type="hidden" name="payer_lname" value="'.$this->input->post('last_name').'" />
                                <input type="hidden" name="payer_email" value="'.$this->input->post('email_address').'" />
                            </form>
                            <script>document.getElementById("paypalForm").submit();</script>';
                    echo $form;
                    die();
                }
                
                $this->index();
                // show thank you page after registration done successfully
               // $this->thank();
            }
	}
        
        /**
         * Function for logout page
         *
         * @access public
         * 
        **/
	public function logout()
	{
            $newdata = array(
            'user_id'   =>'',
            'user_name'  =>'',
            'user_email'     => '',
            'logged_in' => FALSE,
            );
            $this->session->unset_userdata($newdata );
            $this->session->sess_destroy();
            $this->index();
	}
        
        /**
         * Function for add user in user's list
         * 
         * @param string $newUser is means user account is new or already created in "users" table
         *
         * @access public
         * 
         * @print message if user added successfully
         * 
        **/
        public function adduserlist($newUser='') {
            $this->load->library('form_validation');
            $this->load->library('email');
            $config['mailtype'] = 'html';
            $config['protocol'] = 'sendmail';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $this->email->initialize($config);
            $email=$this->input->post('useremail');
            $data['email']=$email;
            $this->form_validation->set_rules('useremail', 'Your Email', 'trim|required|valid_email');
            if($this->form_validation->run() == TRUE && $newUser=='')
            {
                $addUser=$this->user_model->adduser_list();
                if($addUser==TRUE){
                    echo 'User is added successfully';
                }
                else {
                    $length = 6;
                    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, $length);
                    $data['password']=$randomString;
                    $this->load->view('ajax_signup', $data);
                }
                    
            }
            /*
             * if user is new then a welcome email is send to user with username and password
             */
            else if($newUser=='newuser'){
                $this->user_model->addnewuser_list();
                $email =$this->input->post('email_address');
	    	$password = $this->input->post('password');
                $this->email->from(constant('DEFAULT_EMAIL'), constant('DEFAULT_EMAIL_NAME'));
                $this->email->to($email); 
                $this->email->subject('Welcome Email');
                $this->email->message('Hi This is a Welcome email Your Credentials are following: <br> Username/Email: '.$email. ' <br> Password: '.$password);	
                $this->email->send();
                echo 'User is added successfully';
            }
            
            /*
             * if user is added through only email and email is sent to user with a link to complete his/her profile
             */
            elseif ($newUser=='email') {
                $addUserid=$this->user_model->addnewuseremail_list();
                $this->email->from(constant('DEFAULT_EMAIL'), constant('DEFAULT_EMAIL_NAME'));
                $this->email->to($email); 
                $this->email->subject('Registration Email');
                $this->email->message('Hi <br>please complete and register your profile. Click on <a href="http://v04.avpmatrix.com/profile/complete_registration/'.$addUserid.'">register</a>.');	
                $this->email->send();
                echo 'User is added successfully';
                
            }
        }
        
        /**
         * Function for change role of user profile
         * 
         * @access public
         * 
         * @print message if user role is successfully updated or not
         * 
        **/
        
        public function changerole(){
            $role_value =$this->input->post('role_value');
            $userid=$this->input->post('userid');
            
            //call the function change_role of user model
            $update=$this->user_model->change_role($userid, $role_value);
            if($update==true){
                echo 'Role successfully updated';
            }
            else{
                echo 'Role not updated';
            }
        }
        
        /**
         * Function for check project identifier is unique or not
         * 
         * @access public
         * 
         * @print message if identifier is unique or not
         * 
        **/
        public function projectIdentifier() {
            $identifier = $this->input->post('identifier');
            $editpost=$this->input->post('edit');
             if(!empty($editpost)){
                 $edit = $this->input->post('edit');
             }
             else{
                 $edit='';
             }
            echo $this->user_model->check_identifier($identifier, $edit);
        }
        
        /**
         * Function for remove user from "users" table or "user_relation" table
         * 
         * @access public
         * 
         * @print message if identifier is unique or not
         * 
        **/
        public function remove() {
            $userid = $this->input->post('userid');
            $this->user_model->remove_user($userid);
        }
        
        
        /**
         * Function for check user's ssubscription details
         * 
         * @access public
         * 
         * load view of subscriptions
         * 
        **/
        public function subscriptions() {
            //die('coming soon');
            //$this->load->library('../controllers/project');
            //$this->user->welcome($roleid, $roleid2);
            $place_order_msg=($this->session->flashdata('place_order')!='') ? $this->session->flashdata('place_order') : '';
            $userid = $this->session->userdata('user_id');
            //$data['top_menu']= $this->project->topMenu();
            $userSubscriptions=$this->user_model->getUserSubscriptions($userid);
            $remainingUsers=$this->user_model->getRemainingUsers($userid);
            $trafficSize=$this->user_model->getRemianingTraffic($userid);
            $trafficSize=number_format((($trafficSize/1024)/1024), 3);
            $subscriptions= $this->user_model->subscriptions(1);
            //die('traffic: '.$trafficSize);
            $data['selected_menu_subscrip']='yes';
            $data['main_content']='subscriptions_view';
            $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
            $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
            $data['data']=  array('userid'=>$userid, 'userSubscriptions'=>$userSubscriptions, 'remainingUsers'=>$remainingUsers, 'trafficSize'=>$trafficSize, 'place_order_msg'=>$place_order_msg, 'subscriptions'=>$subscriptions);
            $this->load->view('template', $data);
            
            //$this->user_model->remove_user($userid);
        }
        
        /**
         * Function for file management module
         * 
         * @access public
         * 
         * load view of File Management
         * 
        **/
        public function file_management($projectid='', $albumid='', $type='') {
            //die('coming soon');
            $userid = $this->session->userdata('user_id');
            if($projectid>0 && $albumid>0){
                if($this->input->post('type')=='assets'){
                    $filetype=$this->input->post('type');
                    $subtype=$this->input->post('assets_type');
                    $arrayId=$this->input->post('selectFolder');
                    $this->file_manage_action($filetype, $arrayId, $subtype);
                    redirect(base_url('/user/file_management/'.$projectid.'/'.$albumid.'/'.$type), 'refresh');
                }
                $data['selected_menu_file_manage']='yes';
                $list_albums=$this->user_model->getAlbumAssets($projectid, $albumid, $type, $userid);
                //echo '<pre>'; print_r($list_albums); die();
                $data['list_albums']=  $list_albums;
                $data['project_name']=  $this->db->select('title')->get_where('projects', array('id' => $projectid))->row()->title;
                $albumname='';
                switch ($type) {
                    case 'files':
                        $albumname=$this->db->select('name')->get_where('files_folder', array('id' => $albumid))->row()->name;
                        $downloadtype='file';
                    break;
                
                    case 'videos':
                        $albumname=$this->db->select('name')->get_where('videos_folder', array('id' => $albumid))->row()->name;
                        $downloadtype='video';
                    break;
                
                    case 'images':
                        $albumname=$this->db->select('name')->get_where('user_albums', array('id' => $albumid))->row()->name;
                        $downloadtype='image';
                    break;

                    
                }
                $data['album_name']=  $albumname;
                $data['main_content']='file_management/assets_list';
                $data['projectid']=$projectid;
                $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
                $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
                $data['data']=  array('userid'=>$userid, 'download_type'=>$downloadtype, 'curr_albumid'=>$albumid);
                $this->load->view('template', $data);
            }
            else if($projectid>0 && $albumid==''){
                $chkUserProject=  $this->user_model->chkuserProject($projectid);
                if($chkUserProject){
                    if($this->input->post('type')=='album'){
                        $type=$this->input->post('type');
                        $arrayId=$this->input->post('selectFolder');
                        $this->file_manage_action($type, $arrayId);
                        redirect(base_url('/user/file_management/'.$projectid), 'refresh');
                    }
                    if($this->input->post('project_album_title')!=''){
                        //$this->load->model('project_model');
                        //die('name='.$this->input->post('project_album_title').' --- type='.$this->input->post('project_album_type'));
                        //$insertId=$this->project_model->save_album($projectid, '', $this->input->post('project_album_type'));
                        $name=$this->input->post('project_album_title');
                        $type=$this->input->post('project_album_type');
                        $insertId=$this->user_model->insertNewProjectAlbum($projectid, $name, $type);
                        redirect(base_url('/user/file_management/'.$projectid), 'refresh');
                    }
                    $data['selected_menu_file_manage']='yes';
                    $data['list_albums']=  $this->user_model->getprojectListAlbums($projectid);
                    $data['project_name']=  $this->db->select('title')->get_where('projects', array('id' => $projectid))->row()->title;
                    $data['main_content']='file_management/album_list';
                    $data['projectid']=$projectid;
                    $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
                    $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
                    $data['data']=  array('userid'=>$userid);
                    $this->load->view('template', $data);
                }
                else{
                    redirect(base_url());
                }
                
            }
            else{
                if($this->input->post('type')=='project'){
                    $type=$this->input->post('type');
                    $arrayId=$this->input->post('selectFolder');
                    $this->file_manage_action($type, $arrayId);
                    redirect(base_url('/user/file_management'), 'refresh');
                }
                // check if user add project
                if($this->input->post('project_folder_title')!=''){
                    $foldername=$this->input->post('project_folder_title');
                    $this->user_model->add_project_folder($foldername);
                    redirect(base_url('/user/file_management/'), 'refresh');
                }
                if($this->input->post('rename_folder')!=''){
                    $type=$this->input->post('type');
                    $id=$this->input->post('id');
                    $name=$this->input->post('rename_folder');
                    $rename=  $this->user_model->renamefolder($id, $name, $type);
                    if($rename>0)
                        die('success');
                    else
                        die('fail');
                }
                //$data['top_menu']= $this->project->topMenu();

                //die('traffic: '.$trafficSize);
                $data['selected_menu_file_manage']='yes';
                $data['list_projects']=  $this->user_model->getprojectList($userid, 'yes');
                $data['main_content']='file_management/index';
                $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
                $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
                $data['data']=  array('userid'=>$userid);
                $this->load->view('template', $data);
            }
            
        }
    
    /**
        * Function for file management action
        * 
        * @param string $type type of file
        * @param string $arrayId files id in array form
        * @param string $subtype subtype of file
        * 
        * load view of File Management
        * 
       **/
    function file_manage_action($type, $arrayId, $subtype=''){
        //load user controler file
        $this->load->model('project_model');
        switch ($type) {
            case 'project':
                foreach($arrayId as $val){
                    $this->project_model->remove_project_data($val);
                }

            break;
            
            case 'album':
                foreach($arrayId as $val){
                    $explodeVal=explode('_', $val);
                    $filetype=$explodeVal[0];
                    $imageid=$explodeVal[1];
                    $this->project_model->remove_album_image($filetype, $imageid);
                }

            break;
            
            case 'assets':
                $filetype=$subtype.'s';
                foreach($arrayId as $val){
                    $this->project_model->remove_album_image($filetype, $val);
                }

            break;

        }
    }
    
    /**
        * Function for get aliases
        * 
        * @param string $type type of file
        * @param string $id files id 
        * 
        * load view of File Management
        * 
       **/
    function aliases($type, $id){
        $userid = $this->session->userdata('user_id');
        if(!isset($type) || !isset($id)){
            redirect('user/file_management');
        }
        //die('coming soon');
        if($this->input->post('projects') && $this->input->post('albums')>0){
            $projectid=$this->input->post('projects');
            $type=$this->input->post('type');
            $albumid=$this->input->post('albums');
            $fileid=$this->input->post('fileid');
            //echo 'userid='.$userid.', projectid='.$projectid.', type='. $type.', albumid='.$albumid.', fileid=', $fileid;
            $saveAliases=$this->user_model->save_aliases($userid, $projectid, $type, $albumid, $fileid);
            //die($saveAliases.'=dsdsdsd');
            $this->session->set_flashdata('aliases_success', 'Aliases of file is done');
            redirect('user/file_management/'.$projectid.'/'.$albumid.'/'.$type);
        }
        
        $data['selected_menu_file_manage']='yes';
        $data['list_projects']= $this->user_model->getprojectList($userid);
        $data['main_content']='file_management/aliases';
        $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
        $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
        $data['data']=  array('userid'=>$userid, 'type'=>$type, 'id'=>$id);
        $this->load->view('template', $data);
    }
    
    
    /**
        * Function for get webpages listing
        * 
        * @param string $id webpage id 
        * 
        * load view of webpages
        * 
       **/
    function webpages($id=0){
        //die('coming soon');
        $userid = $this->session->userdata('user_id');
        if($id>0){
            $foldername=$this->db->select('foldername')->get_where('wepages', array('id' => $id))->row()->foldername;
            $uploadedUserid=$this->db->select('userid')->get_where('wepages', array('id' => $id))->row()->userid;
            $folder=$foldername.'_'.$id;
            if(file_exists(constant('FILE_INCLUDE_PATH').'/uploads/webpages/'.$folder) && $uploadedUserid==$userid){
                if(file_exists(constant('FILE_INCLUDE_PATH').'/uploads/webpages/'.$folder.'/index.html')){
                    //$html=file_get_contents(base_url('uploads/webpages/'.$folder.'/index.html'));
                    $link=base_url('uploads/webpages/'.$folder.'/index.html');
                }
                else if(file_exists(constant('FILE_INCLUDE_PATH').'/uploads/webpages/'.$folder.'/'.$foldername.'/index.html')){
                    //$html=file_get_contents(base_url('uploads/webpages/'.$folder.'/'.$foldername.'/index.html'));
                    $link=base_url('uploads/webpages/'.$folder.'/'.$foldername.'/index.html');
                }
                echo '<iframe src="'.$link.'" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
                            Your browser doesnt support iframes
                        </iframe>';
                //echo $html;
                die();
            }
            else{
                redirect('user/webpages');
            }
        }
        $data['selected_web_page']='yes';
        $data['list_wepages']=  $this->user_model->getWebPagesList($userid);
        $data['main_content']='webpages/index';
        $data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
        $data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
        $data['data']=  array('userid'=>$userid);
        $this->load->view('template', $data);
    }
    
    function getAlbums(){
        $type=  $this->input->post('type');
        $id=  $this->input->post('id');
        $list=  array();
        if($id>0){
            $list=$this->user_model->getProjectAlbums($type, $id);
        }
        //echo '<pre>'; print_r($list); die;
        echo json_encode($list);
        die();
    }
}
?>