<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * profile controller class file
 *
 */
class profile extends CI_Controller{
	public function __construct()
	{
            /*
             * call the constructor of CI
             */
            parent::__construct();
            /*
             * call the user model file
             */
            $this->load->model('profile_model');
	}
        
        /**
         * Function for activate the user profile
         * 
         * @param string $userid user's primary id from "users" table
         * 
         * @access public
         * 
        **/	
	public function activate($userid)
	{
            $this->session->set_flashdata('success_msg', 'Your profile is activated. You can login now');
            $this->profile_model->activeProfile($userid);
            redirect(base_url());    
	}
	
        /**
         * Function for load web page of complete registration for new user
         * 
         * @param string $userid user's primary id from "users" table
         * 
         * @access public
         * 
        **/
        public function complete_registration($userid) {
            if ( !empty($_POST) ) {
                $this->completeregistration($userid);
            }
            else{
                $email=$this->profile_model->userEmail($userid);
                $data['main_content']='complete_registration';
                $data['data']=array('useremail'=>$email, 'userid'=>$userid);
                $this->load->view('template', $data);
                //$this->profile_model->activeProfile($userid);
            }
            
            
        }
        
        /**
         * Function for save all information in "users" table of new user who complete his/her profile
         * 
         * @access public
         * 
        **/
        public function completeregistration($userid) {
            $this->load->library('form_validation');
            // field name, error message, validation rules
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email_address', 'Your Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');
            $this->form_validation->set_rules('stage', 'Stage', 'trim|required|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                    $this->complete_registration($userid);
            }
            else
            {
                $this->profile_model->complete_userProfile();
                $this->session->set_flashdata('success_msg', 'Your profile is successfully completed. You can login now');
                $data['main_content']='thank_view';
                $activate_msg=$this->session->flashdata('success_msg');
                $data['data']=array('title'=>'Thank', 'activate_msg'=>$activate_msg);
                $this->load->view('template', $data);
            }
        }
}
?>