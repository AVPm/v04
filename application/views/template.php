<?php
// ****************************************************************************
// 
//     Main single template view of site
//
// ****************************************************************************

/**
  * Include header view
  *
  */
$this->load->view('include/header', $jsArray='', $cssArray='');

/**
  * Include page view files that you want to view
  *
  */
echo '<div id="content">';
$this->load->view($main_content, $data);
echo '</div>';

/*
*  if user is logged in then show right panel
*/
if($this->session->userdata('user_id')!='' && !strstr($_SERVER['REQUEST_URI'], 'complete_registration')){
    $this->load->view('right-content-view', $data);
}

/**
  * Include footer view
  *
  */
$this->load->view('include/footer');

?>