<?php
// ****************************************************************************
// 
//     Registration page view 
//
// ****************************************************************************
?>

<!--<div class="signup_wrap">-->

<!-- Simple AnythingSlider -->

<!-- <ul id="slider">

        <li><img src="/images/slide-civil-1.jpg" alt=""></li>

        <li><img src="/images/slide-env-1.jpg" alt=""></li>

        <li><img src="/images/slide-civil-2.jpg" alt=""></li>

        <li><img src="/images/slide-env-2.jpg" alt=""></li>

</ul>  -->

<!-- END AnythingSlider -->
<?php echo @$activate_msg; ?>
   <!-- <a href="javascript:void(0)" onclick="openDiv('formdiv');" class="singup_link">Signup</a>-->
   
   <div id="container_intro">
      
      
         <div class="center">
            &nbsp;&nbsp;&nbsp;<span id="introheading" class="hc">Organize Files</span><br>
            <img id="introimage" alt="" src="<?=base_url(); ?>images/intro_org-files.jpg">
         </div>
         
         <div class="intro_left">
            <div id="intro1">
                <span class="hw">Todd Shepherd</span><br>
                <span class="hg">CTO</span><br>
                <span class="hc">Studeo</span><br><br>          
                <img alt="" src="<?=base_url(); ?>images/partner_pic3.jpg" width="100" height="100"><br><br>
                <div class="intro_left_text">
                    "These online tools have allowed us a quick and effecient way to impress our clients."
                </div>
            </div>
             
             <div id="intro2" style="display: none">
                <span class="hw">Richard Kerris</span><br>
                <span class="hg">CTO</span><br>
                <span class="hc">Lucas Film</span><br><br>          
                <img alt="" src="<?=base_url(); ?>images/logo_lucas-film.jpg" width="100" height="100"><br><br>
                <div class="intro_left_text">
                    "This is how online production should look and work. It was about time!"
                </div>
            </div>
             
             <div id="intro3" style="display: none">
                <span class="hw">Matt Cupal</span><br>
                <span class="hg">President</span><br>
                <span class="hc">Sorenson Media</span><br><br>          
                <img alt="" src="<?=base_url(); ?>images/logo_sorenson-media.jpg" width="100" height="100"><br><br>
                <div class="intro_left_text">
                    "A perfect example of how our video technologies are put to use in an innovative manner"
                </div>
            </div>
             
             <div id="intro4" style="display: none">
                <span class="hw">Todd Rundgren</span><br>
                <span class="hg">Musician | Producer</span><br>
                <span class="hc">Panacea</span><br><br>          
                <img alt="" src="<?=base_url(); ?>images/logo_todd-rundgren.jpg" width="100" height="100"><br><br>
                <div class="intro_left_text">
                    "Fancy shmancy online stuff. But ya know what? This stuff just works and looks cool"
                </div>
            </div>
             
             <div id="intro5" style="display: none">
                <span class="hw">Jefferson Coombs</span><br>
                <span class="hg">Senior VP</span><br>
                <span class="hc">Campbell & Ewald</span><br><br>          
                <img alt="" src="<?=base_url(); ?>images/logo_campbell-ewald.jpg" width="100" height="100"><br><br>
                <div class="intro_left_text">
                    "As an Ad Agency we constantly have to show what we do and look good. This helps us do it."
                </div>
            </div>
               			
         </div>         
		 <div class="intro_right">
                    <a class="intro_thumb" id="img_1"><span>Manage Users</span><br><img alt="" src="<?=base_url(); ?>images/intro_man-users_thumb.jpg"></a>
                    <a class="intro_thumb" id="img_2"><span>Organize Images</span><br><img alt="" src="<?=base_url(); ?>images/intro_org-images_thumb.jpg"></a>
                    <a class="intro_thumb" id="img_3"><span>Stream Video</span><br><img alt="" src="<?=base_url(); ?>images/intro_stream-video_thumb.jpg"></a>
                    <a class="intro_thumb" id="img_4"><span>Manage Account</span><br><img alt="" src="<?=base_url(); ?>images/intro_man-account_thumb.jpg"></a>
                    <a href="#" class="link">&nbsp;</a>
                    <br class="clear"><br>
                    <h1 style="padding-top: -15px;">Welcome</h1>
                    <div class="largetext"><span class="hc">AVP</span>matrix is an online application for organizing and sharing Files, Images and Videos.<br><br>Whether you want to manage an entire production and present your work to a client, or whether you simply want to have a web-based portfolio of your data, <span class="hc">AVP</span>matrix is all you need.</div>
                </div>
		    
		    
		    
      </div>
   
   <!-- signup form overlay -->
    <div class="popup" id="formdiv" style="width: 640px;">
	<div class="popup_head">
		<span>Sign up</span>	
	</div>
	<div class="popup_top">
		<table cellspacing="0" cellpadding="0" border="0">
		<tbody><tr>
		<td width="35%" class="leftcolumn"><span class="hg">Plan:</span>
		</td>
		<td width="65%" class="rightcolumn">TRIAL
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">Traffic:</span>
		</td>
		<td width="65%" class="rightcolumn">1000 GB
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">Users:</span>
		</td>
		<td width="65%" class="rightcolumn">100
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">30 Day Trial:</span>
		</td>
		<td width="65%" class="rightcolumn"><span class="hc">Free</span>
		</td>
		</tr>
		</tbody></table>
		<div class="run">Fill out the form below to sign up for a trial account.</div>
		<div class="clear"></div> 
	</div>
	<div class="popup_forms">
        <?php 
            echo validation_errors('<p class="error">');
            $data = array('onsubmit' => "return chkSignup()", 'id'=>'signup_form');
            echo form_open(base_url("user/registration"), $data); ?>
            <div class="floatLeft">
                <?php
                echo form_label('First Name:', 'first_name'); 
                echo '<br>';
                $data = array(
                                'name'        => 'first_name',
                                'id'          => 'first_name',
                                'value'       => set_value('first_name'),
                                'class'       => 'smallinput',
                              );

                echo form_input($data);
                ?>
            </div>
            
            <div class="floatLeft"> 
            <?php
            echo form_label('Last Name:', 'last_name'); 
            echo '<br>';
            $data = array(
                            'name'        => 'last_name',
                            'id'          => 'last_name',
                            'value'       => set_value('last_name'),  
                            'class'       => 'smallinput',
                          );

            echo form_input($data);
            ?>
            </div>			
            <div class="clear">
                <?php
                    echo form_label('Company Name:', 'company_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'company',
                                    'id'          => 'company',
                                    'value'       => set_value('company'),                                        
                                  );

                    echo form_input($data);
                    echo '<br>';
                ?>
            </div>
        <br>
        <br>

        <?=form_label('eMail Address:', 'email_address'); ?>&nbsp;&nbsp;<span class="hg">(used as login name)</span><br>
        <?php
        $data = array(
                      'name'        => 'email_address',
                      'id'          => 'email_address',
                      'value'       => set_value('email_address'),   
                      'onchange'    => 'return chk_useremail()',
                    );

        echo form_input($data);
        ?>
        &nbsp; <span id="useremail_msg" style="display: none">Avaliable</span>
          <br>	
          <?php
              echo form_label('Password:', 'password'); 
              echo '<br>';
              $data = array(
                              'name'        => 'password',
                              'id'          => 'password',
                              'value'       => set_value('password'),                                        
                            );

              echo form_password($data);
              echo '<br>';

              echo form_label('Password:', 'con_password'); 
              echo '&nbsp;&nbsp;<span class="hg">(Re-Enter)</span><br>';
              $data = array(
                              'name'        => 'con_password',
                              'id'          => 'con_password',
                              'value'       => set_value('con_password'),                                        
                            );

              echo form_password($data);
              echo '<br>';

              echo form_label('Stage Name:', 'stage'); 
              echo '<br>';
              $data = array(
                              'name'        => 'stage',
                              'id'          => 'stage',
                              'value'       => set_value('stage'),                                        
                            );

              echo form_input($data);

              echo form_close(); ?>
            </div>
            <div class="popup_bottom">
                    <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('formdiv')">Cancel</a>
                    <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#signup_form').submit();">Submit</a>
                    <div class="clear">&nbsp;</div> 
            </div>
    </div>
<div class="intro_bottom">
    <table class="table_basic">
        <tbody>
            <tr>
                <th class="firstcol">Plan:</th>
                <th>TRIAL</th>
                <th>SMALL</th>
                <th>MEDIUM</th>
                <th>LARGE</th>
                <th>X-LARGE</th>
            </tr>
            <tr>
                <td class="firstcol">Traffic:</td>
                <td>1000 GB</td>
                <td>1 GB</td>
                <td>10 GB</td>
                <td>100 GB</td>
                <td>1000 GB</td>				
            </tr>
            <tr>
                <td class="firstcol">Users:</td>
                <td>100</td>
                <td>2</td>
                <td>5</td>
                <td>20</td>
                <td>100</td>				
            </tr>
            <tr>
                <td class="firstcol">Monthly:</td>
                <td><span class="hc">Free</span></td>
                <td>$ 000</td>
                <td>$ 000</td>
                <td>$ 000</td>
                <td>$ 000</td>				
            </tr>
            <tr>
                <td class="firstcol">&nbsp;</td>
                <td><a onfocus="this.blur();" href="javascript:void(0)" onclick="openDiv('formdiv');">Sign Up Now</a></td>
                <td>TBA</td>
                <td>TBA</td>
                <td>TBA</td>
                <td>TBA</td>				
            </tr>
        </tbody>
    </table>
</div>

<?php
//old html of signup form
/*
<div class="reg_form">
<div class="form_title">Sign Up</div>
<div class="form_sub_title">It's free and anyone can join</div>
<?php echo validation_errors('<p class="error">'); ?>
	<?php $data = array('onsubmit' => "return chkSignup()");
                echo form_open("user/registration", $data); ?>
                <p>
                    <?php
			echo form_label('User Name', 'user_name'); 
                        $data = array(
                                        'name'        => 'user_name',
                                        'id'          => 'user_name',
                                        'value'       => set_value('user_name'),
                                        'onchange'    => 'return chk_username()',
                                        
                                      );

                          echo form_input($data);
                        ?>
                    &nbsp;
                    <span id="username_msg"></span>
		</p>
		<p>
                    <?php
			echo form_label('First Name', 'first_name'); 
                        $data = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'       => set_value('first_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>  
                <p>
                    <?php
			echo form_label('Last Name', 'last_name'); 
                        $data = array(
                                        'name'        => 'last_name',
                                        'id'          => 'last_name',
                                        'value'       => set_value('last_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                
                <p>
                    <?php
			echo form_label('Company Name', 'company_name'); 
                        $data = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'       => set_value('company'),                                        
                                      );

                          echo form_input($data);
                        ?>
                </p>
                
                <p>
                    <?php
			echo form_label('Your Email', 'email_address'); 
                        $data = array(
                                        'name'        => 'email_address',
                                        'id'          => 'email_address',
                                        'value'       => set_value('email_address'),   
                                        'onchange'    => 'return chk_useremail()',
                                      );

                          echo form_input($data);
                        ?>
                    &nbsp; <span id="useremail_msg" style="display: none">Avaliable</span>
		</p>
		<p>
                    <?php
			echo form_label('Password', 'password'); 
                        $data = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'value'       => set_value('password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Confirm Password', 'con_password'); 
                        $data = array(
                                        'name'        => 'con_password',
                                        'id'          => 'con_password',
                                        'value'       => set_value('con_password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Stage Name', 'stage'); 
                        $data = array(
                                        'name'        => 'stage',
                                        'id'          => 'stage',
                                        'value'       => set_value('stage'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p> 
                
		<p>
                    <?php 
                            $data = array(
                                'name' => 'sbt',
                                'id' => 'sbt',
                                'value' => 'Submit',
                                'type' => 'submit',
                                'content' => 'Submit',
                                'class' => 'greenButton'
                            );

                            echo form_button($data); ?>
		</p>
	<?php echo form_close(); ?>
</div><!--<div class="reg_form">-->    
 * 
 */
?>
