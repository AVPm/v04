<?php
// ****************************************************************************
// 
//     User's project images view
//
// ****************************************************************************
session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
$userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            $albumInfoArr['description']=$value['description'];
        }
    }
}

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <title></title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/bootstrap.min.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/style.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/image_page.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/jquery.mCustomScrollbar.css" />
  <!--  <script src="/js/jquery-1.8.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="/js/jquery.sortable.js"></script>-->
    <script>
        window['flags'] = { upscale: <?=@$image_upscale; ?> };
        var curr_project='<?=@$projectId; ?>';
        var curr_album='<?=@$curr_albumid; ?>';
        var base_url='<?=base_url(); ?>';  
        var imageid='<?=@$imageid; ?>';
        
        var upload_url= "<?=base_url(); ?>project/album/imageupload";	// Relative to the SWF file
        var image_version="<?=@$imageversion; ?>";
        var imageid_version="<?=@$imageid ?>";
        //var album_id="<?=$curr_albumid; ?>";
        var pageAction='imageupload';
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="<?=base_url(); ?>/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfforcesize.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_004.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_002.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_003.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/script_supersized_mod.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/pixastic.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/common.js"></script>
    <script src="<?=base_url(); ?>/js/avp.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload = function()
        {
            CKEDITOR.replace( 'comment', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.replace( 'comment_edit', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.on('dialogDefinition', function( ev ){
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                if ( dialogName == 'link' ){
                    // Get a reference to the "Target" tab.
                            var targetTab = dialogDefinition.getContents( 'target' ); 
                            // Set the default value for the target field.
                            var targetField = targetTab.get( 'linkTargetType' );
                            targetField['default'] = '_blank';
                     }
            });
        }

        //////////end of validations////////////////
    </script>
    
    <style>
        .popup{display: none}
        .preloadedImages
        {
            width: 0px;
            height: 0px;
            display: inline;
        }
    </style>

</head>

<body>
    
    <!-- add album popup starts -->
    <?php
    if($userid>0):
    ?>
        <div class="popup" id="add_album" style="width: 640px;">
            <div class="popup_head">
                    <span>Add Album</span>	
            </div>
            <div id="adduserHtml">
                <div class="popup_top">
                        <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                        <div class="run">To add an Album you have to enter a Name in the form below. You can also supply a picture to have a thumbnail image for the Album.</div>
                        <div class="clear">&nbsp;</div> 
                        <div class="clear" id="userProjectmsg"></div>
                </div>
                <div class="popup_forms">
                    <form action="" enctype="multipart/form-data" method="post" name="addalbum" id="addalbum">
                     <?php
                        /*
                         * hidden field with value of project id
                         */
                            $data = array(
                            'name'        => 'projectid_hidden',
                            'id'          => 'projectid_hidden',
                            'value'       => $projectId,
                            'type'        => 'hidden',
                            );

                            echo form_input($data);

                            echo form_label('Album Name: ', 'album_name'); 
                            echo '<br>';
                            $data = array(
                                            'name'        => 'album_name',
                                            'id'          => 'album_name',
                                          );
                            echo form_input($data);

                            echo '<br>';

                            echo form_label('Album Description: ', 'album_info'); 
                            echo '<br>';
                            $data = array(
                                            'name'        => 'album_info',
                                            'id'          => 'album_info',
                                          );
                            echo form_input($data);
                            ?>
                            <br>
                            Thumbnail:<br>
                            <div style="position:relative; width:310px; height:19px;">
                                            <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                                    <input type="file" onchange="javascript:copyField();" ondblclick="javascript:deleteField();" size="30" name="thumbnail" id="fileselecter" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                            </div>
                                <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                                    <input type="text" style="width:281px; float:left;" id="fakefilefield"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                                </div>
                            </div>
                            <br>Upscale Images:<br><input type="checkbox" onfocus="this.blur();" value="1" name="upscale" class="checkbox"><br>		
                            <?php /* Thumbnail Type: <br><input type="radio" onfocus="this.blur();" value="png" name="thumbnail_type" class="radio_thumbnailtype" checked="checked">PNG &nbsp; <input type="radio" onfocus="this.blur();" value="jpg" name="thumbnail_type" class="radio_thumbnailtype">JPG<br>*/ ?>
                    </form>

                </div>
            </div>
            <div class="popup_bottom">
                    <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('add_album')">Cancel</a>
                    <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#addalbum').submit();">Submit</a>
                    <div class="clear">&nbsp;</div> 
            </div>		
        </div>
    <?php
    endif;
    ?>
    <!-- add album popup ends -->
    
    
    <!-- edit album popup starts -->
    <?php
    if(!empty($albumInfoArr) && $userid>0){
    ?>
    <div class="popup" id="edit_album_form" style="width: 640px;">
        <div class="popup_head">
            <span>Edit Album</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'images')">Remove Album</a>
        </div>
            <div class="popup_top_project">
                <?php $thumbnail=($albumInfoArr['thumbnail']!='') ? base_url().'uploads/'.$albumInfoArr['thumbnail'] : base_url().'images/imageicon.png'; ?>
                    <img alt="" src="<?=$thumbnail;?>" width="160" height="160">
                    <div class="run"></div>
                    <div class="clear">&nbsp;</div> 
            </div>
            <div class="popup_forms">
                <?php

                    echo form_open_multipart("", array('id'=>'editalbum'));
                    /*
                     * hidden field with value of project id
                     */
                    $data = array(
                    'name'        => 'album_hidden',
                    'id'          => 'album_hidden',
                    'value'       => $curr_albumid,
                    'type'        => 'hidden',
                    );

                    echo form_input($data);
                    
                    /*
                     * input field for album name
                     */
                    echo form_label('Album Name: ', 'edit_album_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'edit_album_name',
                                    'id'          => 'edit_album_name',
                                    'value'       => $albumInfoArr['name'],
                                  );
                    echo form_input($data);

                    echo '<br>';
                    
                    /*
                     * input field for album description
                     */
                    echo form_label('Album Description: ', 'edit_album_description'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'edit_album_description',
                                    'id'          => 'edit_album_description',
                                    'value'       => $albumInfoArr['description'],
                                  );
                    echo form_input($data);

                    echo '<br>';

                    /*
                     * hidden field with value of project id
                     */
                    $data = array(
                    'name'        => 'album_thumbnail_hidden',
                    'id'          => 'album_thumbnail_hidden',
                    'value'       => $albumInfoArr['thumbnail'],
                    'type'        => 'hidden',
                    );

                    echo form_input($data);

                ?>
                Thumbnail:<br>
            <div style="position:relative; width:310px; height:19px;">
                <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                        <input type="file" size="30" name="edit_thumbnail" id="fileselecters" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                </div>
                <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                    <input type="text" style="width:281px; float:left;" id="fakefilefield" value="<?=$albumInfoArr['thumbnail']; ?>"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                </div>
            </div>
            <br>

                    remove current Folder Thumbnail:<br>
                    <?php
                    $data = array(
                        'name'        => 'deletethumb',
                        'id'          => 'deletethumb',
                        'value'       => 'delete',
                        'class'       => 'checkbox'
                        );

                    echo form_checkbox($data);
                    ?>
                Upscale Images:<br>
                    <?php
                   // $upscaleChecked=($image_upscale=='true') ? 'checked' : '';
                    $data = array(
                        'name'        => 'upscaleimages',
                        'id'          => 'upscaleimages',
                        'value'       => '1',
                        'class'       => 'checkbox'
                        );
                    
                    if($image_upscale=='true')
                        $data['checked']='checked';

                    echo form_checkbox($data);
                    /*
                    $ext='';
                    $chkpng='';
                    $chkjpg='';
                    if($albumInfoArr['thumbnail']!=''){
                        $arr=pathinfo($albumInfoArr['thumbnail']);
                        $ext=$arr['extension'];
                    }
                    if($ext=='png'){ $chkpng='checked="checked"'; }
                    else if($ext=='jpg'){$chkjpg='checked="checked"'; }
                    ?>
                Thumbnail Type: <br><input type="radio" onfocus="this.blur();" value="png" name="thumbnail_type" class="radio_thumbnailtype" <?=$chkpng; ?>>PNG &nbsp; <input type="radio" onfocus="this.blur();" value="jpg" name="thumbnail_type" class="radio_thumbnailtype" <?=$chkjpg; ?>>JPG<br>		    
                     * <?php 
                     */
                    ?>
            <?php echo form_close(); ?>

            </div>
            <div class="popup_bottom">
                <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_album_form')">Cancel</a>
                <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#editalbum').submit();">Submit</a>
                <div class="clear">&nbsp;</div> 
            </div>		
    </div>
    <?php
    }
    ?>
    <!-- edit album popup ends -->


    <!-- add images in album popup start -->
    <?php
    if($userid>0):
    ?>
        <div class="popup" id="add_image_popup" style="width: 820px;">
            <?php
                if($imageversion=='yes'){
                    $versionClass='on';
                    $imageClass='off';
                }
                else{
                    $versionClass='off';
                    $imageClass='on';
                }
            ?>
        <div class="popup_head">
            <a class="<?=$imageClass;?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>/addimage" id="image_only">Add Images</a>
            <a class="<?=$versionClass;?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>/imageversion" onclick="add_imageversion()" id="imageversion">Add Imageversions</a>  
                </div>	
            <?php
            /*
                <div class="popup_top">
                        <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                        <div class="run">Upload Images. First browse for your image files, then start upload.</div>
                        <div class="clear">&nbsp;</div> 
                </div>
             * 
             */
            ?>
            <div class="popup_forms" style="padding: 0px">
                <form action="" enctype="multipart/form-data" method="post" id="form1">
                    <input type="hidden" name="image_version" id="image_version" value="no" />
                    <input type="hidden" name="imageid_version" id="imageid_version" value="<?=$imageid; ?>" />
                    <?php
                    /*
                <div id="queue-status">
                    <div id="spanButtonPlaceHolderwrapper"><span id="spanButtonPlaceHolder"></span></div>
                    <span id="divStatus" style="display:none;">0 Files Uploaded</span> 
                </div>
                 <div class="controls">
                        <input type="button" onclick="swfu.startUpload();" value="Start upload" id="btnStart" class="controlbuttons" onfocus="this.blur();">
                        <input type="button" onclick="swfu.cancelQueue();" value="Cancel all uploads" id="btnCancel" class="controlbuttons" onfocus="this.blur();">
                </div>
        <div id="fsUploadProgress" class="fieldset flash">
                        <h3>File Queue</h3>
                        <span id="queue-is-empty">currently empty</span>
                </div> 
                    
                     * 
                     */
                    
                    ?>
                <div id="fileuploader">
                    <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                </div>    
                
        <?php
        /*
        <script type="text/javascript">	
        var swfu;

        var settings = {
                flash_url : "<?=base_url(); ?>includes/swfupload.swf",
                upload_url: "<?=base_url(); ?>project/album/imageupload",	// Relative to the SWF file
                post_params: {"PHPSESSID" : "<?=$sessionid; ?>", "album_id":"<?=$curr_albumid; ?>", "image_version":"<?=$imageversion; ?>", "imageid_version":"<?=$imageid ?>"},
                file_size_limit : "999 MB",
                file_types : "*.jpg;*.jpeg;*.gif;*.png",
                file_types_description : "Image Files",
                file_upload_limit : 999,
                file_queue_limit : 0,
                file_post_name : "file", 
                custom_settings : {
                        progressTarget : "fsUploadProgress",
                        cancelButtonId : "btnCancel",
                        startButtonId : "btnStart"
                },
                debug: false,				

                // Button settings
                button_image_url: "<?=base_url(); ?>images/folder4x.png",	// Relative to the Flash file
                button_width: 150,
                button_height: 19,
                button_placeholder_id: "spanButtonPlaceHolder",
                button_text_left_padding : 30, 
                button_text : "<span class=\"whitetext\">Browse for Files<\/span>",
                button_text_style : ".whitetext { color: #FFFFFF;font-family:Arial, Helvetica, sans-serif; font-size:12px;}", 
                button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT, 

                // The event handler functions are defined in handlers.js
                file_queued_handler : fileQueued,
                file_queue_error_handler : fileQueueError,
                file_dialog_complete_handler : fileDialogComplete,
                upload_start_handler : uploadStart,
                upload_progress_handler : uploadProgress,
                upload_error_handler : uploadError,
                upload_success_handler : uploadSuccess,
                upload_complete_handler : uploadComplete,
                queue_complete_handler : queueComplete	// Queue plugin event
        };
        swfu = new SWFUpload(settings);
        </script>
         * 
         */
        ?>
                        </form>

                </div>
                <div class="popup_bottom">
                    <a onclick="window.location.href='/project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>';" href="javascript:void(0)" onfocus="this.blur();" class="but_red floatLeft">cancel</a>
                    <a id="donelink" style="display: none" class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="window.location.href='<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>';">done</a>
                        <div class="clear">&nbsp;</div> 
                </div> 
        </div>
    <?php
    endif;
    ?>
    <!-- add images in album popup ends -->
    
    <!-- edit album's images popup -->
    <?php
        if($imageid>0 && $userid>0){
    ?>
            <div class="popup" id="edit_album_image" style="width: 640px;">
                <div class="popup_head">    
                    <span>Edit Image</span>
                    <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage('images', '<?=$imageid; ?>')">Delete Image</a>
                </div>
                <div class="popup_top">
                    <img alt="" src="/uploads/<?=$image_thumb?>" title="<?=$image_title; ?>" width="98" height="98">
                    <div class="run">Edit an Image.</div>
                    <div class="clear">&nbsp;</div> 
                </div>
                <div class="popup_forms">
                    <?php 
                        echo form_open_multipart("", array('id'=>'editimage'));
                        
                        $data = array(
                                    'name'        => 'album_imageid_hidden',
                                    'id'          => 'album_imageid_hidden',
                                    'value'       => $imageid,
                                    'type'        => 'hidden',
                                    );

                        echo form_input($data);
                
                        echo form_label('Title: ', 'image_title'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'image_title',
                                        'id'          => 'image_title',
                                        'value'       => $image_title,
                                      );
                        echo form_input($data);

                        echo '<br>';

                        echo form_label('additional Info: ', 'additional_info'); 
                        $data = array(
                                'name'        => 'additional_info',
                                'id'          => 'additional_info',
                                'rows'        => '7',
                                'cols'        => '36',
                                'value'       => $image_info
                              );

                            echo form_textarea($data);
                    ?>
                    <br><br>  
                    <?php echo form_close(); ?>
                </div>
                <div class="popup_bottom">
                        <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('edit_album_image')">cancel</a>
                        <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editimage').submit();">submit</a>
                        <div class="clear">&nbsp;</div> 
                </div>

            </div>
    <?php } ?>

    <!-- add comment form -->
    <?php
    if($userid>0):
    ?>
        <div class="popup" id="addalbumCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Add Comment</span>    		
	</div>
        <?php
        /*
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Add a Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
         * 
         */
        ?>
	<div class="popup_forms" style="padding: 0;">
        <?php
            echo form_open("", array('id'=>'addcommentfrm')); 
            
            $data = array(
                        'name'        => 'album_comment_hidden',
                        'id'          => 'album_comment_hidden',
                        //'value'       => $curr_albumid,
                        'value'       => $imageid,
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $user_stagename,
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
           // echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment',
                    'id'          => 'comment',
                    'rows'        => '7',
                    'cols'        => '36',
                  );

            echo form_textarea($data);
            
            echo form_close(); 
        ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addalbumCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#addcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    <?php
    endif;
    ?>
    <!-- add comment form ends -->
    
    <!-- edit comment form -->
    <?php
    //@$lastCommentArr=$album_comments[0];
    $lastCommentArr= array();
    foreach ($album_comments as $key => $value) {
        if($imageid==$key){
            $lastCommentArr=$value[0];
            break;
        }
    }
    
    if($userid>0 && !empty($lastCommentArr)):
    
    ?>
    <div class="popup" id="editCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Edit Comment</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removecomment('images',<?=@$lastCommentArr['id']; ?>)">Remove Comment</a>    		
	</div>
        <?php
        /*
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Edit your last Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
         * 
         */
        ?>
	<div class="popup_forms" style="padding: 0;">
        <?php
        
            echo form_open("", array('id'=>'editcommentfrm'));
            $data = array(
                        'name'        => 'commentid_hidden',
                        'id'          => 'commentid_hidden',
                        'value'       => @$lastCommentArr['id'],
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => @$lastCommentArr['stage_name'],
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
           // echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment_edit',
                    'id'          => 'comment_edit',
                    'rows'        => '7',
                    'cols'        => '36',
                    'value'       => @$lastCommentArr['comment']
                  );

            echo form_textarea($data);
            
            echo form_close(); 
            ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('editCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    
    <?php
    endif;
    ?>
    <!-- edit comment forms ends -->
    
    <!-- album sorting form start -->
    <?php
    if($userid>0):
    ?>
        <div class="popup" id="sorting_album" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Albums</span>
        </div>
              <div id="containmentbox" class="sort_area">
               <ul id="sortbox" class="sortable ui-sortable" style="">
                   <?php
                   foreach ($albumsArr as $album){
                        $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        echo '<li id="id_'.$album['id'].'" style="">
                                <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$album['name'].'</div>
                            </li>';
                   }
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            
         var resultnumber = <?=count($albumsArr); ?>;
         /*
         $('#sortbox').sortable({
            revert: 100,
            containment: $('#containmentbox')
         });
        */

         
        </script>	
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('images');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php
    endif;
    ?>
    <!-- album sorting form ends -->
    
    
    
    <!-- album images sorting form start -->
    <?php
    if($userid>0):
    ?>
    <div class="popup" id="sorting_album_images" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Images</span>
        </div>
              <div id="containmentbox_album_images" class="sort_area">
               <ul id="sortbox_album_images" class="sortable ui-sortable" style="">
                   <?php
                    foreach ($album_images as $albumImg){
                                
                    if($albumImg['image_version']==0):
                        $title=($albumImg['title']!='') ? $albumImg['title'] : 'no title';
                        echo '<li id="id_'.$albumImg['id'].'" style="">
                                <img width="160" height="160" class="selector" src="'.base_url().'uploads/'.$albumImg['thumbnail'].'">
                                <div class="album_name">'.$title.'</div>
                            </li>';
                    endif;
                   }
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        <script type="text/javascript">
         var resultnumber = <?=count($albumsArr); ?>;
         /*
         $('#sortbox_album_images').sortable({
            revert: 100,
            containment: $('#containmentbox_album_images')
         });
         */

         
        </script>	
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album_images')">cancel</a>
                  <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="saveAlbumImagesOrder();">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php
    endif;
    ?>
    <!-- album images sorting form ends -->
    
    <div id="top_header">	
            <div>
                <a href="<?=base_url(); ?>"><img src="<?=base_url(); ?>images/transparent.gif" style="height:33px; width:100px;"></a>
                <div class="topnav">
<!--                    <a class="on" href="http://v04.avpmatrix.com/" onfocus="this.blur();">Dashboard</a>-->
                    <?php $classDash=(!isset($selected_menu)) ? 'on' : ''; 
                    if($userid>0){
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Dashboard</a>';
                    }
                    else{
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Home</a>';
                    }
                    
                        if(isset($top_menu)){
                            foreach ($top_menu as $link=>$menu) {
                                $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                                echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">
                                '.$menu.'</a>';
                            }
                        }
                    ?>                  
                </div>
                <?php
                    $remove_array=$this->session->userdata('last_remove');
                    if(is_array($remove_array)){ ?>
                        <div class="undobutton"><a class="blink_me" href="<?=base_url('project/lastundo'); ?>">Undo</a></div>
                <?php } ?>
            </div>
            
            <div class="topform">
              <form action="" method="post">
                <img width="110" height="24" src="<?=base_url(); ?>images/project_id.gif" style="margin:2px 0 0 0; float:left;"><input type="text" class="top_id_input" size="18" name="identifier_project" value="">
              </form>
            </div>
        </div>
    <div id="wrapper">
        <div id="image_content">
            <div class="divbar" id="divbarid"></div>
            <div id="outerCenterImg">
                <table style="width:100%">
                    <tr>
                        <td valign="middle" align="center">
                            <div id="centerLage_image">
                                <?php
                                    if(isset($largeImgArr['fullimage']) && $largeImgArr['fullimage']!=''){
                                      //  echo '<img src="/uploads/'.$largeImgArr['fullimage'].'" style="visibility:hidden; width:24px; height:24px;" id="large_img">';
                                        echo '<script> var selectedImage ="'.base_url().'uploads/'.$largeImgArr['fullimage'].'";</script>';
                                        /*
                                         * echo '<script>setTimeout(placePlayer("/uploads/'.$largeImgArr['fullimage'].'"), 1000);</script>';
                                         */


                                    }
                                    else{
                                        echo '<strong>&nbsp;</strong>';
                                    }
                                        //echo '<img src="'.base_url().'uploads/'.$largeImgArr['fullimage'].'" id="imagescaler" >';
                                    ?>
                            </div>
                        </td>
                    </tr>
                </table>
                
            </div>
            <div id="divbarbottomid" class="divbarbottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                <?php if($userid>0):  ?>
                    <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_image_popup');">&nbsp;</a>
                    <?php if($imageid>0){  ?>
                    <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_image');">&nbsp;</a>
                    <a class="sortbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('sorting_album_images')" >&nbsp;</a>  
                    <?php } 
                endif;
                ?>
                <?php } ?>
            </div>
        </div>
        <div id="rightcontent">
            
            <div class="divbar" id="album_top">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0):  ?>
                        <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_album');">&nbsp;</a>
                        <?php if($curr_albumid>0){ ?>
                        <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_form')">&nbsp;</a>
                        <a class="sortbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('sorting_album')">&nbsp;</a>
                        <?php } 
                    endif;
                    ?>
                <?php } ?>
            </div>
            
            <div id="centerlisting_albums">
                <?php
                if(!empty($albumsArr)){
                    $i=1;
                    $j=1;
                    foreach ($albumsArr as $album){
                        $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkon' : 'linkoff';
                        $i= ($i>2) ? 1 : $i;
                       
                        if( strlen($j)==1 )
                         {
                         $count='0'.$j;
                         }
                         elseif( strlen($j)>=2 )
                         {
                         $count=$j;
                         }
                        
                    ?>
                <div class="albumcontainer<?=$i;?>" id="sidenavi_<?=$album['id']; ?>" onclick="window.location.href='<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>'">


                        <div class="ac1">

                          <div class="ac1num">
                            <?=$count; ?>                          
                          </div>
                          <div class="ac1name">
                            <a onfocus="this.blur();" class="<?=$classname; ?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>"><?=$album['name']; ?></a>
                          </div>
                          <div class="ac1version">

                          </div>
                        </div>

                        <div class="ac2">
                          Album
                          <br>
                          Name
                        </div>
                        <?php
                            $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        ?>
                        <div class="ac3">
                          <a onfocus="this.blur();" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>">
                            <img width="98" height="98" border="0" alt="" src="<?=$thumbnail;?>">
                          </a>
                        </div>


                      </div>
                    <?php
                        $i++;
                        $j++;
                        }
                    }
                    ?>
            </div>
            <div class="divbar" id="album_bottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0 && $imageid>0): ?>
                    <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('addalbumCommentDiv')">&nbsp;</a>
                    <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('editCommentDiv')">&nbsp;</a>
                    <?php endif; ?>
                <?php } ?>
            </div>
        </div>
        
        <div id="bottomDiv">
            <div id="images" style="overflow:hidden">
                <div id="album_images">
                    <?php
                    if($userid==2){
                       // echo '<pre>'; print_r($album_images); die;
                    }
                        if(!empty($album_images)){
                            $i=1;
                            $j=1;
                            $imageversionidArr=  array();
                            $imageidArr=array();
                            foreach ($album_images_versions as $key => $value) {
                                array_push($imageversionidArr, $value['image_version_id']);
                                array_push($imageidArr, $value['image_id']);
                            }
//                            echo '<pre>';
//                            print_r($imageversionidArr);
//                            die('s');
                            foreach ($album_images as $albumImg){
                                
                                if($albumImg['image_version']==0):
                                
                                    $classname=($imageid==$albumImg['id'] && $imageid>0) ? 'linkon' : 'linkoff';
                                    $i= ($i>2) ? 1 : $i;
                                    if( strlen($j)==1 ){
                                        $count='00'.$j;
                                    }
                                    elseif( strlen($j)==2 ){
                                        $count='0'.$j;
                                    }
                                    elseif( strlen($j)>=3 ){
                                        $count=$j;
                                    }
                            ?>
                        <div id="bottom_<?=$albumImg['id']; ?>" class="imagecontainer">
                            <div style="position:relative; width:160px;">
                             <div style="position:absolute" class="preloaderholder"></div> 
                                 <a href="<?=base_url(); ?>project/images/<?=$projectid; ?>/<?=$albumid ?>/<?=$albumImg['id']; ?>" onfocus="this.blur();" class="bottomlinks" id="link<?=$albumImg['id']; ?>">
                                     <img onmouseover="showComments(<?=$albumImg['id']; ?>)" height="160" border="0" id="medium3648" title="<?=$albumImg['title']; ?>" alt="" src="<?=base_url(); ?>uploads/<?=$albumImg['thumbnail']; ?>" />
                                 </a> 
                             &nbsp;<a id="textlink<?=$albumImg['id']; ?>" class="<?=$classname; ?>" href="<?=base_url(); ?>project/images/<?=$projectid; ?>/<?=$albumid ?>/<?=$albumImg['id']; ?>" style="margin-right: -3px;" onmouseover="showComments(<?=$albumImg['id']; ?>); showlargeImg(this.id);" onmouseout="placePlayer(selectedImage);" alt="<?=$albumImg['full_image']; ?>"><?=$count; ?></a>
                                 <?php
                                 echo '<div class="preloadedImages" style="background-image:url('.base_url('uploads/'.$albumImg['full_image']).')"></div>';
                                $k=2;
                                //if($albumImg['id']==$album_images_versions)
                                foreach ($album_images_versions as $key => $value) {
                                    $versionsclassname=($imageid==$value['image_id'] && $imageid>0) ? 'linkon' : 'linkoff';
                                    //if($imageid==$value['image_version_id'] && $imageid>0){
                                    //if(in_array($imageid, $imageversionidArr) || $imageid==$value['image_id']){
                                        $versionlink=base_url().'project/images/'.$projectid.'/'.$albumid.'/'.$value['image_id'].'" id="textlink'.$value['image_id'].'" alt="'.$value['full_image'];
                                        $versionImgHover='onmouseover="showlargeImg(this.id); showComments('.$value['image_id'].')"  onmouseout="placePlayer(selectedImage);"';
                                        echo '<div class="preloadedImages" style="background-image:url('.base_url('uploads/'.$value['full_image']).')"></div>';
                                   // }
                                   // else{
                                   //     $versionlink='javascript:void(0)';
                                   //     $versionImgHover='';
                                   // }
                                    if( strlen($k)==1 ){
                                        $countk='0'.$k;
                                    }
                                    else {
                                        $countk=$k;
                                    }
                                    if($albumImg['id']==$value['image_version_id']){
                                        echo '<span class="hg">|</span>';
                                        echo '<a class="'.$versionsclassname.'" href="'.$versionlink.'" '.$versionImgHover.'>.'.$countk.'</a>';
                                        $k++;
                                    }
                                }
                             ?>
                             
                             <?php
                                if($j==1){
                             ?>
                                <script type="text/javascript">
                                <?php //echo 'var selectedImage ='.$largeImage ?>
                                $(document).ready(function(){
                                 //alert($("#medium'.$id.'")[0].complete);
                                 //default image preloader
                                 jQuery("<img>").attr("src", selectedImage).load(function(){
                                   placePlayer(selectedImage);
                                 }).each(function() {
                                    if(this.complete) $(this).trigger("load");
                                 });             

                                });                              
                                </script>

                             <?php
                                }
                             ?>


                           </div>
                        </div>
                        <?php
                            $i++;
                            $j++;
                        endif;
                        }
                        
                    }
                    else{
                        echo '<div>&nbsp;</div>';
                    }
                        ?>
                    
                </div>
                
                
                
                
            </div>
            <div id="right_comments">
                <?php
                /*
                <div class="commenttitle">comments for album <?=@$albumInfoArr['name']; ?></div>
                 * 
                 */
                ?>
                <div class="commenttitle">comments for album image</div>
                <?php
                $i=1;
                $html='';
                foreach ($album_comments as $key => $value) {
                    $display=($imageid==$key) ? 'display:block' : '';
                    $html.='<div id="commentsVideo'.$key.'" class="showCommentsVideo" style="'.$display.'">';
                    $j=1;
                    foreach ($value as $arrkey => $arrvalue) {
                        if( strlen($j)==1 ){
                            $count='0'.$j;
                        }
                        else{
                            $count=$j;
                        }
                        $html.= '<div class="commentnormal">
                                    <div class="commentnames">'.$count.' - '.$arrvalue['stage_name'].' ('.date('d.m.y, h:i A', strtotime($arrvalue['added_date'])).') :</div>
                                    <div class="commentmessage"> '.$arrvalue['comment'].'</div>
                                </div>';
                        $j++;
                    }
                    $html.='</div>';
                    
                }
                
                echo $html;
                
                /*
                $j=1;
                if($album_comments!=''){
                    foreach ($album_comments as $key => $value) {
                    if( strlen($j)==1 ){
                        $count='0'.$j;
                    }
                    else{
                        $count=$j;
                    }
                    echo '<div class="commentnormal">
                                <div class="commentnames">'.$count.' - '.$value['stage_name'].' ('.date('d.m.y, h:i A', strtotime($value['added_date'])).') :</div>
                                <div class="commentmessage"> '.$value['comment'].'</div>
                            </div>';
                    $j++;
                }
                }
                 * 
                 */
                ?>
            </div>
        </div>
    </div>
    
    

    <div id="cover" style="display: none; height: 100%; width: 100%;"></div>
    <script>
        <?php
            if($imageversion=='yes' || $imageversion=='addimage')
                echo "openDiv('add_image_popup');";
        ?>
            /*
        $(function() {
            $('.sortable').sortable();
           
        });
        */
        $(function() {
          $( ".sortable" ).sortable();
          $( ".sortable" ).disableSelection();
          
        });
        
        $(window).load(function(){
            if(curr_album>0)
                $("#centerlisting_albums").mCustomScrollbar("scrollTo","#sidenavi_"+curr_album);
            
            if(imageid>0)
                $("#images").mCustomScrollbar("scrollTo","#bottom_"+imageid);
            
            //$("#fileuploader").mCustomScrollbar();
        })
    </script>
    
    
</body>
</html>