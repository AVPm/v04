<?php
// ****************************************************************************
// 
//     User's project images view
//
// ****************************************************************************
session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
$userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            //$albumInfoArr['description']=$value['description'];
        }
    }
}

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <title></title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/bootstrap.min.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/style.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/image_page.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/jquery.mCustomScrollbar.css" />
    <script>
        var curr_project='<?=@$projectId; ?>';
        var curr_album='<?=@$curr_albumid; ?>';
        var base_url='<?=base_url(); ?>';  
        var imageid='<?=@$videoid; ?>';
        var pageAction='videoupload';
        var image_version="";
        var imageid_version="";
        var upload_url= "<?=base_url(); ?>project/album/videoupload";	// Relative to the SWF file
    </script>
  <!--  <script src="/js/jquery-1.8.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="/js/jquery.sortable.js"></script>-->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui.min.js"></script>
    <script src="<?=base_url(); ?>js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfforcesize.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_004.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_002.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_003.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>js/script_supersized_mod.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>js/pixastic.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>js/common.js"></script>
    <script src="<?=base_url(); ?>js/avp.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url(); ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload = function()
        {
            CKEDITOR.replace( 'comment', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.replace( 'comment_edit', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.on('dialogDefinition', function( ev ){
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                if ( dialogName == 'link' ){
                    // Get a reference to the "Target" tab.
                            var targetTab = dialogDefinition.getContents( 'target' ); 
                            // Set the default value for the target field.
                            var targetField = targetTab.get( 'linkTargetType' );
                            targetField['default'] = '_blank';
                     }
            });
        }

        //////////end of validations////////////////
    </script>
    
    <style>
        .popup{display: none}
    </style>

</head>

<body>
    
    <!-- add folder popup starts -->
    <?php
    if($userid>0){
    ?>
        <div class="popup" id="add_album" style="width: 640px;">
        <div class="popup_head">
                <span>Add Folder</span>	
        </div>
        <div id="adduserHtml">
            <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">To Add a Folder you have to enter a Name in the form below. You can also supply a picture to have a thumbnail image for the folder.</div>
                    <div class="clear">&nbsp;</div> 
                    <div class="clear" id="userProjectmsg"></div>
            </div>
            <div class="popup_forms">
                <form action="" enctype="multipart/form-data" method="post" name="addfolder" id="addfolder">
                 <?php
                    /*
                     * hidden field with value of project id
                     */
                        $data = array(
                        'name'        => 'projectid_hidden',
                        'id'          => 'projectid_hidden',
                        'value'       => $projectId,
                        'type'        => 'hidden',
                        );

                        echo form_input($data);

                        echo form_label('Folder Name: ', 'folder_name'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'folder_name',
                                        'id'          => 'folder_name',
                                      );
                        echo form_input($data);

                        ?>
                        <br>
                        Thumbnail:<br>
                        <div style="position:relative; width:310px; height:19px;">
                                        <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                                <input type="file" onchange="javascript:copyField();" ondblclick="javascript:deleteField();" size="30" name="thumbnail" id="fileselecter" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                        </div>
                            <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                                <input type="text" style="width:281px; float:left;" id="fakefilefield"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url('images/folder.png'); ?>">
                            </div>
                        </div>
                </form>

            </div>
        </div>
        <div class="popup_bottom">
            <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('add_album')">Cancel</a>
            <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#addfolder').submit();">Submit</a>
            <div class="clear">&nbsp;</div>
        </div>		
    </div>
    <?php
    }
    ?>
    <!-- add folder popup ends -->
    
    
    <!-- edit folder popup starts -->
    <?php
    if(!empty($albumInfoArr) && $userid>0){
    ?>
    <div class="popup" id="edit_album_form" style="width: 640px;">
        <div class="popup_head">
            <span>Edit Folder</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'videos')">Remove Folder</a>
        </div>
            <div class="popup_top_project">
                <?php $thumbnail=($albumInfoArr['thumbnail']!='') ? base_url().'uploads/'.$albumInfoArr['thumbnail'] : base_url().'images/imageicon.png'; ?>
                <img alt="" src="<?=$thumbnail;?>" width="160" height="160">
                    <div class="run"></div>
                    <div class="clear">&nbsp;</div> 
            </div>
            <div class="popup_forms">
                <?php

                    echo form_open_multipart("", array('id'=>'editalbum'));
                    /*
                     * hidden field with value of project id
                     */
                    $data = array(
                    'name'        => 'album_hidden',
                    'id'          => 'album_hidden',
                    'value'       => $curr_albumid,
                    'type'        => 'hidden',
                    );

                    echo form_input($data);
                    
                    /*
                     * input field for album name
                     */
                    echo form_label('Album Name: ', 'edit_album_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'edit_album_name',
                                    'id'          => 'edit_album_name',
                                    'value'       => $albumInfoArr['name'],
                                  );
                    echo form_input($data);

                    echo '<br>';
                    
                    
                    /*
                     * hidden field with value of project thumbnail
                     */
                    $data = array(
                    'name'        => 'album_thumbnail_hidden',
                    'id'          => 'album_thumbnail_hidden',
                    'value'       => $albumInfoArr['thumbnail'],
                    'type'        => 'hidden',
                    );

                    echo form_input($data);

                ?>
                Thumbnail:<br>
          <div style="position:relative; width:310px; height:19px;">
                <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                        <input type="file" size="30" name="edit_thumbnail" id="fileselecters" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                </div>
                <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                    <input type="text" style="width:281px; float:left;" id="fakefilefield" value="<?=$albumInfoArr['thumbnail']; ?>"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url('images/folder.png'); ?>">
                </div>
            </div>
            <br>

                    remove current Folder Thumbnail:<br>
                    <?php
                    $data = array(
                        'name'        => 'deletethumb',
                        'id'          => 'deletethumb',
                        'value'       => 'delete',
                        'class'       => 'checkbox'
                        );

                    echo form_checkbox($data);
                   
                    echo form_close(); ?>

            </div>
            <div class="popup_bottom">
                <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_album_form')">Cancel</a>
                <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#editalbum').submit();">Submit</a>
                <div class="clear">&nbsp;</div> 
            </div>		
    </div>
    <?php
    }
    ?>
    <!-- edit album popup ends -->

    <?php
    if($userid>0){
    ?>
    <!-- add video in album popup start -->
    <div class="popup" id="add_image_popup" style="width: 820px;">
        <?php
        /*
            if($imageversion=='yes'){
                $versionClass='on';
                $imageClass='off';
            }
            else{
                $versionClass='off';
                $imageClass='on';
            }
         * 
         */
        ?>
    <div class="popup_head">
        <a class="on" href="<?=base_url(); ?>project/videos/<?=$projectId; ?>///addimage" id="image_only">Add Videos</a>
            </div>	
        <?php
        /*
        <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">Upload Videos. First browse for your image files, then start upload.</div>
                    <div class="clear">&nbsp;</div> 
            </div>
         * 
         */
        ?>
            <div class="popup_forms" style="padding: 0px">
            <form action="" enctype="multipart/form-data" method="post" id="form1">
               <?php
               /*
                <input type="hidden" name="image_version" id="image_version" value="no" />
                <input type="hidden" name="imageid_version" id="imageid_version" value="" />
                -->
                <div id="queue-status">
                    <div id="spanButtonPlaceHolderwrapper"><span id="spanButtonPlaceHolder"></span></div>
                    <span id="divStatus" style="display:none;">0 Files Uploaded</span> 
                </div>

            <div class="controls">
                    <input type="button" onclick="swfu.startUpload();" value="Start upload" id="btnStart" class="controlbuttons" onfocus="this.blur();">
                    <input type="button" onclick="swfu.cancelQueue();" value="Cancel all uploads" id="btnCancel" class="controlbuttons" onfocus="this.blur();">
            </div>
    <div id="fsUploadProgress" class="fieldset flash">
                    <h3>File Queue</h3>
                    <span id="queue-is-empty">currently empty</span>
            </div>
                * 
                */
               ?>
                <div id="fileuploader">
                    <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                </div> 
<?php
/*
    <script type="text/javascript">	
    var swfu;

    var settings = {
            flash_url : "<?=base_url(); ?>includes/swfupload.swf",
            upload_url: "<?=base_url(); ?>project/album/videoupload",	// Relative to the SWF file
            post_params: {"PHPSESSID" : "<?=$sessionid; ?>", "album_id":"<?=$curr_albumid; ?>"},
            file_size_limit : "999 MB",
            file_types : "*.mp4;*.avi;*.flv;*.mpeg",
            file_types_description : "Video Files",
            file_upload_limit : 999,
            file_queue_limit : 0,
            file_post_name : "file", 
            custom_settings : {
                progressTarget : "fsUploadProgress",
                cancelButtonId : "btnCancel",
                startButtonId : "btnStart"
            },
            debug: false,				

            // Button settings
            button_image_url: "<?=base_url(); ?>images/folder4x.png",	// Relative to the Flash file
            button_width: 150,
            button_height: 19,
            button_placeholder_id: "spanButtonPlaceHolder",
            button_text_left_padding : 30, 
            button_text : "<span class=\"whitetext\">Browse for Files<\/span>",
            button_text_style : ".whitetext { color: #FFFFFF;font-family:Arial, Helvetica, sans-serif; font-size:12px;}", 
            button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT, 

            // The event handler functions are defined in handlers.js
            file_queued_handler : fileQueued,
            file_queue_error_handler : fileQueueError,
            file_dialog_complete_handler : fileDialogComplete,
            upload_start_handler : uploadStart,
            upload_progress_handler : uploadProgress,
            upload_error_handler : uploadError,
            upload_success_handler : uploadSuccess,
            upload_complete_handler : uploadComplete,
            queue_complete_handler : queueComplete	// Queue plugin event
    };
    swfu = new SWFUpload(settings);
    </script>
 * 
 */
?>
                    </form>

            </div>
            <div class="popup_bottom">
                <a onclick="window.location.href='/project/videos/<?=$projectId; ?>/<?=$curr_albumid; ?>';" href="javascript:void(0)" onfocus="this.blur();" class="but_red floatLeft">cancel</a>
                <a id="donelink" style="display: none" class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="window.location.href='<?=base_url(); ?>project/videos/<?=$projectId; ?>/<?=$curr_albumid; ?>';">done</a>
                    <div class="clear">&nbsp;</div> 
            </div> 
    </div>
    <!-- add video in album popup ends -->
    <?php
    }
    ?>
    
    <!-- edit album's images popup -->
    <?php
        if($videoid>0 && $userid>0){
    ?>
            <div class="popup" id="edit_album_image" style="width: 640px;">
                <div class="popup_head">    
                    <span>Edit Video</span>
                    <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage('videos', '<?=$videoid; ?>')">Delete Video</a>
                </div>
                <div class="popup_top">
                    <?php
                        $videothumb=($video_thumb=='') ? base_url().'/images/popup_pic1.jpg' : base_url().'/uploads/'.$video_thumb;
                        $videotitle=($video_title=='') ? $video_name : $video_title;
                    ?>
                    <img alt="" src="<?=$videothumb; ?>" title="<?=$video_title; ?>" width="98" height="98">
                    <div class="run">Edit an Video.</div>
                    <div class="clear">&nbsp;</div> 
                </div>
                <div class="popup_forms">
                    <?php 
                        echo form_open_multipart("", array('id'=>'editimage'));
                        
                        $data = array(
                                    'name'        => 'project_videoid_hidden',
                                    'id'          => 'project_videoid_hidden',
                                    'value'       => $videoid,
                                    'type'        => 'hidden',
                                    );

                        echo form_input($data);
                
                        echo form_label('Title: ', 'image_title'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'video_title',
                                        'id'          => 'video_title',
                                        'value'       => $videotitle,
                                      );
                        echo form_input($data);

                        echo '<br>';

                        echo form_label('additional Info: ', 'additional_info'); 
                        $data = array(
                                'name'        => 'additional_info',
                                'id'          => 'additional_info',
                                'rows'        => '7',
                                'cols'        => '36',
                                'value'       => $video_info
                              );

                        echo form_textarea($data);
                        
                        /*
                        * hidden field with value of project thumbnail
                        */
                       $data = array(
                       'name'        => 'video_thumbnail_hidden',
                       'id'          => 'video_thumbnail_hidden',
                       'value'       => $video_thumb,
                       'type'        => 'hidden',
                       );

                       echo form_input($data);
                        ?>    
                        Thumbnail:<br>
                        <div style="position:relative; width:310px; height:19px;">
                              <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                      <input type="file" size="30" name="edit_thumbnail" id="fileselectersvideothumb" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                              </div>
                              <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                                  <input type="text" style="width:281px; float:left;" id="fakefilefieldvideothumb" value="<?=$video_thumb; ?>"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url('images/folder.png'); ?>">
                              </div>
                        </div>
                        <br>

                    remove current Folder Thumbnail:<br>
                    <?php
                    $data = array(
                        'name'        => 'deletethumb',
                        'id'          => 'deletethumb',
                        'value'       => 'delete',
                        'class'       => 'checkbox'
                        );

                    echo form_checkbox($data);    
                    ?>
                    <br>
                    Show Vimeo Thumbnail:<br>
                    <?php
                    $data = array(
                        'name'        => 'vimeothumb',
                        'id'          => 'vimeothumb',
                        'value'       => 'vimeo',
                        'class'       => 'checkbox',
                        'checked'     => 'checked'
                        );

                    echo form_checkbox($data);    
                    ?>
                    <br><br>  
                    <?php echo form_close(); ?>
                </div>
                <div class="popup_bottom">
                        <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('edit_album_image')">cancel</a>
                        <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editimage').submit();">submit</a>
                        <div class="clear">&nbsp;</div> 
                </div>

            </div>
    <?php  } ?>

    <?php
    if($userid>0){
    ?>
    <!-- add comment form -->
    
    <div class="popup" id="addalbumCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Add Comment</span>    		
	</div>
        <!--
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Add a Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
        -->
	<div class="popup_forms" style="padding: 0;">
        <?php
            echo form_open("", array('id'=>'addcommentfrm')); 
            
            $data = array(
                        'name'        => 'album_comment_hidden',
                        'id'          => 'album_comment_hidden',
                        //'value'       => $curr_albumid,
                        'value'       =>$videoid,
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $user_stagename,
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            //echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment',
                    'id'          => 'comment',
                    'rows'        => '7',
                    'cols'        => '36',
                  );

            echo form_textarea($data);
            
            echo form_close(); 
        ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addalbumCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#addcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    <!-- add comment form ends -->
    <?php
    }
    ?>
    <!-- edit comment form -->
    <?php
    //$lastCommentArr=$album_comments[0];
    $lastCommentArr= array();
    foreach ($album_comments as $key => $value) {
        if($videoid==$key){
            $lastCommentArr=$value[0];
            break;
        }
    }
    
    if($userid>0 && !empty($lastCommentArr)){
    
    ?>
    <div class="popup" id="editCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Edit Comment</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removecomment('videos','<?=$lastCommentArr['id']; ?>')">Remove Comment</a>    		
	</div>
        <!--
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Edit your last Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
        -->
	<div class="popup_forms" style="padding: 0;">
        <?php
        
            echo form_open("", array('id'=>'editcommentfrm'));
            $data = array(
                        'name'        => 'commentid_hidden',
                        'id'          => 'commentid_hidden',
                        'value'       => $lastCommentArr['id'],
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $lastCommentArr['stage_name'],
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            //echo form_label('Comment: ', 'comment'); 
            
            $data = array(
                    'name'        => 'comment_edit',
                    'id'          => 'comment_edit',
                    'rows'        => '7',
                    'cols'        => '36',
                    'value'       => $lastCommentArr['comment']
                  );

            echo form_textarea($data);
            
            echo form_close(); 
            ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('editCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    <?php
    }
    ?>
    <!-- edit comment forms ends -->
    
    <!-- album sorting form start -->
    <?php
    if($userid>0){
    ?>
    <div class="popup" id="sorting_album" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Albums</span>
        </div>
              <div id="containmentbox" class="sort_area">
                <ul id="sortbox" class="sortable ui-sortable" style="">
                   <?php
                   
                   foreach ($albumsArr as $album){
                        $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        echo '<li id="id_'.$album['id'].'" style="">
                                <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$album['name'].'</div>
                            </li>';
                   }
                    
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('videos');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php
    }
    ?>
    <!-- album sorting form ends -->
    
        <!-- album files sorting form start -->
    <?php if($userid>0 && $videoid>0): ?>
        <div class="popup" id="sorting_album_files" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Albums</span>
        </div>
              <div id="containmentbox" class="sort_area">
               <ul id="sortbox" class="sortable ui-sortable" style="">
                   <?php
                   
                   foreach ($project_videos as $files){
                        $thumbnail=($files['thumbnail']!='') ? base_url().'uploads/'.$files['thumbnail'] : base_url().'images/popup_pic1.jpg';
                        echo '<li id="id_'.$files['id'].'" style="height:145px; width:120px;">
                                <img width="120" height="120" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$files['title'].'</div>
                            </li>';
                   }
                    
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album_files')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('user_videos');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php endif; ?>
    <!-- album files sorting form ends -->
    
    
    
    <div id="top_header">	
            <div>
                <a href="<?=base_url(); ?>"><img src="<?=base_url(); ?>/images/transparent.gif" style="height:33px; width:100px;"></a>
                <div class="topnav">
<!--                    <a class="on" href="http://v04.avpmatrix.com/" onfocus="this.blur();">Dashboard</a>-->
                    <?php $classDash=(!isset($selected_menu)) ? 'on' : ''; 
                    if($userid>0){
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Dashboard</a>';
                    }
                    else{
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Home</a>';
                    }
                    
                        if(isset($top_menu)){
                            foreach ($top_menu as $link=>$menu) {
                                $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                                echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">
                                '.$menu.'</a>';
                            }
                        }
                    ?>                  
                </div>
                <?php
                    $remove_array=$this->session->userdata('last_remove');
                    if(is_array($remove_array)){ ?>
                        <div class="undobutton"><a class="blink_me" href="<?=base_url('project/lastundo'); ?>">Undo</a></div>
                <?php } ?>
            </div>
            
            <div class="topform">
              <form action="" method="post">
                <img width="110" height="24" src="<?=base_url(); ?>images/project_id.gif" style="margin:2px 0 0 0; float:left;"><input type="text" class="top_id_input" size="18" name="identifier_project" value="">
              </form>
            </div>
        </div>
    <div id="wrapper">
        <div id="image_content">
            <div class="divbar" id="divbarid">
               
            </div>
            <div id="outerCenterImg">
                <table style="width:100%; height: 92%">
                    <tr>
                        <td valign="middle" align="center" height="100%">
                            
                            <?php
                            if($vimeo_id!=''){
                                $vimeovideolink = "https://player.vimeo.com/video/".$vimeo_id."?badge=0&portrait=0&byline=0&rel=0";
                            ?>
                                <iframe src="<?=$vimeovideolink;?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            <?php
                            }
                            else{
                                echo '<div id="flashvideoplayer"></div>';
                            }
                            ?>
                            
                            
                        </td>
                    </tr>
                </table>
                
            </div>
            <div id="divbarbottomid" class="divbarbottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                 <?php if($userid>0){ ?>
                <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_image_popup');">&nbsp;</a>
                <?php //if($imageid>0){  ?>
                <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_image');">&nbsp;</a>
                <a onclick="openDiv('sorting_album_files')" onfocus="this.blur();" href="javascript:void(0)" class="sortbt">&nbsp;</a>
                <?php } }?>
            </div>
        </div>
        <div id="rightcontent">
            <div class="divbar" id="album_top">
                <?php if($project_status!='viewer' && $project_status!='none'){  
                    if($userid>0){ ?>
                    <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_album');">&nbsp;</a>
                    <?php //if($curr_albumid>0){ ?>
                    <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_form')">&nbsp;</a>
                    <a class="sortbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('sorting_album')">&nbsp;</a>
                <?php } 
                }
                ?>
                    
            </div>
            <div id="centerlisting_albums">
                <?php
                if(!empty($albumsArr)){
                    $i=1;
                    $j=1;
                    foreach ($albumsArr as $album){
                        $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkon' : 'linkoff';
                        $i= ($i>2) ? 1 : $i;
                       
                        if( strlen($j)==1 )
                         {
                         $count='0'.$j;
                         }
                         elseif( strlen($j)>=2 )
                         {
                         $count=$j;
                         }
                        
                    ?>
                <div class="albumcontainer<?=$i;?>" id="sidenavi_<?=$album['id']; ?>" onclick="window.location.href='<?=base_url(); ?>/project/videos/<?=$projectId; ?>/<?=$album['id']; ?>'">


                        <div class="ac1">

                          <div class="ac1num">
                            <?=$count; ?>                          
                          </div>
                          <div class="ac1name">
                            <a onfocus="this.blur();" class="<?=$classname; ?>" href="<?=base_url(); ?>/project/videos/<?=$projectId; ?>/<?=$album['id']; ?>"><?=$album['name']; ?></a>
                          </div>
                          <div class="ac1version">

                          </div>
                        </div>

                        <div class="ac2">
                            <div>Album</div>
                          
                            <div style="margin-top: 4px;">Name</div>
                        </div>
                        <?php
                            $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        ?>
                        <div class="ac3">
                          <a onfocus="this.blur();" href="/project/videos/<?=$projectId; ?>/<?=$album['id']; ?>">
                            <img width="98" height="98" border="0" alt="" src="<?=$thumbnail;?>">
                          </a>
                        </div>


                      </div>
                    <?php
                        $i++;
                        $j++;
                        }
                    }
                    ?>
            </div>
            <div class="divbar" id="album_bottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                <?php if($userid>0 && $videoid>0){ ?>
                <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('addalbumCommentDiv')">&nbsp;</a>
                <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('editCommentDiv')">&nbsp;</a>
                <?php } }?>
            </div>
        </div>
        
        <div id="bottomDiv">
            <div id="videos" style="overflow:hidden">
                <?php
                    if(!empty($project_videos)){
                        //$videothumb=($video_thumb=='') ? base_url().'/images/popup_pic1.jpg' :  base_url().'uploads/'.$video_thumb;
                        $videothumb=($vimeo_thumbnail=='' && $video_thumb=='') ? base_url().'/images/popup_pic1.jpg' :  ($vimeo_thumbnail!='') ? $vimeo_thumbnail : base_url().'uploads/'.$video_thumb;
                ?>
                <div class="bottomside" id="videoThumbnail">
                    <img width="190" height="190" id="sideimage_3653" style="display:block;" class="buttomsideimage" src="<?=$videothumb;?>">
                </div>
                <?php
                    }
                ?>
                <div id="project_videos" style="height: 191px; width: 100%">
                <?php
                    if(!empty($project_videos)){
                ?>
                
                    <table class="pl">

                        <tbody>
                            <tr class="head">
                                <td class="pl_top"></td>                            
                                <td class="pl_top">
                                    <?php
                                        if($sortColumn=='name'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            //$classname='order_not';
                                            $classname='sort-desc';
                                            $style='';
                                        }
                                        //$sort=()
                                    ?>
                                    <a href="<?=base_url('project/videos/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/name/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Name</a>
                                </td>
                                <td class="pl_top">
                                    <?php
                                        if($sortColumn=='user'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/videos/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/user/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">User</a>
                                </td>
                                <td class="pl_top" style="width: 135px;">
                                    <?php
                                        if($sortColumn=='date'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/videos/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/date/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Date</a>
                                </td>
                                <td class="pl_top" style="width: 75px;">
                                    <?php
                                        if($sortColumn=='size'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-asc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-desc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/videos/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/size/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Size</a>
                                </td>
                                
                            </tr>
                            <?php
                            foreach ($project_videos as $videos) {
                                $videonamereplace=str_replace('_', ' ', $videos['title']);
                                $videoName=(strlen($videonamereplace)>100) ? substr($videonamereplace, 0, 40).'...' : $videonamereplace;
                                $classname=($videos['id']==$videoid) ? 'pl_conton' : 'pl_cont';
                                $filesize=$videos['size']/1024;
                                $filesize=round($filesize, 2);
                                $filesize=$filesize.'MB';
                                $classSelected=($videoid==$videos['id']) ? 'contentrow-selected' : '';
                                $videoThumbImg=($videos['thumbnail'] =='' && $videos['vimeo_thumbnail']=='') ? base_url().'/images/popup_pic1.jpg' :  ($videos['vimeo_thumbnail']!='') ? $videos['vimeo_thumbnail'] : base_url().'uploads/'.$videos['thumbnail'];
                            ?>
                            <tr class="contentrow <?=$classSelected; ?>" id="bottom_<?=$videos['id'] ?>" onmouseover="showComments(<?=$videos['id'] ?>)">
                                <td class="pl_cont1"><a onfocus="this.blur();" href="<?=base_url(); ?>project/download/video/<?=$videos['id'] ?>" class="pl_download">&nbsp;</a></td>
                                
                                <td class="<?=$classname;?>" onmouseover="changeThumbnail('<?=$videoThumbImg; ?>', 'vimeo')"><a href="<?=base_url(); ?>project/videos/<?=$projectId?>/<?=$curr_albumid;?>/<?=$videos['id']?>" title="<?=$videos['title']; ?>"><?=$videoName; ?> </a></td>
                                <td class="pl_cont"><?=$videos['stage_name']; ?></td>
                                <td class="pl_cont"><?=date('Y M d, h:i A', strtotime($videos['added_date'])); ?></td>
                                <td class="pl_cont" style="text-align: right; padding-right: 5px;"><?=$filesize; ?></td>
                                
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                
                <?php
                    }
                ?>
                
                </div>
                
                
                
            </div>
            <div id="right_comments">
                <?php
                /*<div class="commenttitle">comments for album <?=@$albumInfoArr['name']; ?></div>
                 * 
                 */
                ?>
                <div class="commenttitle">comments for video </div>
                <?php
                
                
                $i=1;
                $html='';
                foreach ($album_comments as $key => $value) {
                    $display=($videoid==$key) ? 'display:block' : '';
                    $html.='<div id="commentsVideo'.$key.'" class="showCommentsVideo" style="'.$display.'">';
                    $j=1;
                    foreach ($value as $arrkey => $arrvalue) {
                        if( strlen($j)==1 ){
                            $count='0'.$j;
                        }
                        else{
                            $count=$j;
                        }
                        $html.= '<div class="commentnormal">
                                    <div class="commentnames">'.$count.' - '.$arrvalue['stage_name'].' ('.date('d.m.y, h:i A', strtotime($arrvalue['added_date'])).') :</div>
                                    <div class="commentmessage"> '.$arrvalue['comment'].'</div>
                                </div>';
                        $j++;
                    }
                    $html.='</div>';
                    
                }
                
                echo $html;
                /*
                 * if( strlen($j)==1 ){
                        $count='0'.$j;
                    }
                    else{
                        $count=$j;
                    }
                    echo '<div class="commentnormal">
                                <div class="commentnames">'.$count.' - '.$value['stage_name'].' ('.date('d.m.y, h:i A', strtotime($value['added_date'])).') :</div>
                                <div class="commentmessage"> '.$value['comment'].'</div>
                            </div>';
                    $j++;
                 */
                ?>
            </div>
        </div>
    </div>
    
    

    <div id="cover" style="display: none; height: 100%; width: 100%;"></div>
    <script>
        $(function() {
          $( ".sortable" ).sortable();
          $( ".sortable" ).disableSelection();
        });
        
        $(window).load(function(){
            if(curr_album>0)
                $("#centerlisting_albums").mCustomScrollbar("scrollTo","#sidenavi_"+curr_album);
            
            if(imageid>0)
                $("#project_videos").mCustomScrollbar("scrollTo","#bottom_"+imageid);
            
            
        })
    </script>
    
</body>
</html>