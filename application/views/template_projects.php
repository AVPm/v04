<?php
// ****************************************************************************
// 
//     Main single template view of site
//
// ****************************************************************************

/**
  * Include header view
  *
  */
$this->load->view('include/header_project', $jsArray='', $cssArray='');

/**
  * Include page view files that you want to view
  *
  */
//echo '<div id="content_projects">';
$this->load->view($main_content, $data);
//echo '</div>';


/**
  * Include footer view
  *
  */
$this->load->view('include/footer_project');

?>