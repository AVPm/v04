<?php
// ****************************************************************************
// 
//     User's project list view on top of page in horizontal scroll div
//
// ****************************************************************************

/*
 * User own projects list
 */
$select_projectId=(isset($select_projectId) && $select_projectId>0) ? $select_projectId : 0; 
?>
<div class="clear">&nbsp;</div>
<div style="position:relative">
    <?php
    /*
    <table style="width:939px;" cellpadding="0" cellspacing="0"> 
        <tr>
            <td style="width: 177px;"><a onfocus="this.blur();" class="but_down floatLeft" href="javascript:void(0)" id="drop_projects" onclick="showhideDivData('projects_container', this.id)">Projects</a></td>
            <td style="width: 278px; color: #939393"><a href="javascript:void(0)" onclick="openDiv('addProject_div');">Add</a> <span>|</span> <a href="javascript:void(0)">Edit</a></td>
            <td style="color: #737373; width: 235px">
                Projects as: &nbsp; 
                <?php
                $id = 'id="project_role" onchange="viewProjectbyRole(this.value)"';
                $selected=(isset($roleid) && $roleid>0) ? $roleid : '';
                $options = array('1'=>'Owner', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');
                echo '<div class="styled-select floatRight">';
                echo form_dropdown('role', definitely $options, $selected, $id);
                echo '</div>';
                ?>
            </td>
            <td style="width: 252px;"></td>
        </tr>
    </table>
    
     * 
     */
    ?>
    <table cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td style="width: 115px;" class="whiteText"><a onfocus="this.blur();" class="but_down floatLeft" href="javascript:void(0)" id="drop_projects" onclick="showhideDivData('projects_container', this.id)">Projects</a></td>
                    <td style="width: 165px;">
                        <div style="margin-left:70px;">
                            <a href="javascript:void(0)" onclick="openDiv('addProject_div');">Add</a>
                            <?php if($select_projectId>0): ?>
                            <span>|</span> <a href="javascript:void(0)" onclick="openDiv('editprojectDiv');">Edit</a>
                            <?php endif; ?>
                            <span>|</span> <a href="javascript:void(0)" onclick="openDiv('sorting_projects');">Sort</a>
                        </div>
                    </td>
                    <td style="width: 248px; text-align: right;" class="emailcolor"><span style="margin-right:5px; margin-left: 45px;">Projects as:</span></td>
                    <td style="width: 368px;">
                        
                        <?php
                            $id = 'id="project_role2" onchange="viewProjectbyRole()"';
                            $selected=(isset($roleid2) && $roleid2>0) ? $roleid2 : '4';
                            $options = array(''=>'none', '1'=>'Owner', '4'=>'Co-Owner', '3'=>'Contributor', '2'=>'Viewer');
                            if($roleid>=1)
                                unset($options[$roleid]);
                            
                            echo '<div class="styled-select floatRight" style="margin-left:3px;">';
                            echo form_dropdown('role', $options, $selected, $id);
                            echo '</div>';
                         ?>
                        <span style="float:right; margin-left:5px; margin-right:5px;">and</span>
                        <?php
                            $id = 'id="project_role" onchange="viewProjectbyRole()"';
                            $selected=(isset($roleid) && $roleid>0) ? $roleid : '';
                            $options = array('1'=>'Owner', '4'=>'Co-Owner', '3'=>'Contributor', '2'=>'Viewer');
                            
                            echo '<div class="styled-select floatRight" style="margin-left:9px;">';
                            echo form_dropdown('role', $options, $selected, $id);
                            echo '</div>';
                         ?> 
                        
                    
                    </td>
                    <?php
                    /*
                        if(!strstr($_SERVER['REQUEST_URI'], 'project')){
                            echo '<td style="width: 110px;" class="datecolor">&nbsp;</td>';
                        }
                     * 
                     */
                    ?>
<!--                    <td style="width: 250px;" class="datecolor">&nbsp;</td>-->
                </tr>
            </tbody>
    </table>
    
    <?php
    /*
    <div class="linkpositioner"></div>
    <div id="project_filter">Projects as: &nbsp; 
        <?php
        $id = 'id="project_role" onchange="viewProjectbyRole(this.value)"';
        $selected=(isset($roleid) && $roleid>0) ? $roleid : '';
        $options = array('1'=>'Owner', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');
        echo '<div class="styled-select floatRight">';
        echo form_dropdown('role', $options, $selected, $id);
        echo '</div>';
        ?>
    </div>
     * 
     */
    ?>
</div>

<!-- add project HTML -->
<div class="popup" id="addProject_div" style="width: 640px;">
    <div class="popup_head">    
           <span>Add a new Project</span>
    </div>
    <div class="popup_top">
           <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
           <div class="run">To add a new project you need pick a unique project identifier, a project title and you can supply an image as frontcover for that project.</div>
           <div class="clear">&nbsp;</div> 
    </div>
    <div class="popup_forms">
       <?php
           echo form_open_multipart(base_url("user/login"), array('onsubmit'=>'return chkaddProject()', 'id'=>'add_project'));
           echo form_label('Identifier (unique):', 'identifier'); 
           echo '<br>';
           $data = array(
                           'name'        => 'identifier',
                           'id'          => 'identifier',
                           'onchange'    => 'check_identifier()'
                         );

           echo form_input($data) .'&nbsp; <span id="identifier_msg" style="display:none"></span>';
           echo '<br>';

           echo form_label('Title:', 'title'); 
           echo '<br>';
           $data = array(
                           'name'        => 'title',
                           'id'          => 'title',
                         );

           echo form_input($data);
           echo '<br>';
           
           echo '<label for="view_only_mode" style="float: left;">Allow access in View-only mode:</label>';
           echo '<input type="checkbox" style="float: left; width: auto; height: auto; margin: 2px 0px 0px 5px;" name="view_only_mode" id="view_only_mode" value="1">';
           echo '<div class="clear"></div>';
           echo '<br>';
           
           echo form_label('Upload image:', 'upload_image'); 
           echo '<br>';
           $data = array(
                           'name'        => 'upload_image',
                           'id'          => 'upload_image',
                           'style'       => 'height:25px;'
                         );

           echo form_upload($data);
           echo '<br>';

           echo form_close();
       ?>

   </div>
   <div class="popup_bottom">
       <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProject_div')">cancel</a>
       <a class="but_green floatRight" href="javascript:void(0)" onclick="$('#add_project').submit();">submit</a>
       <div class="clear">&nbsp;</div> 
   </div> 
</div>

<?php
if($select_projectId>0):
?>
<!-- edit project form -->
<div class="popup" id="editprojectDiv" style="width: 640px;">
<div class="popup_head">
    <span>Edit Project</span>
    <a class="off" href="<?=base_url(); ?>project/remove/<?=$select_projectId; ?>">Remove Project</a>	
	</div>
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run"></div>
		<div class="clear">&nbsp;</div> 
	</div>
	<div class="popup_forms">
            <?php
           
            echo form_open_multipart(base_url("/project/update"), array('onsubmit'=>'return chkupdateProject()', 'id'=>'edit_project'));
            echo form_label('Identifier (unique):', 'identifier'); 
            echo '<br>';
            $data = array(
                            'name'        => 'identifier',
                            'id'          => 'edit_identifier',
                            'onchange'    => 'check_identifier()',
                            'value'       => $view_project_detail[0]['identifier']
                          );

            echo form_input($data) .'&nbsp; <span id="edit_identifier_msg" style="display:none"></span>';
            echo '<br>';
            
            echo form_label('Title:', 'title'); 
            echo '<br>';
            $data = array(
                            'name'        => 'title',
                            'id'          => 'edit_title',
                            'value'       => $view_project_detail[0]['title']
                          );

            echo form_input($data);
            echo '<br>';
            $checked=($view_project_detail[0]['view_only_mode']=='1') ? 'checked="checked"' : '';
            echo '<label for="view_only_mode" style="float: left;">Allow access in View-only mode:</label>';
            echo '<input type="checkbox" style="float: left; width: auto; height: auto; margin: 2px 0px 0px 5px;" name="view_only_mode" id="view_only_mode" value="1" '.$checked.'>';
            echo '<div class="clear"></div>';
            echo '<br>';
            
            $data = array(
                            'name'        => 'thumbnail',
                            'id'          => 'thumbnail',
                            'value'       => $view_project_detail[0]['thumbnail'],
                            'type'        => 'hidden'
                          );

            echo form_input($data);
            
            $data = array(
                            'name'        => 'project_id',
                            'id'          => 'project_id',
                            'value'       => $select_projectId,
                            'type'        => 'hidden'
                          );

            echo form_input($data);

            echo form_label('Upload image:', 'upload_image'); 
            echo '<br>';
            $data = array(
                            'name'        => 'upload_image',
                            'id'          => 'edit_upload_image',
                            'style'       => 'height:25px;'
                          );

            echo form_upload($data);
            echo '<br>';

            echo form_close();
            ?>
            
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('editprojectDiv')">cancel</a>
            <a class="but_green floatRight" href="javascript:void(0)" onclick="$('#edit_project').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
        </div> 		
</div>
<?php
endif;
?>



<?php
//view listing of user's project

    if(!empty($user_projects) || !empty($project_participated)){
?>
<div class="clear">&nbsp;</div>
 <!-- my own project listing -->
 <div class="projects_container floatclear" id="projects_container"> 
     <div id="wn7" style="overflow: hidden; width: 100%;">
        <table cellspacing="0" cellpadding="0" border="0" id="t2">
          <tbody>
              <tr id="sortrow">
              <?php
                foreach ($user_projects as $key) {
                  $thumbnail=($key['thumbnail']!='') ? $key['thumbnail'] : 'projecticon160.png';
                  $class=($key['id']==$select_projectId) ? 'outerprojects_select' : 'outerprojects';
              ?>    
                <td id="projectlist_<?=$key['id']; ?>">
                        <div class="<?=$class; ?>">
                            <a onfocus="this.blur();" class="pic" href="<?=base_url(); ?>project/view/<?=$key['id']; ?>"><img alt="" height="160" width="160" src="<?=base_url(); ?>uploads/<?=$thumbnail; ?>"></a>

                            <div class="underpic_label">name:</div>
                            <div class="underpic_value"><?=$key['title']; ?></div>
                            <div class="clear"></div>

                            <div class="underpic_label">id:</div>
                            <div class="underpic_value"><?=$key['identifier']; ?></div>
                            <div class="clear"></div>    
                        </div>
                    
                </td>
                <td style="width: 20px;">&nbsp;</td>
            <?php
                }
                if(!empty($project_participated)){
                    foreach ($project_participated as $key) {
                        $thumbnail=($key['thumbnail']!='') ? $key['thumbnail'] : 'projecticon160.png';
                        $class=($key['id']==$select_projectId) ? 'outerprojects_select' : 'outerprojects';
                    ?>    
                      <td id="projectlist_<?=$key['id']; ?>">
                              <div class="<?=$class; ?>">
                                  <a onfocus="this.blur();" class="pic" href="<?=base_url(); ?>project/view/<?=$key['id']; ?>"><img alt="" height="160" width="160" src="<?=base_url(); ?>uploads/<?=$thumbnail; ?>"></a>

                                  <div class="underpic_label">name:</div>
                                  <div class="underpic_value"><?=$key['title']; ?></div>
                                  <div class="clear"></div>

                                  <div class="underpic_label">id:</div>
                                  <div class="underpic_value"><?=$key['identifier']; ?></div>
                                  <div class="clear"></div>    
                              </div>

                      </td>
                      <td style="width: 20px;">&nbsp;</td>
                  <?php
                      }
                }
                    

                
              ?>

              </tr>
        </tbody></table>      
    </div>

  </div>
 <?php
 }
 
 /*
 // to show listing of participated projects
 
 ?>
<hr class="hr_grey">
<div class="clear">&nbsp;</div>

<!--
<div style="position:relative">
    <a onfocus="this.blur();" class="but_down floatLeft" href="javascript:void(0)">Projects I Participate in</a>
</div>
-->
<table cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td style="width: 280px;" class="whiteText"><a onfocus="this.blur();" class="but_down floatLeft" href="javascript:void(0)" id="drop_projects" onclick="showhideDivData('projects_container', this.id)">Projects I Participate in</a></td>
            
            <td style="width: 248px; text-align: right;" class="emailcolor">
                <!--<span style="margin-right:5px; margin-left: 45px;">Projects as:</span> -->
            </td>
            <td style="width: 160px;">
            
                $id = 'id="project_role2" onchange="viewProjectbyRole()"';
                $selected=(isset($roleid2) && $roleid2>0) ? $roleid2 : '4';
                $options = array('1'=>'Owner', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');
                echo '<div class="styled-select floatRight" style="margin-left:9px;">';
                echo form_dropdown('role', $options, $selected, $id);
                echo '</div>';
             
             ?> 
            </td>
            <?php
                if(!strstr($_SERVER['REQUEST_URI'], 'project')){
                    echo '<td style="width: 110px;" class="datecolor">&nbsp;</td>';
                }
            ?>
<!--                    <td style="width: 250px;" class="datecolor">&nbsp;</td>-->
        </tr>
    </tbody>
</table>
<?php
    if(!empty($project_participated)){
?>
<div class="clear">&nbsp;</div>
 <!-- my own project listing -->
 
 <div class="projects_container floatclear" id="project_participated"> 
     <div id="wn7" style="overflow: hidden; width: 100%;">
        <table cellspacing="0" cellpadding="0" border="0" id="t2">
          <tbody>
              <tr id="sortrow">
              <?php
              foreach ($project_participated as $key) {
                  $thumbnail=($key['thumbnail']!='') ? $key['thumbnail'] : 'projecticon160.png';
                  $class=($key['id']==$select_projectId) ? 'outerprojects_select' : 'outerprojects';
              ?>    
                <td>
                        <div class="<?=$class; ?>">
                            <a onfocus="this.blur();" class="pic" href="/project/view/<?=$key['id']; ?>"><img alt="" src="/uploads/<?=$thumbnail; ?>"></a>

                            <div class="underpic_label">name:</div>
                            <div class="underpic_value"><?=$key['title']; ?></div>
                            <div class="clear"></div>

                            <div class="underpic_label">id:</div>
                            <div class="underpic_value"><?=$key['identifier']; ?></div>
                            <div class="clear"></div>    
                        </div>
                    
                </td>
                <td style="width: 20px;">&nbsp;</td>
            <?php
                }
              ?>

              </tr>
        </tbody></table>      
    </div>

  </div>
 
 <?php
 */
if($select_projectId>0){
?>
    <script>
        $(window).load(function(){

                $("#projects_container").mCustomScrollbar("scrollTo","#projectlist_<?=$select_projectId; ?>");

        })
    </script>
<?php
}
?>
    
<!-- project sorting form start -->
    <?php
    $userid = $this->session->userdata('user_id');
    if($userid>0 && (!empty($user_projects) || !empty($project_participated))):
    ?>
    <div class="popup" id="sorting_projects" style="width: 1100px;">
        <div class="popup_head">    
          <span>Sort Projects</span>
        </div>
              <div id="containmentbox" class="sort_area">
               <ul id="sortprojectbox" class="sortable ui-sortable" style="">
                   <?php
                   foreach ($user_projects as $key) {
                        //$thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        $thumbnail=($key['thumbnail']!='') ? base_url().'uploads/'.$key['thumbnail'] : base_url().'uploads/projecticon160.png';
                        echo '<li id="id_'.$key['id'].'" style="">
                                <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$key['title'].'</div>
                            </li>';
                   }
                   if(!empty($project_participated)){
                        foreach ($project_participated as $key) {
                            $thumbnail=($key['thumbnail']!='') ? base_url().'uploads/'.$key['thumbnail'] : base_url().'uploads/projecticon160.png';
                            echo '<li id="id_'.$key['id'].'" style="">
                                    <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                    <div class="album_name">'.$key['title'].'</div>
                                </li>';
                       }
                   }
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_projects')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('projects');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
        
    <?php
    endif;
    ?>
    <!-- project sorting form ends -->
    
    <script>
         $(function() {
            $( ".sortable" ).sortable();
            $( ".sortable" ).disableSelection();

          });
    </script>