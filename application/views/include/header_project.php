<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr" lang="en"><head>
<title><?php echo (isset($title)) ? $title : "AVP Matrix" ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="screen">@import url("<?php echo base_url();?>css/style_screen.css");</style>
<style type="text/css" media="screen">@import url("<?php echo base_url();?>css/style_scroll.css");</style>
<script src="<?php echo base_url();?>js/dw_event.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>js/dw_scroll.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/dw_scrollbar.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/scroll_controls.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/swfobject.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/swfforcesize.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/swfupload_004.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/swfupload_002.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/swfupload.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/swfupload_003.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui-1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/script_supersized_mod.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/pixastic.js"></script>
<script src="<?php echo base_url();?>js/avp.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/common.js"></script>
<script src="<?php echo base_url('js/jquery.mCustomScrollbar.concat.min.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/jquery.mCustomScrollbar.css');?>" />
<script type="text/javascript">
var base_url='<?=base_url(); ?>';  
window.selectedImage ='content/displayitems/';
window['flags'] = { upscale: false };
		
  function gotoID(){
		
		dw_scrollObj.col.wn1.scrollToId('wn1', 'sidenavi_3650', 'lyr1', 0);   
	}
  $(function(){
	    resizeTable();
      $('#tableLayout').show();
      init_dw_Scroll();
      tableruler(); 
             
      gotoID(); 
      divruler();
      //$('#formdiv').draggable();
      
      /**
       * changes img into canvas and can apply all kinds of alterations to it...
       * so far pretty nice, but has no api to alter it later on - yet  
       *
       *                    
      
      $('.bottomlinks').mouseenter(function(){        
        $(this).children('img').pixastic("hsl", {hue:0,saturation:100,lightness:0});
        $('bottomlinks').mouseleave(function(){
          $(this).children('canvas').revert();
        });
      });
      */    
                                            
	});
	$(window).resize(function() {
	  resizeTable();
	  init_dw_Scroll();
	  gotoID();
  });
    	(function($){
            $(window).load(function(){
                $(".projects_container").mCustomScrollbar({
                        horizontalScroll:true,
                        scrollButtons:{
                                enable:true
                        },
                });
            })
        })(jQuery);

</script>
</head>
<body>
<table id="tableLayout" style="height: 630px;" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td id="top" colspan="2">
        <div id="top2">
            <a href="javascript:void(0)">
                <img src="<?php echo base_url();?>images/transparent.gif" style="height:33px; width:100px;" />
            </a>
            
            
            <div class="topnav">
                <?php $classDash=(!isset($selected_menu)) ? 'on' : ''; ?>
                <a onfocus="this.blur();" href="<?=base_url(); ?>" class="<?=$classDash; ?>">Dashboard</a>
                <?php
                if(isset($top_menu)){
                    foreach ($top_menu as $link=> $menu) {
                        $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                        echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">'.$menu.'</a>';
                    }
                }
                ?>
      
            </div>
            <?php if(count($this->session->userdata('last_remove'))>0){ ?>
                <div class="undobutton"><a class="blink_me" href="<?=base_url('project/lastundo'); ?>">Undo</a></div>
            <?php } ?>
            <div class="topform">
              
              <img src="<?php echo base_url();?>images/project_id.gif" style="margin:2px 0 0 0; float:left;" height="24" width="110" />
                <?php
                    $data = array(
                    'name'        =>
                    'identifier_project',
                    'id'          =>
                    'top_id_input',
                    'size'        =>
                    '18',
                    );

                    echo form_input($data);
                    
                ?>

            </div>
            
          </div>
  </td>
      </tr>
        
        