 <!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<meta http-equiv="X-Frame-Options" content="allow">
<title><?php echo (isset($title)) ? $title : "AVP Matrix" ?> </title>

<script src="<?php echo base_url('js/jquery-1.8.2.min.js');?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    var base_url='<?=base_url(); ?>';  
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/common.js"></script>
<?php
if(isset($jsArray)):
    foreach ($jsArray as $js) {
        echo '<script type="text/javascript" src="'.base_url().'js/'.$js.'.js"></script>';
        echo "\n";
    }
endif;
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" />

<?php
if(isset($cssArray)):
    foreach ($cssArray as $css) {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().'css/'.$css.'.css" />';
        echo "\n";
    }
endif;
?>
</head>
<body>
    <?php 
        
    /*
     *  if user is already login then show login user's information and logout link
     */
        if($this->session->userdata('user_id')!=''){
    ?>
        <div id="top_header">	
            <div>
                <a href="<?=base_url(); ?>"><img style="height:33px; width:100px;" src="<?=base_url(); ?>images/transparent.gif"></a>
                <div class="topnav">
                    <?php $classDash=(!isset($selected_menu) && !isset($selected_menu_subscrip)) ? 'on' : ''; ?>
                    <a onfocus="this.blur();" href="<?=base_url(); ?>" class="<?=$classDash; ?>">Dashboard</a>
                    <?php
                    if(isset($top_menu)){
                        foreach ($top_menu as $link=>$menu) {
                            $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                            echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">'.$menu.'</a>';
                        }
                    }
                    else{
                        $classSubscrip=(isset($selected_menu_subscrip)) ? 'on' : '';
                        $classFileManagement=(isset($selected_menu_file_manage)) ? 'on' : '';
                        $classWebPages=(isset($selected_web_page)) ? 'on' : '';
                        echo '<a onfocus="this.blur();" href="'.base_url('user/subscriptions').'" class="'.$classSubscrip.'">Subscription</a>';
                        echo '<a onfocus="this.blur();" href="'.base_url('user/file_management').'" class="'.$classFileManagement.'">File Management</a>';
                    }
                    
                    
                    ?> 
                    
                </div>
                <?php
                    $remove_array=$this->session->userdata('last_remove');
                    if(is_array($remove_array)){ ?>
                        <div class="undobutton"><a class="blink_me" href="<?=base_url('project/lastundo'); ?>">Undo</a></div>
                <?php } ?>
            </div>
            
            <div class="topform">
              <form method="post" action="index.php">
                <img width="110" height="24" style="margin:2px 0 0 0; float:left;" src="<?=base_url(); ?>images/project_id.gif"><input type="text" value="" name="identifier_project" size="18" class="top_id_input">
              </form>
            </div>
        </div>
    <?php
    /*
    <div class="topmenu"><a href="<?=base_url(); ?>">HOME</a></div>
    <div style="margin-top: 15px;" class="floatRight">
        logged in as <?php echo $this->session->userdata('user_email'); ?>
        <br>
        <a href="javascript:void(0)" onclick="openDiv('edit_profile')">Editdetails</a>
        <br>
        <?php echo anchor('user/logout', 'Logout'); ?>
    </div>
     * 
     */
    ?>
    <?php
        }
    else {
    ?>
    <div class="head">
        <?php
        /*
         * if user is not login then show login form
         */
        ?>
        <div class="login">
            <?=form_open("user/login"); ?>		

                <div class="loginrow"><?=form_label('User Name', 'user_name').': '; ?></div>
                <?php
                    $data = array(
                                'name'        => 'email',
                                'id'          => 'email',
                                'size'        => '22',
                              );

                    echo form_input($data);
                ?>
                <div class="loginrow"><?=form_label('Password', 'pass').': '; ?></div>
                <?php
                    $data = array(
                            'name'        => 'pass',
                            'id'          => 'pass',
                            'size'        => '22',
                          );

                    echo form_password($data);
                ?>
                <div class="loginrow">&nbsp;</div>
                <div style="width: 270px;">
                    <?php
                        $data = array(
                            'name' => 'sbt_signup',
                            'id' => 'sbt_signup',
                            'value' => 'Sign up',
                            'type' => 'button',
                            'content' => 'Sign Up',
                            'onclick' => 'return openDiv(\'formdiv\');',
                            'style' => 'float:left;',
                        );
                        
                        echo '<div style="width:80px; float:left">'.form_button($data).'</div>';
                        
                        echo '<div style="width:20px; float:left; margin-top:4px;">OR</div>';
                        
                        $data = array(
                            'name' => 'sbt_signin',
                            'id' => 'sbt_signin',
                            'value' => 'Sign in',
                            'type' => 'submit',
                            'content' => 'Sign In',
                        );

                        echo '<div style="width:66px; float:right">'.form_button($data).'</div>';
                    ?>
                </div>
                    
                
           <?=form_close(); ?>	
        </div>
        <?php 
        /*
         * old login html
         
        <div class="signin_form">
	
        
        
            echo form_open("user/login"); 
        
            echo form_label('User Name', 'user_name').': '; 
            $data = array(
                            'name'        => 'email',
                            'id'          => 'email',
                          );

            echo form_input($data);
        
            
            echo form_label('Password', 'pass').': '; 
            $data = array(
                            'name'        => 'pass',
                            'id'          => 'pass',
                          );

              echo form_password($data);
         
            $data = array(
                'name' => 'sbt_signin',
                'id' => 'sbt_signin',
                'value' => 'Sign in',
                'type' => 'submit',
                'content' => 'Sign in',
                'onclick' => 'return '
            );

            echo form_button($data); 
            echo form_close(); 
    ?>
        </div><!--<div class="signin_form">-->
         * 
         */
      ?>  
    </div>
    <?php
    }
    ?>
	<div id="wrapper">
            
           
            
 
            