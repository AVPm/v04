<?php
// ****************************************************************************
// 
//     User's project files view
//
// ****************************************************************************
session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
$userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            //$albumInfoArr['description']=$value['description'];
        }
    }
}

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <title></title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/bootstrap.min.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/style.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/image_page.css"  />
    <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/jquery.mCustomScrollbar.css" />
    <script>

        var curr_project='<?=@$projectId; ?>';
        var curr_album='<?=@$curr_albumid; ?>';
        var base_url='<?=base_url(); ?>';  
        var imageid='<?=@$fileid; ?>';
        var pageAction='fileupload';
        var image_version="";
        var imageid_version="";
        var upload_url= "<?=base_url(); ?>project/album/fileupload";
        var donelink='donelink';
    </script>
  <!--  <script src="/js/jquery-1.8.2.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="/js/jquery.sortable.js"></script>-->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui.min.js"></script>
    <script src="<?=base_url(); ?>/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfforcesize.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_004.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_002.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/swfupload_003.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/script_supersized_mod.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/pixastic.js"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/common.js"></script>
    <script src="<?=base_url(); ?>/js/avp.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload = function()
        {
            CKEDITOR.replace( 'comment', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.replace( 'comment_edit', {
                    removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.on('dialogDefinition', function( ev ){
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                if ( dialogName == 'link' ){
                    // Get a reference to the "Target" tab.
                            var targetTab = dialogDefinition.getContents( 'target' ); 
                            // Set the default value for the target field.
                            var targetField = targetTab.get( 'linkTargetType' );
                            targetField['default'] = '_blank';
                     }
            });
        }

        //////////end of validations////////////////
    </script>
    
    <style>
        .popup{display: none}
    </style>

</head>

<body>
    
    <!-- add folder popup starts -->
    <?php if($userid>0){ ?>
        <div class="popup" id="add_album" style="width: 640px;">
        <div class="popup_head">
                <span>Add Folder</span>	
        </div>
        <div id="adduserHtml">
            <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">To Add a Folder you have to enter a Name in the form below. You can also supply a picture to have a thumbnail image for the folder.</div>
                    <div class="clear">&nbsp;</div> 
                    <div class="clear" id="userProjectmsg"></div>
            </div>
            <div class="popup_forms">
                <form action="" enctype="multipart/form-data" method="post" name="addfolder" id="addfolder">
                 <?php
                    /*
                     * hidden field with value of project id
                     */
                        $data = array(
                        'name'        => 'projectid_hidden',
                        'id'          => 'projectid_hidden',
                        'value'       => $projectId,
                        'type'        => 'hidden',
                        );

                        echo form_input($data);

                        echo form_label('Folder Name: ', 'folder_name'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'folder_name',
                                        'id'          => 'folder_name',
                                      );
                        echo form_input($data);

                        ?>
                        <br>
                        Thumbnail:<br>
                        <div style="position:relative; width:310px; height:19px;">
                                        <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                                <input type="file" onchange="javascript:copyField();" ondblclick="javascript:deleteField();" size="30" name="thumbnail" id="fileselecter" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                        </div>
                            <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                                <input type="text" style="width:281px; float:left;" id="fakefilefield"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                            </div>
                        </div>
                </form>

            </div>
        </div>
        <div class="popup_bottom">
            <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('add_album')">Cancel</a>
            <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#addfolder').submit();">Submit</a>
            <div class="clear">&nbsp;</div>
        </div>		
    </div>
    <?php } ?>
    <!-- add folder popup ends -->
    
    
    <!-- edit folder popup starts -->
    <?php
    if(!empty($albumInfoArr) && $userid>0){
    ?>
    <div class="popup" id="edit_album_form" style="width: 640px;">
        <div class="popup_head">
            <span>Edit Folder</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'files')">Remove Folder</a>
        </div>
            <div class="popup_top_project">
                <?php $thumbnail=($albumInfoArr['thumbnail']!='') ? base_url().'uploads/'.$albumInfoArr['thumbnail'] : base_url().'images/imageicon.png'; ?>
                    <img alt="" src="<?=$thumbnail;?>" width="160" height="160">
                    <div class="run"></div>
                    <div class="clear">&nbsp;</div> 
            </div>
            <div class="popup_forms">
                <?php

                    echo form_open_multipart("", array('id'=>'editalbum'));
                    /*
                     * hidden field with value of project id
                     */
                    $data = array(
                    'name'        => 'album_hidden',
                    'id'          => 'album_hidden',
                    'value'       => $curr_albumid,
                    'type'        => 'hidden',
                    );

                    echo form_input($data);
                    
                    /*
                     * input field for album name
                     */
                    echo form_label('Album Name: ', 'edit_album_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'edit_album_name',
                                    'id'          => 'edit_album_name',
                                    'value'       => $albumInfoArr['name'],
                                  );
                    echo form_input($data);

                    echo '<br>';
                    
                    
                    /*
                     * hidden field with value of project thumbnail
                     */
                    $data = array(
                    'name'        => 'album_thumbnail_hidden',
                    'id'          => 'album_thumbnail_hidden',
                    'value'       => $albumInfoArr['thumbnail'],
                    'type'        => 'hidden',
                    );

                    echo form_input($data);

                ?>
                Thumbnail:<br>
          <div style="position:relative; width:310px; height:19px;">
                <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                        <input type="file" size="30" name="edit_thumbnail" id="fileselecters" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                </div>
                <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                    <input type="text" style="width:281px; float:left;" id="fakefilefield" value="<?=$albumInfoArr['thumbnail']; ?>"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                </div>
            </div>
            <br>

                    remove current Folder Thumbnail:<br>
                    <?php
                    $data = array(
                        'name'        => 'deletethumb',
                        'id'          => 'deletethumb',
                        'value'       => 'delete',
                        'class'       => 'checkbox'
                        );

                    echo form_checkbox($data);
                   
                    echo form_close(); ?>

            </div>
            <div class="popup_bottom">
                <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_album_form')">Cancel</a>
                <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#editalbum').submit();">Submit</a>
                <div class="clear">&nbsp;</div> 
            </div>		
    </div>
    <?php
    }
    ?>
    <!-- edit album popup ends -->


    <!-- add images in album popup start -->
    <?php if($userid>0): ?>
        <div class="popup" id="add_image_popup" style="width: 820px;">
        <?php
        /*
            if($imageversion=='yes'){
                $versionClass='on';
                $imageClass='off';
            }
         * 437551221086006
            else{
                $versionClass='off';
                $imageClass='on';
            }
         * 
         */
        ?>
    <div class="popup_head">
        <a class="on" href="javascript:void(0)" id="image_only">Add Files</a>
            </div>
            <?php
            /*
            <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">Upload Files. First browse for your image files, then start upload.</div>
                    <div class="clear">&nbsp;</div> 
            </div>
             * 
             */
            ?>
            <div class="popup_forms"  style="padding: 0px">
            <form action="" enctype="multipart/form-data" method="post" id="form1">
            <?php
            /*
            <div id="queue-status">
        <div id="spanButtonPlaceHolderwrapper"><span id="spanButtonPlaceHolder"></span></div>
        <span id="divStatus" style="display:none;">0 Files Uploaded</span> 
      </div>

            <div class="controls">
                    <input type="button" onclick="swfu.startUpload();" value="Start upload" id="btnStart" class="controlbuttons" onfocus="this.blur();">
                    <input type="button" onclick="swfu.cancelQueue();" value="Cancel all uploads" id="btnCancel" class="controlbuttons" onfocus="this.blur();">
            </div>
    <div id="fsUploadProgress" class="fieldset flash">
                    <h3>File Queue</h3>
                    <span id="queue-is-empty">currently empty</span>
            </div>
            */
            ?>
                <div id="fileuploader">
                    <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                </div> 
<?php
/*
    <script type="text/javascript">	
    var swfu;

    var settings = {
            flash_url : "<?=base_url(); ?>includes/swfupload.swf",
            upload_url: "<?=base_url(); ?>project/album/fileupload",	// Relative to the SWF file
            post_params: {"PHPSESSID" : "<?=$sessionid; ?>", "album_id":"<?=$curr_albumid; ?>"},
            file_size_limit : "999 MB",
            file_types : "*.*",
            file_types_description : "Files",
            file_upload_limit : 999,
            file_queue_limit : 0,
            file_post_name : "file", 
            custom_settings : {
                    progressTarget : "fsUploadProgress",
                    cancelButtonId : "btnCancel",
                    startButtonId : "btnStart"
            },
            debug: false,				

            // Button settings
            button_image_url: "<?=base_url(); ?>images/folder4x.png",	// Relative to the Flash file
            button_width: 150,
            button_height: 19,
            button_placeholder_id: "spanButtonPlaceHolder",
            button_text_left_padding : 30, 
            button_text : "<span class=\"whitetext\">Browse for Files<\/span>",
            button_text_style : ".whitetext { color: #FFFFFF;font-family:Arial, Helvetica, sans-serif; font-size:12px;}", 
            button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT, 

            // The event handler functions are defined in handlers.js
            file_queued_handler : fileQueued,
            file_queue_error_handler : fileQueueError,
            file_dialog_complete_handler : fileDialogComplete,
            upload_start_handler : uploadStart,
            upload_progress_handler : uploadProgress,
            upload_error_handler : uploadError,
            upload_success_handler : uploadSuccess,
            upload_complete_handler : uploadComplete,
            queue_complete_handler : queueComplete	// Queue plugin event
    };
    swfu = new SWFUpload(settings);
    </script>
 * 
 */
?>
                    </form>

            </div>
            <div class="popup_bottom">
                <a onclick="window.location.href='/project/files/<?=$projectId; ?>/<?=$curr_albumid; ?>';" href="javascript:void(0)" onfocus="this.blur();" class="but_red floatLeft">cancel</a>
                <a id="donelink" style="display: none" class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="window.location.href='/project/files/<?=$projectId; ?>/<?=$curr_albumid; ?>';">done</a>
                    <div class="clear">&nbsp;</div> 
            </div> 
    </div>
    <?php endif; ?>
    <!-- add images in album popup ends -->
    
    <!-- edit album's images popup -->
    <?php
        if($fileid>0 && $userid>0){
    ?>
            <div class="popup" id="edit_album_image" style="width: 640px;">
                <div class="popup_head">    
                    <span>Edit File</span>
                    <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage('files', '<?=$fileid; ?>')">Delete file</a>
                </div>
                <div class="popup_top">
                    <?php
                        $videothumb=($file_thumb=='') ? base_url().'images/popup_pic1.jpg' : '/uploads/'.$file_thumb;
                    ?>
                    <img alt="" src="<?=$videothumb; ?>" title="<?=$file_title; ?>" width="98" height="98">
                    <div class="run">Edit an File.</div>
                    <div class="clear">&nbsp;</div> 
                </div>
                <div class="popup_forms">
                    <?php 
                        echo form_open_multipart("", array('id'=>'editfile'));
                        
                        $data = array(
                                    'name'        => 'project_fileid_hidden',
                                    'id'          => 'project_fileid_hidden',
                                    'value'       => $fileid,
                                    'type'        => 'hidden',
                                    );

                        echo form_input($data);
                
                        echo form_label('Title: ', 'file_title'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'file_title',
                                        'id'          => 'file_title',
                                        'value'       => $file_title,
                                      );
                        echo form_input($data);

                        echo '<br>';

                        echo form_label('additional Info: ', 'additional_info'); 
                        $data = array(
                                'name'        => 'additional_info',
                                'id'          => 'additional_info',
                                'rows'        => '7',
                                'cols'        => '36',
                                'value'       => $file_info
                              );

                        echo form_textarea($data);
                        
                        /*
                        * hidden field with value of project thumbnail
                        */
                       $data = array(
                       'name'        => 'file_thumbnail_hidden',
                       'id'          => 'file_thumbnail_hidden',
                       'value'       => $file_thumb,
                       'type'        => 'hidden',
                       );

                       echo form_input($data);
                        ?>    
                        Thumbnail:<br>
                        <div style="position:relative; width:310px; height:19px;">
                              <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                      <input type="file" size="30" name="edit_thumbnail" id="fileselectersvideothumb" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                              </div>
                              <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                                  <input type="text" style="width:281px; float:left;" id="fakefilefieldvideothumb" value="<?=$file_thumb; ?>"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                              </div>
                        </div>
                        <br>
<!--
                    remove current Folder Thumbnail:<br>-->
                    <?php
//                    $data = array(
//                        'name'        => 'deletethumb',
//                        'id'          => 'deletethumb',
//                        'value'       => 'delete',
//                        'class'       => 'checkbox'
//                        );
//
//                    echo form_checkbox($data);    
                    ?>
                    
<!--                    <br>-->
                    <?php
//                    echo 'Show Box Thumbnail:<br>';
//                    $data = array(
//                        'name'        => 'crocdocthumb',
//                        'id'          => 'crocdocthumb',
//                        'value'       => 'crocdoc',
//                        'class'       => 'checkbox',
//                        'checked'     => 'checked'
//                        );
//
//                    echo form_checkbox($data); 
//                        
                    ?>
<!--                    <br> -->
                    <?php echo form_close(); ?>
                </div>
                <div class="popup_bottom">
                        <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('edit_album_image')">cancel</a>
                        <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editfile').submit();">submit</a>
                        <div class="clear">&nbsp;</div> 
                </div>

            </div>
    <?php  } ?>

    <!-- add comment form -->
    <?php if($userid>0): ?>
        <div class="popup" id="addalbumCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Add Comment</span>    		
	</div>
        <?php
        /*
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Add a Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
         * 
         */
        ?>
	<div class="popup_forms" style="padding: 0;">
        <?php
            echo form_open("", array('id'=>'addcommentfrm')); 
            
            $data = array(
                        'name'        => 'album_comment_hidden',
                        'id'          => 'album_comment_hidden',
                        //'value'       => $curr_albumid,
                        'value'       => $fileid,
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $user_stagename,
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            //echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment',
                    'id'          => 'comment',
                    'rows'        => '7',
                    'cols'        => '36',
                  );

            echo form_textarea($data);
            
            echo form_close(); 
        ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addalbumCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#addcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    <?php endif; ?>
    <!-- add comment form ends -->
    
    <!-- edit comment form -->
    <?php
    //@$lastCommentArr=$album_comments[0];
    $lastCommentArr= array();
    foreach ($album_comments as $key => $value) {
        if($fileid==$key){
            $lastCommentArr=$value[0];
            break;
        }
    }
    if($userid>0 && !empty($lastCommentArr)):
    ?>
        <div class="popup" id="editCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Edit Comment</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removecomment('files','<?=$lastCommentArr['id']; ?>')">Remove Comment</a>    		
	</div>
        <?php
        /*
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Edit your last Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
         * 
         */
        ?>
	<div class="popup_forms" style="padding: 0;">
        <?php
        
            echo form_open("", array('id'=>'editcommentfrm'));
            $data = array(
                        'name'        => 'commentid_hidden',
                        'id'          => 'commentid_hidden',
                        'value'       => $lastCommentArr['id'],
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $lastCommentArr['stage_name'],
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            //echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment_edit',
                    'id'          => 'comment_edit',
                    'rows'        => '7',
                    'cols'        => '36',
                    'value'       => $lastCommentArr['comment']
                  );

            echo form_textarea($data);
            
            echo form_close(); 
            ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('editCommentDiv')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
    <?php endif; ?>
    <!-- edit comment forms ends -->
    
    <!-- album sorting form start -->
    <?php if($userid>0): ?>
        <div class="popup" id="sorting_album" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Albums</span>
        </div>
              <div id="containmentbox" class="sort_area">
               <ul id="sortbox" class="sortable ui-sortable" style="">
                   <?php
                   
                   foreach ($albumsArr as $album){
                        $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        echo '<li id="id_'.$album['id'].'" style="">
                                <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$album['name'].'</div>
                            </li>';
                   }
                    
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('files');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php endif; ?>
    <!-- album sorting form ends -->
    
    
    <!-- album files sorting form start -->
    <?php if($userid>0 && $fileid>0): ?>
        <div class="popup" id="sorting_album_files" style="width: 94%;">
        <div class="popup_head">    
          <span>Sort Albums</span>
        </div>
              <div id="containmentbox" class="sort_area">
               <ul id="sortbox" class="sortable ui-sortable" style="">
                   <?php
                   
                   foreach ($project_files as $files){
                        $thumbnail=($files['thumbnail']!='') ? base_url().'uploads/'.$files['thumbnail'] : base_url().'images/imageicon.png';
                        echo '<li id="id_'.$files['id'].'" style="height:145px; width:120px;">
                                <img width="120" height="120" class="selector" src="'.$thumbnail.'">
                                <div class="album_name">'.$files['title'].'</div>
                            </li>';
                   }
                    
                   ?>
                
                </ul>  
                  <div class="clear">&nbsp;</div>
        
              </div>
              <div class="popup_bottom_wide">
                  <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="closeDiv('sorting_album_files')">cancel</a>
                      <a class="but_green floatright" onfocus="this.blur();" href="javascript:saveOrder('user_files');">done</a>		
                      <div class="clear">&nbsp;</div> 
              </div>
       </div>
    <?php endif; ?>
    <!-- album files sorting form ends -->
    
    
    
    <div id="top_header">	
            <div>
                <a href="<?=base_url(); ?>"><img src="<?=base_url(); ?>images/transparent.gif" style="height:33px; width:100px;"></a>
                <div class="topnav">
<!--                    <a class="on" href="http://v04.avpmatrix.com/" onfocus="this.blur();">Dashboard</a>-->
                    <?php $classDash=(!isset($selected_menu)) ? 'on' : ''; 
                    if($userid>0){
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Dashboard</a>';
                    }
                    else{
                        echo '<a onfocus="this.blur();" href="'.base_url().'" class="'.$classDash.'">Home</a>';
                    }
                        if(isset($top_menu)){
                            foreach ($top_menu as $link=>$menu) {
                                $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                                echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">
                                '.$menu.'</a>';
                            }
                        }
                    ?>                  
                </div>
                <?php
                    $remove_array=$this->session->userdata('last_remove');
                    if(is_array($remove_array)){ ?>
                        <div class="undobutton"><a class="blink_me" href="<?=base_url('project/lastundo'); ?>">Undo</a></div>
                <?php } ?>
            </div>
            
            <div class="topform">
              <form action="" method="post">
                <img width="110" height="24" src="<?=base_url(); ?>images/project_id.gif" style="margin:2px 0 0 0; float:left;"><input type="text" class="top_id_input" size="18" name="identifier_project" value="">
              </form>
            </div>
        </div>
    <div id="wrapper">
        <div id="image_content">
            <div class="divbar" id="divbarid">
                
            </div>
            <div id="outerCenterImg">
                <table style="width:100%; height: 100%">
                    <tr>
                        <td valign="middle" align="center" style="height: 100%">
                            
                            <?php
                            $filArr = pathinfo($file_name);
                            //if($file_crocdoc_id!=''){
                            $img_arr = array('jpg', 'jpeg', 'gif', 'png');
                            $file_arr = array('doc', 'docx', 'xlsx', 'xls', 'xl', 'txt', 'text', 'pdf', 'ppt', 'pptx', 'word');
                            if(in_array(strtolower($filArr['extension']), $img_arr)){
                            ?>
                            <div id="centerLage_image" style="height: 100%; padding-bottom: 20px;">
                                <img id="imagescaler" src="<?=base_url().'uploads/'.'/'.$file_name;?>" style="height: 100%">
                            </div>
<!--                            <div style="height: 100%;" id="iframeDiv">
                                <embed src="<?=base_url().'uploads/'.$file_name;?>" type="application/pdf" style="height: 408px;" id="file_iframe">
                                <iframe id="file_iframe" src="<?=base_url().'uploads/'.'/'.$file_name;?>" frameborder="0" scrolling="no" webkitallowfullscreen mozallowfullscreen allowfullscreen style="width:50%" align=""middle"></iframe>
                            </div>-->
                            <?php
                            }
                            else if(in_array(strtolower($filArr['extension']), $file_arr)){
                            ?>
                            <div style="height: 100%;" id="iframeDiv">
<!--                                <embed src="<?=base_url().'uploads/'.$file_name;?>" type="application/pdf" style="height: 408px;" id="file_iframe">-->
<!--                                <iframe id="file_iframe" src="<?=base_url().'uploads/'.'/'.$file_name;?>" width="100%" frameborder="0" scrolling="no" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
                                <iframe id="file_iframe" src="https://docs.google.com/gview?url=<?=base_url().'uploads/'.$file_name.'&embedded=true';?>" width="100%" frameborder="0" scrolling="no" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                                
                            <?php
                            }
                            else{
                                echo '<div id="filesview_default" style="overflow:hidden">
                                            <img alt="" class="whiteborder" src="/images/icon_def.png" >
                                        </div>';
                            }
                             
                            ?>
                            
                            
                        </td>
                    </tr>
                </table>
                
            </div>
            <div id="divbarbottomid" class="divbarbottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0): ?>
                        <!-- openDiv('add_image_popup'); -->
                        <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_image_popup');">&nbsp;</a>
                        <?php //if($imageid>0){  ?>
                        <!-- openDiv('edit_album_image'); -->
                        <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_image');">&nbsp;</a>
                        
                        <a onclick="openDiv('sorting_album_files')" onfocus="this.blur();" href="javascript:void(0)" class="sortbt">&nbsp;</a>
                    <?php endif; ?>
                <?php } ?>        
            </div>
        </div>
        <div id="rightcontent">
            <div class="divbar" id="album_top">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0): ?>
                        <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('add_album');">&nbsp;</a>
                        <?php //if($curr_albumid>0){ ?>
                        <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('edit_album_form')">&nbsp;</a>
                        <a class="sortbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('sorting_album')">&nbsp;</a>
                    <?php endif; ?>
                <?php } ?>    
            </div>
            <div id="centerlisting_albums">
                <?php
                if(!empty($albumsArr)){
                    $i=1;
                    $j=1;
                    foreach ($albumsArr as $album){
                        $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkon' : 'linkoff';
                        $i= ($i>2) ? 1 : $i;
                       
                        if( strlen($j)==1 )
                         {
                         $count='0'.$j;
                         }
                         elseif( strlen($j)>=2 )
                         {
                         $count=$j;
                         }
                        
                    ?>
                <div class="albumcontainer<?=$i;?>" id="sidenavi_<?=$album['id']; ?>" onclick="window.location. href='/project/files/<?=$projectId; ?>/<?=$album['id']; ?>'">


                        <div class="ac1">

                          <div class="ac1num">
                            <?=$count; ?>                          
                          </div>
                          <div class="ac1name">
                            <a onfocus="this.blur();" class="<?=$classname; ?>" href="/project/files/<?=$projectId; ?>/<?=$album['id']; ?>"><?=$album['name']; ?></a>
                          </div>
                          <div class="ac1version">

                          </div>
                        </div>

                        <div class="ac2">
                          Album
                          <br>
                          Name
                        </div>
                        <?php
                            $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        ?>
                        <div class="ac3">
                          <a onfocus="this.blur();" href="/project/files/<?=$projectId; ?>/<?=$album['id']; ?>">
                            <img width="98" height="98" border="0" alt="" src="<?=$thumbnail;?>">
                          </a>
                        </div>


                      </div>
                    <?php
                        $i++;
                        $j++;
                        }
                    }
                    ?>
            </div>
            <div class="divbar" id="album_bottom">
                <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0 && $fileid>0): ?>
                        <a class="addbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('addalbumCommentDiv')">&nbsp;</a>
                        <a class="editbt" href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('editCommentDiv')">&nbsp;</a>
                    <?php endif; ?>
                <?php } ?>
            </div>
        </div>
        
        <div id="bottomDiv">
            <div id="videos" style="overflow:hidden">
                <?php
                    if(!empty($project_files)){
                        $videothumb=($file_thumb=='') ? base_url().'images/popup_pic1.jpg' :  base_url().'uploads/'.$file_thumb;
                ?>
                <div class="bottomside" id="videoThumbnails">
                        <img width="190" height="190" id="sideimage_3653" style="display:block;" class="buttomsideimage" src="<?=$videothumb.'?'.time();?>">
                    
                </div>
                <?php
                    }
                ?>
                <div id="project_videos" style="height: 191px; width: 100%">
                <?php
                    
                    if(!empty($project_files)){
                ?>
                
                    <table class="pl">

                        <tbody>
                            <tr class="head">
                                <td class="pl_top"></td>                            
                                <td class="pl_top">
                                    <?php
                                        if($sortColumn=='name'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            //$classname='order_not';
                                            $classname='sort-desc';
                                            $style='';
                                        }
                                        //$sort=()
                                    ?>
                                    <a href="<?=base_url('project/files/'.$projectId.'/'.$curr_albumid.'/'.$fileid.'/name/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Name</a>
                                </td>
                                <td class="pl_top">
                                    <?php
                                        if($sortColumn=='user'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/files/'.$projectId.'/'.$curr_albumid.'/'.$fileid.'/user/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">User</a>
                                </td>
                                <td class="pl_top" style="width: 135px;">
                                    <?php
                                        if($sortColumn=='date'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/files/'.$projectId.'/'.$curr_albumid.'/'.$fileid.'/date/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Date</a>
                                </td>
                                <td class="pl_top" style="width: 75px;">
                                    <?php
                                        if($sortColumn=='size'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            $classname='order_not';
                                            $style='';
                                        }
                                    ?>
                                    <a href="<?=base_url('project/files/'.$projectId.'/'.$curr_albumid.'/'.$fileid.'/size/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Size</a>
                                </td>
                                
                            </tr>
                            <?php
                            foreach ($project_files as $files) {
                                $videonamereplace=str_replace('_', ' ', $files['title']);
                                $videoName=(strlen($videonamereplace)>100) ? substr($videonamereplace, 0, 40).'...' : $videonamereplace;
                                $classname=($files['id']==$fileid) ? 'pl_conton' : 'pl_cont';
                                $filesize=$files['size']/1024;
                                $filesize=round($filesize, 2);
                                $filesize=$filesize.'MB';
                                $classSelected=($fileid==$files['id']) ? 'contentrow-selected' : '';
                            ?>
                            <tr class="contentrow <?=$classSelected; ?>" id="bottom_<?=$files['id'] ?>" onmouseover="showComments(<?=$files['id']; ?>)">
                                <td class="pl_cont1"><a onfocus="this.blur();" href="/project/download/file/<?=$files['id'] ?>" class="pl_download">&nbsp;</a></td>
                                <?php $fileThumb = ($files['thumbnail'] != '') ? $files['thumbnail'].'?'.time() : ''; ?>
                                <td class="<?=$classname;?> abc" onmouseover="changeThumbnail('<?=$fileThumb; ?>')"><a href="/project/files/<?=$projectId?>/<?=$curr_albumid;?>/<?=$files['id']?>" title="<?=$files['title']; ?>"><?=$videoName; ?> </a></td>
                                <td class="pl_cont"><?=$files['stage_name']; ?></td>
                                <td class="pl_cont"><?=date('Y M d, h:i A', strtotime($files['added_date'])); ?></td>
                                <td class="pl_cont" style="text-align: right; padding-right: 5px;"><?=$filesize; ?></td>
                                
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                
                <?php
                    }
                    
                ?>
                
                </div>
                
                
                
            </div>
            <div id="right_comments">
                <?php
                /*
                <div class="commenttitle">comments for album <?=@$albumInfoArr['name']; ?></div>
                 * 
                 */
                ?>
                <div class="commenttitle">comments for album files</div>
                <?php
                $i=1;
                $html='';
                foreach ($album_comments as $key => $value) {
                    $display=($fileid==$key) ? 'display:block' : '';
                    $html.='<div id="commentsVideo'.$key.'" class="showCommentsVideo" style="'.$display.'">';
                    $j=1;
                    foreach ($value as $arrkey => $arrvalue) {
                        if( strlen($j)==1 ){
                            $count='0'.$j;
                        }
                        else{
                            $count=$j;
                        }
                        $html.= '<div class="commentnormal">
                                    <div class="commentnames">'.$count.' - '.$arrvalue['stage_name'].' ('.date('d.m.y, h:i A', strtotime($arrvalue['added_date'])).') :</div>
                                    <div class="commentmessage"> '.$arrvalue['comment'].'</div>
                                </div>';
                        $j++;
                    }
                    $html.='</div>';
                    
                }
                
                echo $html;
                /*
                $j=1;
                if($album_comments!=''){
                    foreach ($album_comments as $key => $value) {
                    if( strlen($j)==1 ){
                        $count='0'.$j;
                    }
                    else{
                        $count=$j;
                    }
                    echo '<div class="commentnormal">
                                <div class="commentnames">'.$count.' - '.$value['stage_name'].' ('.date('d.m.y, h:i A', strtotime($value['added_date'])).') :</div>
                                <div class="commentmessage"> '.$value['comment'].'</div>
                            </div>';
                    $j++;
                }
                }
                 * 
                 */
                
                ?>
            </div>
        </div>
    </div>
    
    

    <div id="cover" style="display: none; height: 100%; width: 100%;"></div>
    <script>
        $(function() {
          $( ".sortable" ).sortable();
          $( ".sortable" ).disableSelection();
        });
        
        $(window).load(function(){
            if(curr_album>0)
                $("#centerlisting_albums").mCustomScrollbar("scrollTo","#sidenavi_"+curr_album);
            
            if(imageid>0)
                $("#project_videos").mCustomScrollbar("scrollTo","#bottom_"+imageid);
        })
    </script>
    
</body>
</html>