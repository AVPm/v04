<?php
// ****************************************************************************
// 
//     right content after user login
//
// ****************************************************************************
?>
<div id="rightcontent">
    <div id="userdetails">
        logged in as <?php echo $this->session->userdata('user_email'); ?>
        <br>
        <a href="javascript:void(0)" onclick="openDiv('edit_profile')">My Account</a>
        <br>
        <?php echo anchor('user/logout', 'Logout'); ?>
        
        <hr class="hr_grey">

        <a href="javascript:void(0)" class="but_right" onfocus="this.blur();" onclick="showSearch()" id="searchtxt">Search</a>
        <div id="searchDiv" style="display: none">
            <input type="text" id="input_search" name="input_search" value="" class="input_search" onkeypress="handleEvent(event)">
            <a href="javascript:void(0)" class="but_search" onclick="showSearchResult()" >Search</a>
            <br />
            <br />
        </div>
        
        <a href="javascript:void(0)" class="but_down" onfocus="this.blur();">Whats New</a>
        
        <div class="styled-select floatLeft filter_width">		
            <select size="1" id="identifier_project" name="identifier_project" class="" onchange="showSearchResult();">    
                <option value="" selected="selected">Projects (All)</option> 
                <?php
                    if(count($all_projects)>0){
                        foreach ($all_projects as $key => $value) {
                            echo '<option value="'.$value['id'].'">Project ('.$value['identifier'].')</option>';
                        }
                    }
                ?>

            </select>
    </div>
    <div class="styled-select floatLeft filter_width">			
		<select size="1" id="itemtype_filter" name="itemtype_filter" class="" onchange="showSearchResult();">    
                    <option value="" selected="selected">Type (All)</option>
                    <option value="images">Type (Images)</option>
                    <option value="videos">Type (Videos)</option>
                    <option value="files">Type (Files)</option>
                </select>
    </div>        
    <div class="styled-select floatLeft filter_width">
		<select size="1" id="search_limit" name="search_limit" class="" onchange="showSearchResult();">    
                    <option value="10" selected="selected">Show (10 items)</option>
                    <option value="25">Show (25 items)</option>
                    <option value="50">Show (50 items)</option>
                </select>
    </div>
    <div class="styled-select floatLeft filter_width">
		<select size="1" id="search_order" name="search_order" class="" onchange="showSearchResult();">    
                    <option value="latest_first" selected="selected">Sort (latest first)</option>
                    <option value="oldest_first">Sort (oldest first)</option>
                    <option value="user">Sort (User)</option>
                    <option value="project">Sort (Project)</option>
                </select>
    </div>    
    </div>
    
    <div id="recent_content" style="overflow: auto; overflow-x: hidden;">
        <?php
        /*
            for($i=1; $i<=7; $i++){
        ?>
            <div class="item">
                <a href="javascript:void(0)" onfocus="this.blur();"><img src="/uploads/Koala_thumb.jpg" alt="" class="floatLeft" width="98" height="98"></a>
                <div class="floatLeft">
                  <b>File</b>
                        <div class="nooverflow">interface_04.01.psd</div>
                        <div class="nooverflow">2014 Nov 15th | 1:29 pm</div>
                        by User: Arun<br> <div class="seen_yes">Seen:</div>	Project Id: avpmv04<br>									</div>
                <div class="clear"></div>
            </div>
        <?php
            }
         * 
         */
        ?>
        <img style="position:absolute; z-index:15; top:20%; left:25%" src="<?=base_url(); ?>images/ajax-loader.gif">
    </div>
    
    
    
</div>

    