<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>
<style>
    .bottom_table tr th{
        font-size: 12px;
    }
    .tableheading{
        width: auto !important;
    }
    .bottom_table tr td{background: #242424 none repeat scroll 0 0 !important}
</style>

<!-- add project HTML -->
<div class="popup" id="addProjectFolder_div" style="width: 640px;">
    <div class="popup_head">    
           <span>Add a new Project</span>
    </div>
    
    <div class="popup_forms" style="padding:25px 15px 25px 25px">
       <?php
           echo form_open_multipart(base_url("user/file_management"), array('onsubmit'=>'return chkaddProjectFolder()', 'id'=>'add_project'));
           
           echo '<label for="title">Title:</label>'; 
           echo '<input type="text" id="project_folder_title" value="" name="project_folder_title" style="margin-left:15px;">';

           echo form_close();
       ?>

   </div>
   <div class="popup_bottom">
       <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProjectFolder_div')">cancel</a>
       <a class="but_green floatRight" href="javascript:void(0)" onclick="$('#add_project').submit();">submit</a>
       <div class="clear">&nbsp;</div> 
   </div> 
</div>


<div class="clear">&nbsp;</div>

<div></div>
<h4>File Management</h4>
<div class="clear">&nbsp;</div>
<div>
    <a href="javascript:void(0)" style="color:#fff" onclick="openDiv('addProjectFolder_div')"><img src="<?=base_url('images/folder_add.png'); ?>"> Add Project</a>
</div>
<div class="clear">&nbsp;</div>
<form name="fileManageForm" id="fileManageForm" method="POST" action="">
    <input type="hidden" name="type" value="project" id="type" />
    <input type="button" name="deletechk" id="deletechk" value="Delete Selected" disabled="disabled">


<table class="bottom_table">
    <tbody>
        <tr>
            <th style="width: 2px; "><input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes"></th>
            <th style="width:350px;">Name</th>
            <th style="width:100px;">Type</th>
            <th style="width:100px;">Size</th>
            <th style="width:100px;">DATE</th>
            <th style="width:120px;">Action</th>
            
        </tr>
        <?php
        foreach ($list_projects as $key => $value) {
            $filesize=$value['size']/1024;
            $filesize=round($filesize, 2);
            $filesize=$filesize.' MB';
        ?>
        <tr id="projectrow_<?=$value['id']; ?>">
            <td class="tableheading" style="width: 2px; text-align: center" align="center"><input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox"></td>
            <td class="tableheading"><img src="<?=base_url('images/closed_folder_yellow.png'); ?>" height="25" width="25" ondblclick="window.location.href='<?=base_url('user/file_management/'.$value['id']);?>'">
                <span class="project_title" id="project_title_<?=$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>)"><?=ucwords(strtolower($value['title'])); ?></span>
                <input type="text" name="foldername" id="foldernametxt_<?=$value['id']; ?>" value="<?=$value['title']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, 'project')" class="foldername" />
                <input type="hidden" name="folderid" id="folderidtxt_<?=$value['id']; ?>" value="<?=$value['id']; ?>" />
            </td>
            <td class="tableheading" style="text-align: right"><?php echo 'Folder'; ?></td>
            <td class="tableheading" style="text-align: right"><?=$filesize; ?></td>
            <td class="tableheading" style="text-align: right"><?=date('d-M-Y', strtotime($value['added_date']));?></td>
            <td class="tableheading"><a href="<?=base_url('user/file_management/'.$value['id']);?>">Open</a> | <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>)">Edit</a> | <a href="javascript:void(0)" onclick="deleteProject(<?=$value['id']; ?>)">Delete</a></td>
        </tr>
        <?php
        }
        ?>
        
</tbody>
    </table>
</form>
<script>
    
    function chkaddProjectFolder(){
        var title=$('#project_folder_title').val();
        if(title=='' || title==null){
            $('#project_folder_title').css('border', '1px solid red');
            $('#project_folder_title').focus();
            return false;
        }
    }
    
    function showrename(id){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+id).hide();
        $('#foldernametxt_'+id).show();
    }
    
    function renamefolder(id, type){
        var name=$('#foldernametxt_'+id).val();
        var id=$('#folderidtxt_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+id).html($('#foldernametxt_'+id).val());
                    $('#project_title_'+id).show();
                    $('#foldernametxt_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
    function deleteProject(id){
         var r = confirm("Are you sure to remove this?");
        if (r == true) {
            $.ajax({
                url:base_url+"project/remove/"+id,
                type: 'POST',
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+id).remove();
                }    			
            }); 
        }
    }
</script>
