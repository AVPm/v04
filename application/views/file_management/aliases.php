<?php
// ****************************************************************************
// 
//     aliases of files view
//
// ****************************************************************************


?>




<div class="clear">&nbsp;</div>

<div></div>
<h4>Aliases</h4>
<div class="clear">&nbsp;</div>

<div class="clear">&nbsp;</div>
<div class="filepath"><a href="<?=base_url('user/file_management') ?>"><img src="<?=base_url('images/home_icon.png')?>" height="20" width="20">File Management</a></div>
<div class="clear">&nbsp;</div>
<div id="frm">
    <form method="post" name="aliases_frm" id="aliases_frm">
        <input type="hidden" name="type" id="type" value="<?=$type; ?>" />
        <input type="hidden" name="fileid" id="fileid" value="<?=$id; ?>" />
        <div class="floatLeft">
            <label>Select Project: </label>
            <div class="styled-select filter_width">
                <select name="projects" id="projects" onchange="showAlbums('<?=$type ?>', this.value)">
                    <option value="">Select Project</option>
                    <?php 
                    foreach ($list_projects as $key => $value) {
                        echo '<option value="'.$value['id'].'">'.$value['title'].'</option>';
                    } 
                    ?>
                </select>
            </div>
        </div>
        <div class="clear">&nbsp;</div>
        <div class="floatLeft" id="album_list" style="display: none">
            <label>Select <?=ucwords($type); ?> Albums: </label>
            <div class="styled-select filter_width" id="albumsListing">
                
            </div>
        </div>
    </form>
</div>


<script>
    function showAlbums(type, id){
        var html='There is some error';
        $.ajax({
            url:base_url+"user/getAlbums/",
            type: 'POST',
            data: 'type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                html='';
                var myArray = jQuery.parseJSON(msg);
                if(myArray.length>0){   
                    html+='<select name="albums" id="albums">';           
                    for(var i=0; i<myArray.length;i++){
                        html+='<option value='+myArray[i]['id']+'>'+myArray[i]['name']+'</option>';
                    }
                    html+='</select>';
                    $('#albumsListing').html(html);
                    if($('#savealiases').length==0){
                        $('#album_list').after('<div class="clear" id="savealiases"><input type="button" name="save_aliases" id="save_aliases" value="Save Aliases" ></div>')
                    }
                    
                    
                }
                else{
                    $('#savealiases').remove();
                    $('#albumsListing').html('There is not any albums for this type')
                }
                
                $('#album_list').show();
            }    			
        });
    }
    
    $('#aliases_frm').on('click', '#save_aliases', function(){
        $('#aliases_frm').submit();
    })
    
</script>