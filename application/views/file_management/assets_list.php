<?php
// ****************************************************************************
// 
//     User's assets list detail view
//
// ****************************************************************************


?>
<style>
    .bottom_table tr th{
        font-size: 12px;
    }
    .tableheading{
        width: auto !important;
    }
    .bottom_table tr td{background: #242424 none repeat scroll 0 0 !important}
</style>
<script>
    var upload_url= "<?=base_url(); ?>project/album/<?=$download_type; ?>upload";
    var curr_album='<?=@$curr_albumid; ?>';
</script>
<!-- add project HTML -->
<?php
/*
<div class="popup" id="addProjectFolder_div" style="width: 640px;">
    <div class="popup_head">    
           <span>Add a new <?=ucwords($download_type); ?></span>
    </div>
    
    <div class="popup_forms" style="padding:25px 15px 25px 25px">
       <?php
           echo form_open_multipart(base_url("user/file_management/".$projectid), array('onsubmit'=>'return chkaddProjectFolder()', 'id'=>'add_project_album'));
           
           echo '<label for="title">Name:</label>'; 
           echo '<input type="text" id="project_album_title" value="" name="project_album_title" style="margin-left:15px;">';
           echo '</br>';
           echo '<label for="title">Type:</label>'; 
           echo '<select id="project_album_type" name="project_album_type" style="margin-left:22px; color:#000">
                    <option value="images">Images</option>
                    <option value="videos">Videos</option>
                    <option value="files">Files</option>
                </select>';

           echo form_close();
       ?>

   </div>
   <div class="popup_bottom">
       <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProjectFolder_div')">cancel</a>
       <a class="but_green floatRight" href="javascript:void(0)" onclick="$('#add_project_album').submit();">submit</a>
       <div class="clear">&nbsp;</div> 
   </div> 
</div>
 * 
 */
?>
<div class="popup" id="addProjectFolder_div" style="width: 820px;">
            
        <div class="popup_head">
            <a class="on" href="#" id="image_only">Add <?=ucwords($download_type); ?></a>
        </div>	
            
        <div class="popup_forms" style="padding: 0px">
            <form action="" enctype="multipart/form-data" method="post" id="form1">

                <div id="fileuploader">
                    <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                </div>    


            </form>

        </div>
        <div class="popup_bottom">
            <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProjectFolder_div')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="window.location.reload()">done</a>
                <div class="clear">&nbsp;</div> 
        </div> 
</div>


<div class="clear">&nbsp;</div>

<div></div>
<h4>File Management</h4>
<div class="clear">&nbsp;</div>
<div>
    <a href="javascript:void(0)" style="color:#fff" onclick="openDiv('addProjectFolder_div')"><img src="<?=base_url('images/folder_add.png'); ?>"> Add <?=ucwords($download_type); ?></a>
</div>
<div class="clear">&nbsp;</div>
<div class="filepath"><a href="<?=base_url('user/file_management') ?>"><img src="<?=base_url('images/home_icon.png')?>" height="20" width="20"></a>/<a href="<?=base_url('user/file_management/'.$projectid); ?>"><?=ucwords(strtolower($project_name)); ?></a> / <a href="#"><?=ucwords(strtolower($album_name)); ?></a></div>
<div class="clear">&nbsp;</div>
<?php
if($this->session->flashdata('aliases_success')!=''){
    echo '<div class="clear">'.$this->session->flashdata('aliases_success').'</div>';
    echo '<div class="clear">&nbsp;</div>';
}
?>
<form name="fileManageForm" id="fileManageForm" method="POST" action="">
    <input type="hidden" name="type" value="assets" id="type" />
    <input type="hidden" name="assets_type" value="<?=$download_type; ?>" id="assets_type" />
    <input type="button" name="deletechk" id="deletechk" value="Delete Selected" disabled="disabled">
<table class="bottom_table">
    <tbody>
        <tr>
            <th style="width: 2px; "><input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes"></th>
            <th style="width:350px;">Name</th>
            <th style="width:100px;">Type</th>
            <th style="width:100px;">Size</th>
            <th style="width:100px;">DATE</th>
            <th style="width:180px;">Action</th>
            
        </tr>
        <?php
        foreach ($list_albums as $key => $value) {
            $filesize=$value['size']/1024;
            $filesize=round($filesize, 2);
            $filesize=$filesize.' MB';
            $filename=(strlen($value['name'])>30) ? substr($value['name'], 0, 30).'...' : $value['name'];
            $path_parts = pathinfo($value['name']);
            //echo '<pre>'; print_r($path_parts); die();
            $type= $path_parts['extension'];
            switch ($type) {
                case 'mp4':
                    $icon='mp4_icon.png';
                break;
            
                case 'docx':
                case 'doc':
                    $icon='docx_icon.png';
                break;
            
                case 'xlsx':
                case 'xlx':
                    $icon='xlsx_icon.png';
                break;
            
                case 'jpg':
                case 'jpeg':
                    $icon='jpg_icon.png';
                break;
            
                case 'txt':
                    $icon='txt_icon.png';
                break;
            
                case 'pdf':
                    $icon='pdf_icon.png';
                break;
            
                case 'png':
                    $icon='png_icon.png';
                break;
            
                case 'gif':
                    $icon='gif_icon.png';
                break;

                default:
                    $icon='default_file_icon.png';
                break;
            }
            $status=$value['status'];
        ?>
        <tr id="projectrow_<?=$value['id']; ?>">
            <td class="tableheading" style="width: 2px; text-align: center" align="center"><input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox"></td>
            <td class="tableheading"><img src="<?=base_url('images/'.$icon); ?>" height="24" width="24">
                <span class="project_title" id="project_title_<?=$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>)" title="<?=ucwords(strtolower($value['name'])); ?>"><?=ucwords(strtolower($filename)); ?></span>
                <input type="text" name="foldername" id="foldernametxt_<?=$value['id']; ?>" value="<?=$value['name']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, '<?=$value['type']; ?>')" class="foldername" />
                <input type="hidden" name="folderid" id="folderidtxt_<?=$value['id']; ?>" value="<?=$value['id']; ?>" />
            </td>
            <td class="tableheading" style="text-align: right"><?php echo $type; ?></td>
            <td class="tableheading" style="text-align: right"><?=$filesize; ?></td>
            <td class="tableheading" style="text-align: right"><?=date('d-M-Y', strtotime($value['added_date']));?></td>
            <td class="tableheading">
                <a href="<?=base_url('project/download/'.$download_type.'/'.$value['id']);?>" target="_blank">Download</a> | 
                <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>)">Edit</a> | 
                <span class="deletetxt">
                    <?php if($status=='1'){ ?>
                    <a href="javascript:void(0)" onclick="removeAlbumFile(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Delete</a>
                    <?php } else{ ?>
                    <a href="javascript:void(0)" onclick="undoAlbumFile(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Undo</a>
                    <?php } ?>
                </span>
                <span class="aliasestxt">
                    <?php if($value['aliases']==0 && $status=='1'){ ?>
                    | <a href="<?=base_url('user/aliases/'.$value['type'].'/'.$value['id']); ?>">Aliases</a>
                    <?php } ?>
                </span>
            </td>
        </tr>
        <?php
        }
        ?>
        
</tbody>
    </table>
</form>
<script>
    
   
    function chkaddProjectFolder(){
        var title=$('#project_album_title').val();
        if(title=='' || title==null){
            $('#project_album_title').css('border', '1px solid red');
            $('#project_album_title').focus();
            return false;
        }
    }
    
    function showrename(id){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+id).hide();
        $('#foldernametxt_'+id).show();
    }
    
    function removeAlbumFile(albumid, table){
        var r = confirm("Are you sure to remove this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/removealbumimage/",
                type: 'POST',
                data: 'imageid='+albumid+"&type="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+albumid).remove();
                    $('#projectrow_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="undoAlbumFile('+albumid+', \''+table+'\')">Undo</a>');
                    $('#projectrow_'+albumid+' .aliasestxt').hide();
                }    			
            });
        } else {
            return false;
        }
    }
    
    function undoAlbumFile(albumid, table){
        var r = confirm("Are you sure to undo this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/undoalbumimage/",
                type: 'POST',
                data: 'imageid='+albumid+"&type="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+albumid).remove();
                    $('#projectrow_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="removeAlbumFile('+albumid+', \''+table+'\')">Delete</a>');
                    $('#projectrow_'+albumid+' .aliasestxt').show();
                }    			
            });
        } else {
            return false;
        }
    }
    
    function renamefolder(id, type){
        var name=$('#foldernametxt_'+id).val();
        var id=$('#folderidtxt_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+id).html($('#foldernametxt_'+id).val());
                    $('#project_title_'+id).show();
                    $('#foldernametxt_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
</script>
